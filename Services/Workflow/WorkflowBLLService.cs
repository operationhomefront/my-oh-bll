﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.WorkFlow;
using OH.DAL.Domain.WorkflowTemplates;
using OH.DAL.Repositories.WorkFlow;

namespace OH.BLL.Services.Workflow
{
    public class WorkflowBLLService : IWorkflowBLLService
    {
        private readonly IWorkFlowItemRepository _workFlowItemRepository;
        private readonly IWorkFlowItemDependencyRepository _workFlowItemDependencyRepository;

        public WorkflowBLLService(IWorkFlowItemRepository workFlowItemRepository,
            IWorkFlowItemDependencyRepository workFlowItemDependencyRepository)
        {
            _workFlowItemRepository = workFlowItemRepository;
            _workFlowItemDependencyRepository = workFlowItemDependencyRepository;
        }

        public IEnumerable<WorkflowItemDependencyViewModel> GetDependencyViewModel()
        {
            var s = _workFlowItemDependencyRepository.GetAll();
            var mapped = Mapper.Map<IEnumerable<WorkflowItemDependency>, IEnumerable<WorkflowItemDependencyViewModel>>(s);
            return mapped;
        }

        public IEnumerable<WorkflowItemViewModel> GetWorkflowItemViewModel()
        {
            var s = _workFlowItemRepository.GetAll().Select(

                task => new WorkflowItemViewModel
                {
                    Id = task.Id,
                    Title = task.Title,
                    Start = DateTime.SpecifyKind(task.Start, DateTimeKind.Utc),
                    End = DateTime.SpecifyKind(task.End, DateTimeKind.Utc),
                    ParentId = task.ParentWorkFlowItemId,
                    PercentComplete = task.PercentComplete,
                    OrderId = task.OrderId,
                    Expanded = task.Expanded,
                    Summary = task.Summary
                }
                ).AsEnumerable();
            //var mapped = Mapper.Map<IEnumerable<WorkflowItem>, IEnumerable<WorkflowItemViewModel>>(s);
            return s;
        }
    }
}
