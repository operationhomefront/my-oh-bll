﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.WorkFlow;


namespace OH.BLL.Services.Workflow
{
    public interface IWorkflowBLLService
    {
        IEnumerable<WorkflowItemDependencyViewModel> GetDependencyViewModel();
        IEnumerable<WorkflowItemViewModel> GetWorkflowItemViewModel();

    }
}
