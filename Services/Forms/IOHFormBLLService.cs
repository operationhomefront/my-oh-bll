﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Forms;

namespace OH.BLL.Services.Forms
{
    public interface IOHFormBLLService
    {
        void UpdateForm(OHFormViewModel model);
        void RemoveControl(Guid controlGuid);
        void AddControl(OHFormViewModel form,OHFormControlViewModel control);
        void AddNewForm(OHFormViewModel model);
    }
}
