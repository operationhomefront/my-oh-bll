﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Repositories.Forms;

namespace OH.BLL.Services.Forms
{
    public class OHFormBLLService : IOHFormBLLService
    {
        private readonly IOHFormRepository _formRepository;
        private readonly IOHFormCheckBoxPropertiesRepository _checkBoxPropertiesRepository;
        private readonly IOHFormControlRepository _controlRepository;
        private readonly IOHFormDateTimePropertiesRepository _dateTimePropertiesRepository;
        private readonly IOHFormDescriptionPropertiesRepository _descriptionPropertiesRepository;
        private readonly IOHFormEmbeddedHtmlPropertiesRepository _embeddedHtmlPropertiesRepository;
        private readonly IOHFormFileUploadPropertiesRepository _fileUploadPropertiesRepository;
        private readonly IOHFormNumberPropertiesRepository _numberPropertiesRepository;
        private readonly IOHFormRadioButtonsPropertiesRepository _radioButtonsPropertiesRepository;
        private readonly IOHFormSectionRepository _sectionRepository;
        private readonly IOHFormSelectListPropertiesRepository _selectListPropertiesRepository;
        private readonly IOHFormServiceMemberPropertiesRepository _serviceMemberPropertiesRepository;
        private readonly IOHFormThemeRepository _themeRepository;
        private readonly IOHFormUmbracoPropertiesRepository _umbracoPropertiesRepository;
        private readonly IOHFormValueRepository _formValueRepository;
        
        public void UpdateForm(Models.Forms.OHFormViewModel model)
        {
            throw new NotImplementedException();
        }

        public void RemoveControl(Guid controlId)
        {
            _controlRepository.Delete(controlId);
            _controlRepository.Save();
        }

        public void AddNewForm(Models.Forms.OHFormViewModel model)
        {
            throw new NotImplementedException();
        }
        public void AddControl(Models.Forms.OHFormViewModel form, Models.Forms.OHFormControlViewModel control)
        {
            throw new NotImplementedException();
        }

    }
}
