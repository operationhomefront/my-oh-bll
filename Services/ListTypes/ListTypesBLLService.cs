﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using OH.BLL.Models.ListTypes;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.ListTypes.CareGiver;
using OH.DAL.Domain.ListTypes.General;
using OH.DAL.Domain.ListTypes.Injury;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ListTypes.Person;
using OH.DAL.Repositories.ListTypes;
using OH.DAL.Repositories.ListTypes.Caregiver;
using OH.DAL.Repositories.ListTypes.General;
using OH.DAL.Repositories.ListTypes.Person;
using OH.DAL.Repositories.ProgramProfiles.Injury;

namespace OH.BLL.Services.ListTypes
{
    public class ListTypesBLLService : IListTypesBLLService
    {
       

        private readonly IListTypeRepository _listTypeRepository;

        public ListTypesBLLService(
            IListTypeRepository listTypeRepository)
        {
            _listTypeRepository = listTypeRepository;

        }

        public IEnumerable<ListTypeViewModel> GenderList()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(Gender));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> StatesList()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(States));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> AgeRangeList()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(AgeRange));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> DurationProvidingCareList()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(DurationProvidingCare));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> HoursProvidingCareWeeklyList()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(HoursProvidingCareWeekly));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> Relationship()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(Relationship));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> InjuryInfliction()
        {
            return new List<ListTypeViewModel>();
            //try
            //{
            //    IEnumerable<ListInjuryInflictionCategory> listUnmapped = _injuryInflictionRepository.GetAll();
            //    IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

            //    var model1 = new ListDisplayViewModel
            //    {
            //        ListDisplays = mapped
            //    };

            //    return model1;
            //}
            //catch (Exception)
            //{

            //    throw;
            //}
        }


        public IEnumerable<ListTypeViewModel> GetListByType<T>() where T : class
        {

            IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByTypeTest<T>();

            IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

            return mapped;
            //return _listTypeRepository.GetByTypeTest<T>();
        }


        public IEnumerable<ListTypeViewModel> BranchOfService()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(ListMilitaryBranch));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> ServiceStatus()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(ListMilitaryServiceStatus));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> MilitaryRank()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(ListMilitaryRank));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> DeploymentStatus()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(ListMilitaryDeploymentStatus));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<ListTypeViewModel> VeteranStatus()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(ListMilitaryVeteranStatus));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public IEnumerable<ListTypeViewModel> InjuryInflictionCategory()
        {
            try
            {
                IEnumerable<ListType> listUnmapped = _listTypeRepository.GetByType(typeof(ListInjuryInflictionCategory));

                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);

                return mapped;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
