﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ListTypes;

namespace OH.BLL.Services.ListTypes.Person
{
    public interface IGenderBLLService
    {
        ListDisplayViewModel GetGenderLookup();
    }
}
