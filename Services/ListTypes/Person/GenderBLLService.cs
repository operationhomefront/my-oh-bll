﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.ListTypes;
using OH.DAL.Domain.ListTypes.Person;
using OH.DAL.Repositories.ListTypes.Person;

namespace OH.BLL.Services.ListTypes.Person
{
    public class GenderBLLService : IGenderBLLService
    { 
        private readonly IGenderRepository _genderRepository;

        public GenderBLLService(IGenderRepository genderRepository)
        {
            _genderRepository = genderRepository;
        }

        public ListDisplayViewModel GetGenderLookup()
        {
 	        try
                    {
                        IEnumerable<Gender> listUnmapped = _genderRepository.GetAll();
                        IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);
                
                        var model1 = new ListDisplayViewModel
                        {
                            ListDisplays = mapped
                        };

                        return model1;
                    }
                    catch (Exception)
                    {
                
                        throw;
                    }
        }
    }
}
