using OH.BLL.Models.ListTypes;

namespace OH.BLL.Services.ListTypes.Military
{
    public interface IListMilitaryVeteranStatusBLLService
    {
        ListDisplayViewModel GetMilitaryVeteranStatu();
    }
}