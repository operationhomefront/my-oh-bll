using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using OH.BLL.Models.ListTypes;
using OH.BLL.Models.ProgramProfiles;
using OH.BLL.Services.ListTypes.Military;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Repositories.ProgramProfiles.Military;

namespace OH.BLL.Services.ProgramProfiles
{
    public class ListMilitaryBranchBllService : IListMilitaryBranchBLLService
    {
        private readonly IListMilitaryBranchRepository _branchRepository;

        public ListMilitaryBranchBllService(IListMilitaryBranchRepository branchRepository)
        {
            _branchRepository = branchRepository;
        }

        public ListDisplayViewModel GetMilitaryBranches()
        {
            var model1 = new ListDisplayViewModel
            {
                ListDisplays = 
                    Mapper.Map<IEnumerable<ListMilitaryBranch>, IEnumerable<ListTypeViewModel>>(
                        _branchRepository.GetAll().OrderBy(o=>o.SortOrder))
            };

            return model1;
        }

        public ListDisplayViewModel GetActiveDutyMilitaryBranches()
        {
            try
            {
                IEnumerable<ListMilitaryBranch> listUnmapped = _branchRepository.GetActiveDutyMilitaryBranches(true);
                IEnumerable<ListTypeViewModel> mapped = listUnmapped.Select(Mapper.DynamicMap<ListTypeViewModel>);
                
                var model1 = new ListDisplayViewModel
                {
                    ListDisplays = mapped
                };

                return model1;
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

       
    }
}