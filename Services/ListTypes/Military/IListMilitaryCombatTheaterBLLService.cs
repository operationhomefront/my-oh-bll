﻿using OH.BLL.Models.ListTypes;

namespace OH.BLL.Services.ListTypes.Military
{
    public interface IListMilitaryCombatTheaterBLLService
    {
        ListDisplayViewModel GetMilitaryCombatTheater();
    }
}