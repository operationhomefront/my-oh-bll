﻿using OH.BLL.Models.ListTypes;

namespace OH.BLL.Services.ListTypes.Military
{
    public interface IListMilitaryBranchBLLService   
    {
        ListDisplayViewModel GetMilitaryBranches();
        ListDisplayViewModel GetActiveDutyMilitaryBranches();
        
    }
}