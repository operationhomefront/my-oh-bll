﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ListTypes;

namespace OH.BLL.Services.ListTypes
{
    public interface IListTypesBLLService
    {
        IEnumerable<ListTypeViewModel> GenderList();
        IEnumerable<ListTypeViewModel> StatesList();
        IEnumerable<ListTypeViewModel> AgeRangeList();
        IEnumerable<ListTypeViewModel> DurationProvidingCareList();
        IEnumerable<ListTypeViewModel> HoursProvidingCareWeeklyList();
        IEnumerable<ListTypeViewModel> Relationship();
        IEnumerable<ListTypeViewModel> InjuryInfliction();

        //***********************  Service member lookups **********************/
        IEnumerable<ListTypeViewModel> BranchOfService();
        IEnumerable<ListTypeViewModel> ServiceStatus();
        IEnumerable<ListTypeViewModel> MilitaryRank();
        IEnumerable<ListTypeViewModel> DeploymentStatus();
        IEnumerable<ListTypeViewModel> VeteranStatus();

        IEnumerable<ListTypeViewModel> InjuryInflictionCategory();

        IEnumerable<ListTypeViewModel>  GetListByType<T>()where T:class;
    }
}
