﻿using System;
using System.Linq;
using OH.BLL.Models.Schedulers;

namespace OH.BLL.Services.Schedulers
{
    public interface ISchedulerMeetingService
    {
        IQueryable<MeetingViewModel> GetAll();
        void Insert(MeetingViewModel model);
        void Update(MeetingViewModel model);
        void Delete(MeetingViewModel model);
        IQueryable<ResourceViewModel> GetAllAvailablePersons();

        IQueryable<ResourceViewModel> GetAllAvailableMeetingLocations();

        IQueryable<ResourceViewModel> GetAllAvailableMeetingRooms(Guid meetingLocationId);

    }
}