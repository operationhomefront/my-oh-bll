﻿using System.Linq;
using System.Web.ModelBinding;
using OH.BLL.Models.Schedulers;

namespace OH.BLL.Services.Schedulers
{
    public interface ISchedulerTaskService
    {
        IQueryable<TaskViewModel> GetAll();
        void Insert(TaskViewModel model);
        void Update(TaskViewModel model);
        void Delete(TaskViewModel model);
    }
}