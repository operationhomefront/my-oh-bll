﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using OH.BLL.Models.Schedulers;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories.Meetings;
using OH.DAL.Repositories.Persons;

namespace OH.BLL.Services.Schedulers
{
    public class SchedulerMeetingService: ISchedulerMeetingService
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IMeetingLocationRepository _meetingLocationRepository;
        private readonly IMeetingRoomRepository _meetingRoomRepository;
        private readonly IMeetingAttendeeRepository _meetingAttendeeRepository;

        public SchedulerMeetingService(IMeetingRepository meetingRepository, IPersonRepository personRepository, IMeetingLocationRepository meetingLocationRepository, IMeetingRoomRepository meetingRoomRepository, IMeetingAttendeeRepository meetingAttendeeRepository)
        {
            _meetingRepository = meetingRepository;
            _personRepository = personRepository;
            _meetingLocationRepository = meetingLocationRepository;
            _meetingRoomRepository = meetingRoomRepository;
            _meetingAttendeeRepository = meetingAttendeeRepository;
        }

        public   IQueryable<MeetingViewModel> GetAll()
        {
            var model = Mapper.Map<IEnumerable<Meeting>, IEnumerable<MeetingViewModel>>(_meetingRepository.GetAll()).ToList();

            return model.AsQueryable();
        }

        public IQueryable<ResourceViewModel> GetAllAvailablePersons()
        {
            //TODO: Add some filtering here, for persons...doing a get all person for now
            return Mapper.Map<IEnumerable<Person>, IEnumerable<ResourceViewModel>>(_personRepository.GetAll()).AsQueryable();
        }

        public IQueryable<ResourceViewModel> GetAllAvailableMeetingLocations()
        {
            //TODO: Add some filtering here, for persons...doing a get all person for now
            return Mapper.Map<IEnumerable<MeetingLocation>, IEnumerable<ResourceViewModel>>(_meetingLocationRepository.GetAll()).AsQueryable();
        }

        public IQueryable<ResourceViewModel> GetAllAvailableMeetingRooms(Guid meetingLocationId)
        {
            //TODO: Add some filtering here, for persons...doing a get all person for now
            return Mapper.Map<IEnumerable<MeetingRoom>, IEnumerable<ResourceViewModel>>(_meetingRoomRepository.GetAllByLocationId(meetingLocationId)).AsQueryable();
        }

        public void Insert(MeetingViewModel model)
        {
            
                var meeting = Mapper.Map<MeetingViewModel, Meeting>(model);


                _meetingRepository.AddOrUpdate(meeting);
                _meetingRepository.Save();

                AddMeetingAttendees(model, meeting);

                model.MeetingID = meeting.Id.ToString();                

            
        }

        public void Update(MeetingViewModel model)
        {


            var entity = _meetingRepository.GetById(new Guid(model.MeetingID));

            var meeting = Mapper.Map<MeetingViewModel, Meeting>(model);

            var updatedMeeting = Mapper.Map(meeting, entity);

            RemoveMeetingAttendees(entity, updatedMeeting);

            AddMeetingAttendees(model, entity);
                              
        }

        public void Delete(MeetingViewModel model)
        {
            _meetingRepository.Delete(new Guid(model.MeetingID));
            _meetingRepository.Save();
        }

        private void AddMeetingAttendees(MeetingViewModel model, Meeting entity)
        {
            foreach (var attendeeId in model.Attendees)
            {
                _meetingAttendeeRepository.AddOrUpdate(new MeetingAttendee
                {
                    MeetingId = entity.Id,
                    PersonId = new Guid(attendeeId)
                });
                _meetingAttendeeRepository.Save();
            }
        }

        private void RemoveMeetingAttendees(Meeting entity, Meeting updatedMeeting)
        {
            foreach (var attendee in entity.MeetingAttendees)
            {
                _meetingAttendeeRepository.Delete(attendee.Id);
                _meetingAttendeeRepository.Save();
            }


            _meetingRepository.AddOrUpdate(updatedMeeting);
        }

   
    }
}