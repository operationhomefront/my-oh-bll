﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using OH.BLL.Models.Schedulers;
using OH.DAL.Domain.Tasks;
using OH.DAL.Repositories.Tasks;

namespace OH.BLL.Services.Schedulers
{
    public class SchedulerTaskService : ISchedulerTaskService
    {
        private readonly ITaskRepository _taskRepository;

        public SchedulerTaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public IQueryable<TaskViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(_taskRepository.GetAll()).AsQueryable();
        }

        public void Insert(TaskViewModel model)
        {
            Task task = Mapper.Map<TaskViewModel, Task>(model);

            _taskRepository.AddOrUpdate(task);
            _taskRepository.Save();

            model.TaskID = task.Id.ToString();
        }

        public void Update(TaskViewModel model)
        {
            Task task = Mapper.Map<TaskViewModel, Task>(model);
            _taskRepository.AddOrUpdate(task);
            _taskRepository.Save();
        }

        public void Delete(TaskViewModel model)
        {
            _taskRepository.Delete(new Guid(model.TaskID));
            _taskRepository.Save();
        }
    }
}