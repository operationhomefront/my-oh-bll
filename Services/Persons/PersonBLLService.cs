﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Categories;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.ListTypes;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Phones;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Repositories.Addresses;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.Phones;

namespace OH.BLL.Services.Persons
{
    public class PersonBllService : IPersonBllService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IAddressBLLService _addressBLLService;
        private readonly IPersonPhoneRepository _personPhoneRepository;
        private readonly IPersonAddressRepository _personAddresRepository;
        private readonly IPersonEmailAddressRepository _personEmailAddresRepository;

        public PersonBllService(IPersonRepository personRepository, IAddressBLLService addressBLLService,
            IPersonPhoneRepository personPhoneRepository,
              IPersonAddressRepository personAddresRepository,
              IPersonEmailAddressRepository personEmailAddresRepository)
        {
            _personRepository = personRepository;
            _addressBLLService = addressBLLService;
            _personPhoneRepository = personPhoneRepository;
            _personAddresRepository = personAddresRepository;
            _personEmailAddresRepository = personEmailAddresRepository;
        }

        public PersonViewModel Get()
        {
            return new PersonViewModel();
            
        }

        public PersonAddressViewModel InitializePersonAddress()
        {
            var address = _addressBLLService.InitializeAddress();
            var model = Mapper.Map<PersonAddressViewModel>(new PersonAddress(){AddressId=address.Id});
            model.Address = address;
            return model;
        }

        public PersonAddressViewModel InitializePersonAddressByType(Guid typeId)
        {
            var model = InitializePersonAddress();
            model.AddressType = typeId;
            return model;
        }

        public PersonAddressViewModel InitializeByPersonId(Guid id)
        {
            var model = InitializePersonAddress();
            model.PersonId = id;
            return model;
        }
        public PersonAddressViewModel InitializeByPersonIdAndType(Guid pesronId,Guid typeId)
        {
            var model = InitializeByPersonId(pesronId);
            model.AddressType = typeId;
            return model;
        }

        public PersonViewModel GetFullModel(Guid id)
        {
            var person = new PersonViewModel();
            var personEntity = _personRepository.GetNoTracking(id);
            
            if (personEntity != null)
            {
                person = Mapper.Map<PersonViewModel>(_personRepository.GetNoTracking(id));

                person.Addresses = new Collection<PersonAddressViewModel>();
                person.EmailAddresses = new Collection<PersonEmailAddressViewModel>();
                person.Phones = new Collection<PersonPhoneViewModel>();

                var personPhone = _personPhoneRepository.GetByPersonId(id);
                if (personPhone != null)
                    person.PrimaryPhone = Mapper.Map< PhoneViewModel>(personPhone.Phone);

                var primaryEmail = _personEmailAddresRepository.GetByPersonId(id);
                if (primaryEmail != null)
                    person.PrimaryEmail = Mapper.Map< EmailAddressViewModel>(primaryEmail.EmailAddress);

                var mailingAddress = _personAddresRepository.GetByPersonIdAndType(id, new Guid("249EF9CC-8EC1-432D-BDDE-763F2F4DAFA2"));
                if (mailingAddress != null)
                    person.MailingAddress = Mapper.Map< PersonAddressViewModel>(mailingAddress);

                var physicalAddress = _personAddresRepository.GetByPersonIdAndType(id, new Guid("C0A9FD9A-7D1C-4C44-8B06-25D4E4503575"));
                if (physicalAddress != null)
                    person.PhysicalAddress = Mapper.Map< PersonAddressViewModel>(physicalAddress);
            }
            else
            {
                person = Mapper.Map< PersonViewModel>(new Person() { Id = Guid.NewGuid() });
            }
            return person;
        }

        public Guid AddPerson(PersonViewModel personViewModel)
        {
            Mapper.CreateMap<PersonViewModel,Person>()
                .ForMember(o => o.PrimaryEmail, d => d.Ignore())
                .ForMember(o => o.PrimaryPhone, d => d.Ignore());
                
            var person = Mapper.Map<PersonViewModel, Person>(personViewModel);
            var entity = _personRepository.GetById(person.Id);

            if (entity != null)
            {
                person = Mapper.Map(person, entity);
                _personRepository.Update(person);
            }
            else
            {
                person.Id = _personRepository.AddOrUpdate(person);
                _personRepository.Save();
    
            }
            
            return person.Id;
        }
    }
}
