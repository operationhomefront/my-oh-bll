﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Persons;

namespace OH.BLL.Services.Persons
{
    public interface IAddressBLLService
    {
        Guid AddUpdateAddress(AddressViewModel avm);
        Guid AddUpdateAddMapping(PersonAddressViewModel pavm);
        AddressViewModel InitializeAddress();
    }
}
