using System;
using OH.BLL.Models.Persons;

namespace OH.BLL.Services.Persons
{
    public interface IPersonBllService
    {
        Guid AddPerson(PersonViewModel personViewModel);
        PersonAddressViewModel InitializePersonAddress();
        PersonAddressViewModel InitializePersonAddressByType(Guid typeId);
        PersonAddressViewModel InitializeByPersonId(Guid id);
        PersonAddressViewModel InitializeByPersonIdAndType(Guid pesronId, Guid typeId);

        PersonViewModel Get();
        PersonViewModel GetFullModel(Guid id);
    }
}