﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Persons;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories.Addresses;
using OH.DAL.Repositories.Persons;

namespace OH.BLL.Services.Persons
{
    public class AddressBLLService : IAddressBLLService
    {
        private readonly IAddressRepository _addressRepository;
        private readonly IPersonAddressRepository _personAddressRepository;
        

        public AddressBLLService(IAddressRepository addressRepository, IPersonAddressRepository personAddressRepository)
        {
            _addressRepository = addressRepository;
            _personAddressRepository = personAddressRepository;
        }


        public Guid AddUpdateAddMapping(PersonAddressViewModel pavm)
        {
            var addressMapping = Mapper.Map<PersonAddressViewModel, PersonAddress>(pavm);

            //var addressVal = AddUpdateAddress(pavm.Address);

            //addressMapping.AddressId = addressVal;

            var rtn = _personAddressRepository.AddOrUpdate(addressMapping);
            _personAddressRepository.Save();

            return rtn;
        }

        public Guid AddUpdateAddress(Models.Addresseses.AddressViewModel avm)
        {
            var address = Mapper.Map<AddressViewModel,Address>(avm);
            var rtn = _addressRepository.AddOrUpdate(address);
            _addressRepository.Save();

            return rtn;
        }

        public AddressViewModel InitializeAddress()
        {
            return Mapper.Map<AddressViewModel>(new Address());

        }
    }
}
