﻿using System;
using OH.BLL.Models;
using OH.DAL.Domain;
using OH.DAL.Repositories.Events;
using OH.BLL.Models.Events;
using OH.DAL.Domain.Events;
using System.Collections.Generic;
using AutoMapper;

namespace OH.BLL.Services.Events
{
    public class CollectionEventOfficeMappingBllService : ICollectionEventOfficeMappingBllService
    {
        private readonly ICollectionEventOfficeMappingRepository _CollectionEventOfficeMappingBllServiceRepository;

        public CollectionEventOfficeMappingBllService(ICollectionEventOfficeMappingRepository CollectionEventOfficeMappingRepository)
        {
            _CollectionEventOfficeMappingBllServiceRepository = CollectionEventOfficeMappingRepository;
        }


        public CollectionEventOfficeMappingViewModel GetById(Guid id)
        {
            return Mapper.Map <CollectionEventOfficeMapping,CollectionEventOfficeMappingViewModel>(_CollectionEventOfficeMappingBllServiceRepository.GetById(id));
        }

        public IEnumerable<CollectionEventOfficeMappingViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<CollectionEventOfficeMapping>, IEnumerable<CollectionEventOfficeMappingViewModel>>(_CollectionEventOfficeMappingBllServiceRepository.GetAll());
        }

        public Guid AddOrUpdate(ICollectionEventOfficeMappingBllService ICollectionEventOfficeMappingBllServiceViewModel)
        {
            throw new NotImplementedException();
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
