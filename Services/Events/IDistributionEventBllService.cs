﻿using System;
using System.Collections.Generic;
using OH.BLL.Models;
using OH.BLL.Models.Events;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.BLL.Services.Events
{
    public interface IDistributionEventBllService
    {
        DistributionEventViewModel GetById(Guid id);

        IEnumerable<DistributionEventViewModel> GetAll();
        IEnumerable<DistributionEventViewModel> GetActiveDistributionEvents();
        IEnumerable<DistributionEventViewModel> GetClosingActiveDistributionEvents();
        IEnumerable<DistributionEventViewModel> GetClosedDistributionEvents();
        IEnumerable<DistributionEventViewModel> GetUpcomingDistributionEvents();

        Guid AddOrUpdate(DistributionEvent DistributionEventViewModel);

        void Delete(Guid id);
    }
}
