﻿using System;
using System.Collections.Generic;
using AutoMapper;
using OH.BLL.Models;
using OH.BLL.Models.Events;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;
using OH.DAL.Repositories.Events;

namespace OH.BLL.Services.Events
{
    public class DistributionEventBllService : IDistributionEventBllService
    {
        private readonly IDistributionEventRepository _DistributionEventRepository;

        public DistributionEventBllService(IDistributionEventRepository DistributionEventRepository)
        {
            _DistributionEventRepository = DistributionEventRepository;
        }

        public DistributionEventViewModel GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DistributionEventViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<DistributionEvent>, IEnumerable<DistributionEventViewModel>>(_DistributionEventRepository.GetAll());
        }

        public IEnumerable<DistributionEventViewModel> GetActiveDistributionEvents()
        {
            return Mapper.Map<IEnumerable<DistributionEvent>, IEnumerable<DistributionEventViewModel>>(_DistributionEventRepository.GetActiveDistributionEvents());
        }

        public IEnumerable<DistributionEventViewModel> GetClosingActiveDistributionEvents()
        {
            return Mapper.Map<IEnumerable<DistributionEvent>, IEnumerable<DistributionEventViewModel>>(_DistributionEventRepository.GetClosingActiveDistributionEvents());
        }

        public IEnumerable<DistributionEventViewModel> GetClosedDistributionEvents()
        {
            return Mapper.Map<IEnumerable<DistributionEvent>, IEnumerable<DistributionEventViewModel>>(_DistributionEventRepository.GetClosedDistributionEvents());
        }

        public IEnumerable<DistributionEventViewModel> GetUpcomingDistributionEvents()
        {
            return Mapper.Map<IEnumerable<DistributionEvent>, IEnumerable<DistributionEventViewModel>>(_DistributionEventRepository.GetUpcomingDistributionEvents());
        }

        public Guid AddOrUpdate(DistributionEvent DistributionEventViewModel)
        {
            throw new NotImplementedException();
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
