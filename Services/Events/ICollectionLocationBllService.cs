﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Events;
namespace OH.BLL.Services.Events
{
    public interface ICollectionLocationBllService
    {
        IEnumerable<CollectionLocationViewModel> Initialize();

    }
}
