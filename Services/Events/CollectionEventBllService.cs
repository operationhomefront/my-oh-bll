﻿using System;
using System.Collections.Generic;
using AutoMapper;
using OH.BLL.Helpers;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Events;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Schedulers;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Meetings;
using OH.DAL;
using OH.DAL.Domain.Persons;
using OH.DAL.LegacyOHv4;
using OH.DAL.Repositories.Events;
using OH.DAL.Repositories.Meetings;
using System.Collections.ObjectModel;

namespace OH.BLL.Services.Events
{
    public class CollectionEventBllService : ICollectionEventBllService
    {
        private readonly ICollectionEventRepository _collectionEventRepository;
        private readonly IOfficeRepository _officeRepository;
        private readonly ICollectionLocationRepository _collectionLocationRepository;
        private readonly ICollectionVolunteerRepository _collectionVolunteerRepository;
        private readonly ICollectionLocationMappingRepository _collectionLocationMappingRepository;
        private readonly ICollectionVolunteerMappingRepository _collectionVolunteerMappingRepository;

        public CollectionEventBllService(ICollectionEventRepository collectionEventRepository, IOfficeRepository officeRepository, ICollectionLocationRepository collectionLocationRepository,ICollectionVolunteerRepository collectionVolunteerRepository, ICollectionLocationMappingRepository collectionLocationMappingRepository )//, ICollectionVolunteerMappingRepository collectionVolunteerMappingRepository)
        {
            _collectionEventRepository = collectionEventRepository;
            _officeRepository = officeRepository;
            _collectionLocationRepository = collectionLocationRepository;
            _collectionLocationMappingRepository = collectionLocationMappingRepository;
            //_collectionVolunteerMappingRepository = collectionVolunteerMappingRepository;
            _collectionVolunteerRepository = collectionVolunteerRepository;
        }

        public CollectionEventViewModel GetById(Guid id)
        {
            var cd = _collectionEventRepository.GetById(id);
            var c= Mapper.Map<CollectionEvent, CollectionEventViewModel>(cd);
            return c;
        }

        public IEnumerable<CollectionEventViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetAll());
        }
        public IEnumerable<CollectionEventViewModel> GetAll(string officeName)
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetAll(officeName));
        }

        public IEnumerable<CollectionEventViewModel> GetAllActiveCollectionEvents()
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetAllActiveCollectionEvents());
        }
        public IEnumerable<CollectionEventViewModel> GetAllActiveCollectionEvents(string officeName)
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetAllActiveCollectionEvents(officeName));
        }

        public IEnumerable<CollectionEventViewModel> GetClosedCollectionEvents()
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetClosedCollectionEvents());
        }
        public IEnumerable<CollectionEventViewModel> GetClosedCollectionEvents(string officeName)
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetClosedCollectionEvents(officeName));
        }

        public IEnumerable<CollectionEventViewModel> GetUpcomingCollectionEvents()
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetUpcomingCollectionEvents());
        }
        public IEnumerable<CollectionEventViewModel> GetUpcomingCollectionEvents(string officeName)
        {
            return Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetUpcomingCollectionEvents(officeName));
        }

        public IEnumerable<OfficeViewModel> GetOffices()
        {
            return Mapper.Map<IEnumerable<Office>, IEnumerable<OfficeViewModel>>(_officeRepository.GetAll());
        }

        public Guid AddEvent(CollectionEventViewModel collectionEventViewModel)
        {
            //MT: I could never get this method to work for UPDATES, used a manual method instead called UpdateEvent
            var newDomain = Mapper.Map<CollectionEventViewModel, CollectionEvent>(collectionEventViewModel);
            //var existing = _collectionEventRepository.GetById(new Guid(collectionEventViewModel.MeetingID));
            //if (existing != null)
            //    newDomain = Mapper.Map(newDomain, existing);
            var c = _collectionEventRepository.AddOrUpdate(newDomain);
            _collectionEventRepository.Save();
            return c;
        }

        public Guid UpdateEvent(CollectionEventViewModel collectionEventViewModel)
        {
            var existing = _collectionEventRepository.GetById(new Guid(collectionEventViewModel.MeetingID));
            existing.Title = collectionEventViewModel.Title;
            existing.Owner = Mapper.Map<OfficeViewModel, Office>(collectionEventViewModel.Owner);
            existing.Participants = Mapper.Map<IEnumerable<OfficeViewModel>, ICollection<Office>>(collectionEventViewModel.Participants);
            existing.Partner = collectionEventViewModel.Partner;
            existing.Description = collectionEventViewModel.Description;
            existing.RegistrationRadius = collectionEventViewModel.RegistrationRadius;
            existing.MaxStoresPerVolunteer = collectionEventViewModel.MaxStoresPerVolunteer;
            existing.EventClosed = collectionEventViewModel.EventClosed;
            existing.CollectionBeginsUtc = collectionEventViewModel.CollectionBeginsUtc;
            existing.CollectionEndsUtc = collectionEventViewModel.CollectionEndsUtc;
            existing.RegistrationClosesUtc = collectionEventViewModel.RegistrationClosesUtc;
            existing.RegistrationOpensUtc = collectionEventViewModel.RegistrationOpensUtc;
            existing.PublishToEventCalendar = collectionEventViewModel.PublishToEventCalendar;
            var c = _collectionEventRepository.AddOrUpdate(existing);
            _collectionEventRepository.Save();
            return c;

        }


        //public Guid AddCollectionLocation(CollectionLocationViewModel collectionLocationViewModel)
        //{
        //    var m = _collectionLocationRepository.AddOrUpdate(Mapper.Map<CollectionLocationViewModel, CollectionLocation>(collectionLocationViewModel));
        //    _collectionLocationRepository.Save();
        //    return m;
        //}

        private Guid UpdateCollectionLocation(CollectionLocationViewModel collectionLocationViewModel)
        {
            var existing = _collectionLocationRepository.GetById(collectionLocationViewModel.Id.Value);
            existing.Name = collectionLocationViewModel.Name;
            existing.Description = collectionLocationViewModel.Description;
            existing.PointOfInterest = collectionLocationViewModel.PointOfInterest;
            existing.PointOfContact = collectionLocationViewModel.PointOfContact;
            existing.StoreNumber = collectionLocationViewModel.StoreNumber;
            existing.HoursOfOperation = collectionLocationViewModel.HoursOfOperation;
            existing.AddressValid = collectionLocationViewModel.AddressValid;
            existing.NotAvailable = collectionLocationViewModel.NotAvailable;
            existing.OpeningOnUtc = collectionLocationViewModel.OpeningOnUtc;
            existing.Address.Address1 = collectionLocationViewModel.Address.Address1;
            existing.Address.Address2 = collectionLocationViewModel.Address.Address2;
            existing.Address.City = collectionLocationViewModel.Address.City;
            existing.Address.State = collectionLocationViewModel.Address.State;
            existing.Address.Zip = collectionLocationViewModel.Address.Zip;
            var m = _collectionLocationRepository.AddOrUpdate(existing);
            _collectionLocationRepository.Save();
            return m;


        }
        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public CollectionLocationMappingViewModel AddCollectionLocationMapping(CollectionLocationMappingViewModel collectionLocationMappingViewModel)
        {
            var cl = Mapper.Map<CollectionLocationMappingViewModel, CollectionLocationMapping>(collectionLocationMappingViewModel);
            cl.CollectionLocationId = cl.CollectionLocation.Id;
            var id = _collectionLocationMappingRepository.AddOrUpdate(cl);
            _collectionLocationMappingRepository.Save();
            return this.GetCollectionLocationMappingViewModelById(id);
        }
        public CollectionLocationMappingViewModel UpdateCollectionLocationMapping(CollectionLocationMappingViewModel collectionLocationMappingViewModel)
        {
            this.UpdateCollectionLocation(collectionLocationMappingViewModel.CollectionLocation);
            return this.GetCollectionLocationMappingViewModelById(collectionLocationMappingViewModel.Id);
        }

        public CollectionLocationMappingViewModel GetCollectionLocationMappingViewModelById(Guid id)
        {
            var m=_collectionLocationMappingRepository.GetById(id);
            var v= Mapper.Map<CollectionLocationMapping, CollectionLocationMappingViewModel>(m);
            return v;
        }

        public void DeleteCollectionLocationMapping(Guid id)
        {
            _collectionLocationMappingRepository.Delete(id);
        }





        private Guid UpdateCollectionVolunteer(CollectionVolunteerViewModel collectionVolunteerViewModel)
        {
            var existing = _collectionVolunteerRepository.GetById(collectionVolunteerViewModel.Id);
            //TODO:ADD/UPDATE EDITABLE PROPERTIES HERE
            //existing.Name = collectionLocationViewModel.Name;
            //existing.Description = collectionLocationViewModel.Description;
            //existing.PointOfInterest = collectionLocationViewModel.PointOfInterest;
            //existing.PointOfContact = collectionLocationViewModel.PointOfContact;
            //existing.StoreNumber = collectionLocationViewModel.StoreNumber;
            //existing.HoursOfOperation = collectionLocationViewModel.HoursOfOperation;
            //existing.AddressValid = collectionLocationViewModel.AddressValid;
            //existing.NotAvailable = collectionLocationViewModel.NotAvailable;
            //existing.OpeningOnUtc = collectionLocationViewModel.OpeningOnUtc;
            //existing.Address.Address1 = collectionLocationViewModel.Address.Address1;
            //existing.Address.Address2 = collectionLocationViewModel.Address.Address2;
            //existing.Address.City = collectionLocationViewModel.Address.City;
            //existing.Address.State = collectionLocationViewModel.Address.State;
            //existing.Address.Zip = collectionLocationViewModel.Address.Zip;
            var m = _collectionVolunteerRepository.AddOrUpdate(existing);
            _collectionVolunteerRepository.Save();
            return m;


        }




        public CollectionVolunteerMappingViewModel AddCollectionVolunteerMapping(CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel)
        {
            var cvMappingNew = Mapper.Map<CollectionVolunteerMappingViewModel, CollectionVolunteerMapping>(collectionVolunteerMappingViewModel);
            cvMappingNew.CollectionVolunteerId = cvMappingNew.CollectionVolunteer.Id;
            //TODO:MISSING PHYSICAL ADDRESS IN DAL
            //cvMappingNew.CollectionVolunteer.PhysicalAddress.PersonId = cvMappingNew.CollectionVolunteer.Id;
            var id = _collectionVolunteerMappingRepository.AddOrUpdate(cvMappingNew);
            _collectionVolunteerMappingRepository.Save();
            return this.GetCollectionVolunteerMappingViewModelById(id);
        }
        public CollectionVolunteerMappingViewModel UpdateCollectionVolunteerMapping(CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel)
        {
            this.UpdateCollectionVolunteer(collectionVolunteerMappingViewModel.CollectionVolunteer);
            return this.GetCollectionVolunteerMappingViewModelById(collectionVolunteerMappingViewModel.Id);

        }

        public CollectionVolunteerMappingViewModel GetCollectionVolunteerMappingViewModelById(Guid id)
        {
            var m = _collectionVolunteerMappingRepository.GetById(id);
            var v = Mapper.Map<CollectionVolunteerMapping, CollectionVolunteerMappingViewModel>(m);
            return v;
        }

        public void DeleteCollectionVolunteerMapping(Guid id)
        {
            _collectionVolunteerMappingRepository.Delete(id);
        }






        public IEnumerable<CollectionVolunteerViewModel> GetByVolunteersByEventIdRadius(Guid id, int radius)
        {
            throw new NotImplementedException();
        }

        //JAKE CHANGE
        public CollectionVolunteerMappingViewModel AddOrUpdateCollectionVolunteerMapping(CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel)
        {
            return new CollectionVolunteerMappingViewModel();
        }


        public CollectionEventViewModel Initialize()
        {
            var model = Mapper.Map<CollectionEventViewModel>(new CollectionEvent());
            model.Locations = Mapper.Map < IEnumerable<CollectionLocationViewModel>>(new Collection<CollectionLocation>());
            return model;
        }
    }
}
