﻿using System;
using OH.BLL.Models;
using OH.DAL.Domain;
using OH.BLL.Models.Events;
using System.Collections.Generic;

namespace OH.BLL.Services.Events
{
    public interface ICollectionEventOfficeMappingBllService
    {
        CollectionEventOfficeMappingViewModel GetById(Guid id);

        IEnumerable<CollectionEventOfficeMappingViewModel> GetAll();

        Guid AddOrUpdate(ICollectionEventOfficeMappingBllService ICollectionEventOfficeMappingBllServiceViewModel);

        void Delete(Guid id);
    }
}
