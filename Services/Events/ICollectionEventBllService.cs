﻿using System;
using System.Collections.Generic;
using OH.BLL.Models;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Events;
using OH.BLL.Models.Schedulers;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.BLL.Services.Events
{
    public interface ICollectionEventBllService
    {
        CollectionEventViewModel GetById(Guid id);

        IEnumerable<CollectionEventViewModel> GetAll();

        IEnumerable<CollectionEventViewModel> GetAllActiveCollectionEvents();
        IEnumerable<CollectionEventViewModel> GetClosedCollectionEvents();
        IEnumerable<CollectionEventViewModel> GetUpcomingCollectionEvents();
        IEnumerable<CollectionEventViewModel> GetAll(string officeName);

        IEnumerable<CollectionEventViewModel> GetAllActiveCollectionEvents(string officeName);
        IEnumerable<CollectionEventViewModel> GetClosedCollectionEvents(string officeName);
        IEnumerable<CollectionEventViewModel> GetUpcomingCollectionEvents(string officeName);

        IEnumerable<OfficeViewModel> GetOffices();
        Guid AddEvent(CollectionEventViewModel collectionEventViewModel);
        Guid UpdateEvent(CollectionEventViewModel collectionEventViewModel);
        //Guid AddCollectionLocation(CollectionLocationViewModel collectionLocationViewModel);
        //Guid UpdateCollectionLocation(CollectionLocationViewModel collectionLocationViewModel);

        void Delete(Guid id);

        CollectionLocationMappingViewModel AddCollectionLocationMapping(CollectionLocationMappingViewModel collectionLocationMappingViewModel);
        CollectionLocationMappingViewModel UpdateCollectionLocationMapping(CollectionLocationMappingViewModel collectionLocationMappingViewModel);
        CollectionLocationMappingViewModel GetCollectionLocationMappingViewModelById(Guid id);
        void DeleteCollectionLocationMapping(Guid id);
        CollectionVolunteerMappingViewModel AddCollectionVolunteerMapping(CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel);
        CollectionVolunteerMappingViewModel UpdateCollectionVolunteerMapping(CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel);
        CollectionVolunteerMappingViewModel GetCollectionVolunteerMappingViewModelById(Guid id);
        void DeleteCollectionVolunteerMapping(Guid id);
        IEnumerable<CollectionVolunteerViewModel> GetByVolunteersByEventIdRadius(Guid id, int radius);

        CollectionVolunteerMappingViewModel AddOrUpdateCollectionVolunteerMapping(CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel);

        CollectionEventViewModel Initialize();
    }
}
