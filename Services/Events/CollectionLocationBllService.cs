﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Events;
using AutoMapper;
using OH.BLL.Models.Events;
using OH.DAL.Domain.Events;
using System.Collections.ObjectModel;
namespace OH.BLL.Services.Events
{
    public class CollectionLocationBllService : ICollectionLocationBllService
    {
        public IEnumerable<CollectionLocationViewModel> Initialize()
        {
            return Mapper.Map<IEnumerable<CollectionLocationViewModel>>(new Collection<CollectionLocation>());
        }

    }
}
