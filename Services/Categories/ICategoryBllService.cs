using System;
using System.Collections.Generic;
using OH.BLL.Models.Categories;

namespace OH.BLL.Services.Categories
{
    public interface ICategoryBllService
    {
        CategoryTreeViewModel GetCategoryTreeViewModelById(Guid categoryId);
        CategoryTreeViewModel GetCategoryTreeViewModelByOwnerId(Guid ownerId);
        CategoryViewModel GetNewCategoryViewModel();
        Guid CreateCategory(CategoryViewModel model);

        //IEnumerable<CategoryItemViewModel> GetAllCategoryItems();

        //void AddSubCategory(CategoryItemViewModel model, string newCategoryItemText);

        //void CreateCategoryItem(CategoryItemViewModel model);

        //void DeleteCategoryItem(Guid id);

        //void UpdateCategoryItem(CategoryItemViewModel model);

        //CategoryItemViewModel GetCategoryItemById(Guid id);
    }
}