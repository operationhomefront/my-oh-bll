﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using AutoMapper;
using OH.BLL.Models.Events;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Reports;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories;
using OH.DAL.Repositories.Authorization;
using OH.DAL.Repositories.Events;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Net.Mime;
using OH.BLL.Models.Applications;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.ListTypes.Person;
using OH.DAL.Domain.Reports;
using OH.DAL.Repositories.Applications;


namespace OH.BLL.Services.Reports
{
    public class ReportsBllService : IReportsBllService
    {
        private readonly IReportRepository _reportRepository;
        private readonly ICollectionEventRepository _collectionEventRepository;
        private readonly IDistributionEventRepository _distributionEventRepository;
        private readonly IMyOhUserRepository _myOhUserRepository;
        private readonly IMyOhRoleRepository _myOhRoleRepository;
        private readonly IDistributionLocationRepository _myOhDistributionLocationRepository;
        private readonly IApplicationRepository _applicationRepository;

        public ReportsBllService(IReportRepository reportRepository, ICollectionEventRepository collectionEventRepository, IDistributionEventRepository distributionEventRepository, IMyOhUserRepository myOhUserRepository, IMyOhRoleRepository myOhRoleRepository, IDistributionLocationRepository myOhDistributionLocationRepository, IApplicationRepository applicationRepository )
        {
            _reportRepository = reportRepository;
            _collectionEventRepository = collectionEventRepository;
            _distributionEventRepository = distributionEventRepository;
            _myOhUserRepository = myOhUserRepository;
            _myOhRoleRepository = myOhRoleRepository;
            _myOhDistributionLocationRepository = myOhDistributionLocationRepository;
            _applicationRepository = applicationRepository;
        }

        public IEnumerable<PersonViewModel> GetAllPersons()
        {
            var persons = Mapper.Map<IEnumerable<Person>, IEnumerable<PersonViewModel>>(_reportRepository.GetAllPersons());

            return persons;
        }

        public IEnumerable<ApplicationViewModel> GetAllApplications()
        {
            return
                Mapper.Map<IEnumerable<Application>, IEnumerable<ApplicationViewModel>>(_applicationRepository.GetAll());
        } 

        public IEnumerable<ReportViewModel> GetAllReports()
        {
          return  Mapper.Map<IEnumerable<Report>,IEnumerable<ReportViewModel>>(_reportRepository.GetAllAvailableReports());
        }

        public IEnumerable<ReportViewModel> GetAllReportsByAppId(Guid applicationId)
        {
            return Mapper.Map<IEnumerable<Report>, IEnumerable<ReportViewModel>>(_reportRepository.GetAllAvailableReportsById(applicationId));
        }


        public IEnumerable<PersonViewModel> GetAllPersonsByFirstName(string firstName)
        {
            var persons = Mapper.Map<IEnumerable<Person>, IEnumerable<PersonViewModel>>(_reportRepository.GetAllPersonsByFirstName(firstName));

            return persons;
        }

        public IEnumerable<CollectionEventViewModel> GetAllActiveCollectionEvents()
        {
            return Mapper.Map<IEnumerable<CollectionEvent>,IEnumerable<CollectionEventViewModel>>(_collectionEventRepository.GetAllActiveCollectionEvents());
        }

        public RallyPointDash BuildRallyPointDash(RallyPointDash rpdDash)
        {
            rpdDash.CollectionEvents =
                Mapper.Map<IEnumerable<CollectionEvent>, IEnumerable<CollectionEventViewModel>>(
                    _collectionEventRepository.GetAll()).ToList();

            rpdDash.DistributionEvents = Mapper.Map<IEnumerable<DistributionEvent>, IEnumerable<DistributionEventViewModel>>(_distributionEventRepository.GetAll()).ToList();

            return rpdDash;
        }

        public HovDashBoard GetHovDashBoard()
        {
            HovDashBoard hd = new HovDashBoard();

            _reportRepository.GetUserRoles();
            //var p = _myOhUserRepository.GetAllUsers();

            return hd;
        }

        public DataTable GetMafaReport(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce)
        {
            var mafaReport = _reportRepository.GetMafaReport(bgnDateTime, endDateTime, includeArmy, includeNavy, includeMarines, includeAirForce);

            return mafaReport;
        }

        public DataTable BuildReportFromStoredProcedure(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce, string includeReport)
        {
            var dTable = _reportRepository.BuildGridReport(bgnDateTime, endDateTime, includeArmy, includeNavy, includeMarines, includeAirForce, includeReport);

            return dTable;
        }

        public void DistributionRegistrationTable(Guid eventId)
        {
            //var cellsLicense = new License();
            //cellsLicense.SetLicense("Aspose.Total.lic");

            //var directoryPath = Server.MapPath("/tmp");
            //if (!Directory.Exists(directoryPath))
            //    Directory.CreateDirectory(directoryPath);

            //FileExtension.PurgeOldFiles(directoryPath, 60, ".xlsx");

            //var workbook = new Workbook();

            //OHv4Context db = new OHv4Context();

            //// JAKE COMMENT OUT var distEvent = EFctx.DistributionEvents.SingleOrDefault(x => x.Id == eventId);
            //var distEvent = Mapper.Map<DistributionEvent, DistributionEventViewModel>(_distributionEventRepository.GetById(eventId));



            ////// JAKE COMMENT OUT 
            ////var locations = (from lo in db.DistributionLocations
            ////                 where lo.DistributionEvent.Id == eventId
            ////                 select lo).ToList();


            ////var locations = Mapper.Map<DistributionLocation, DistributionLocationViewModel>(_myOhDistributionLocationRepository.GetById(eventId));

            //var locations = Mapper.Map<IEnumerable<DistributionLocation>, IEnumerable<DistributionLocationViewModel>>(_myOhDistributionLocationRepository.GetLocationsByEventId(eventId));

            
            //if (distEvent != null)

            //    // JAKE COMMENT OUT
            //    //foreach (var disLoc in locations.Where(x => x.Registrants.Count > 0))    
            //    foreach (var disLoc in locations.Where(x => x.Registrants.Count() > 0))               
            //    {
            //        //loading the registrants

            //        // JAKE COMMENT OUT
            //       // var locRegistrants = EFctx.GetDistributionRegistrantsByLocation(disLoc.Id).ToList();
            //        var locRegistrants =
            //            _myOhDistributionLocationRepository.GetLocationsByEventId(distEvent.MeetingID).ToList();

            //        var worksheet = workbook.Worksheets.Add("Sheet 1");

            //        worksheet.Name = BuildSheetName(disLoc.Name + "_" + disLoc.StartDateTime.ToString("MM-dd"));
            //        #region location stuff

            //        var columnHeaders = (from s in EFctx.FormValues
            //                             where s.EventId == disLoc.DistributionEvent.Id
            //                                   && s.Form.Id == disLoc.DistributionEvent.FormNbr
            //                             select new { s.DataName }).Distinct();


            //        //Instantiating a "Report" DataTable object
            //        var reportTable = new DataTable("Report");

            //        int registrantColumnCount = 0;
            //        int formFieldColumnCount = 0;
            //        int serviceMemberColumnCount = 0;
            //        int dependent1ColumnCount = 0;




            //        //Add Registrant Columns
            //        reportTable.Columns.Add("RegisteredOn", typeof(string));
            //        registrantColumnCount++;
            //        reportTable.Columns.Add("RegistrationStatus", typeof(string));
            //        registrantColumnCount++;

            //        if (disLoc.EnableTimeSlots)
            //        {
            //            reportTable.Columns.Add("RegistrationTimeSlot", typeof(string));
            //            registrantColumnCount++;
            //        }
            //        reportTable.Columns.Add("RegistrantLastName", typeof(string));
            //        registrantColumnCount++;
            //        reportTable.Columns.Add("RegistrantFirstName", typeof(string));
            //        registrantColumnCount++;
            //        reportTable.Columns.Add("RegistrantEmail", typeof(string));
            //        registrantColumnCount++;
            //        reportTable.Columns.Add("RegistrantPrimaryPhone", typeof(string));
            //        registrantColumnCount++;


            //        //Add Form Builder Columns
            //        foreach (var dataName in columnHeaders)
            //        {
            //            reportTable.Columns.Add(dataName.DataName, typeof(string));
            //            formFieldColumnCount++;
            //        }

            //        //Add Dependent Count
            //        reportTable.Columns.Add("DependentCount", typeof(string));
            //        dependent1ColumnCount++;

            //        //Add Dependents Info
            //        var dependentColumnCount = disLoc.Registrants.Select(x => x.OhMember.StdProfile.Dependents.Count).Max();
            //        for (int i = 1; i <= dependentColumnCount; i++)
            //        {
            //            reportTable.Columns.Add("Dep_" + i + "_Relationship", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_FirstName", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_MiddleInitial", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_LastName", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_Grade", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_LegalDependent", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_DateOfBirth", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_Gender", typeof(string));
            //            dependent1ColumnCount++;
            //            reportTable.Columns.Add("Dep_" + i + "_ShirtSize", typeof(string));
            //            dependent1ColumnCount++;
            //        }


            //        //Add Row Data
            //        //locRegistrants
            //        foreach (var reg in locRegistrants.OrderBy(x => x.RegistrationStatus).ThenBy(x => x.TimeSlot).ThenBy(x => x.RegistrantLastName))
            //        {
            //            var dr = reportTable.NewRow();
            //            dr["RegisteredOn"] = reg.TimeStamp_LastModifiedDate.ToString();

            //            if (disLoc.EnableTimeSlots)
            //            {
            //                dr["RegistrationTimeSlot"] = reg.TimeSlot == SqlDateTime.MinValue ? "N/A" : reg.TimeSlot.ToShortTimeString();
            //            }

            //            if (Enum.IsDefined(typeof(DistributionMemberRegistrationStatus), Convert.ToInt32(reg.RegistrationStatus)))
            //            {
            //                DistributionMemberRegistrationStatus testStatus = (DistributionMemberRegistrationStatus)Convert.ToInt32(reg.RegistrationStatus);
            //                dr["RegistrationStatus"] = DistributionMemberRegistrationStatuses.Name(testStatus);
            //            }
            //            else
            //            {
            //                dr["RegistrationStatus"] = "Unknown";
            //            }
            //            dr["RegistrantLastName"] = reg.RegistrantLastName;
            //            dr["RegistrantFirstName"] = reg.RegistrantFirstName;
            //            dr["RegistrantEmail"] = reg.RegistrantEmail;
            //            dr["RegistrantPrimaryPhone"] = reg.RegistrantPrimaryPhone.ToPhone();

            //            //add form values to data row
            //            FormValueCollect(Convert.ToInt32(reg.OhMember_Id), dr, disLoc);
            //            //collect dependents

            //            var regDependents = EFctx.GetDistributionRegistrantDependents(reg.RegistrantId).ToList();
            //            //Add Dependent Rows
            //            int totCount = regDependents.Count();
            //            dr["DependentCount"] = totCount;
            //            var depi = 1;
            //            foreach (var dep in regDependents)
            //            {
            //                if (Enum.IsDefined(typeof(Relationship), Convert.ToInt32(dep.Relationship)))
            //                {
            //                    Relationship testRelationship = (Relationship)Convert.ToInt32(dep.Relationship);
            //                    dr["Dep_" + depi + "_Relationship"] = RelationshipTypes.Name(testRelationship);
            //                }
            //                else
            //                {
            //                    dr["Dep_" + depi + "_Relationship"] = "Unknown";
            //                }

            //                dr["Dep_" + depi + "_FirstName"] = dep.FirstName;
            //                dr["Dep_" + depi + "_MiddleInitial"] = dep.MiddleInitial;
            //                dr["Dep_" + depi + "_LastName"] = dep.LastName;
            //                if (Enum.IsDefined(typeof(Grade), Convert.ToInt32(dep.Grade)))
            //                {
            //                    Grade tesGrade = (Grade)Convert.ToInt32(dep.Grade);
            //                    dr["Dep_" + depi + "_Grade"] = GradeLevels.Name(tesGrade);
            //                }
            //                else
            //                {
            //                    dr["Dep_" + depi + "_Grade"] = "Unknown";
            //                }
            //                dr["Dep_" + depi + "_LegalDependent"] = dep.LegalDependent.ToString() == "True" ? "yes" : "no";
            //                dr["Dep_" + depi + "_DateOfBirth"] = dep.DOB.ToShortDateString();
            //                dr["Dep_" + depi + "_Gender"] = dep.Sex;
            //                if (Enum.IsDefined(typeof(ShirtSize), Convert.ToInt32(dep.ShirtSize)))
            //                {
            //                    ShirtSize testShirtSize = (ShirtSize)Convert.ToInt32(dep.ShirtSize);
            //                    dr["Dep_" + depi + "_ShirtSize"] = ShirtSizeTypes.Name(testShirtSize);
            //                }
            //                else
            //                {
            //                    dr["Dep_" + depi + "_ShirtSize"] = "Unknown";
            //                }
            //                depi++;
            //            }
            //            if (dependentColumnCount - totCount > 0)
            //            {
            //                for (int i = 1; i < (dependentColumnCount - totCount); i++)
            //                {
            //                    dr["Dep_" + depi + "_Relationship"] = "";
            //                    dr["Dep_" + depi + "_FirstName"] = "";
            //                    dr["Dep_" + depi + "_MiddleInitial"] = "";
            //                    dr["Dep_" + depi + "_LastName"] = "";
            //                    dr["Dep_" + depi + "_LegalDependent"] = "";
            //                    dr["Dep_" + depi + "_DateOfBirth"] = "";
            //                    dr["Dep_" + depi + "_Gender"] = "";
            //                    dr["Dep_" + depi + "_ShirtSize"] = "";
            //                }
            //            }
            //            reportTable.Rows.Add(dr);
            //        }
            //        //Accessing the first row of the worksheet
            //        worksheet.Cells.ImportDataTable(reportTable, true, "A1");

            //        worksheet.Cells.InsertRow(0);
            //        //GET COLUMNN COUNT FROM THE REGISTRANT SECTION
            //        int wbII = workbook.Styles.Add();
            //        var style1 = workbook.Styles[wbII];

            //        style1.HorizontalAlignment = TextAlignmentType.CenterAcross;
            //        style1.ForegroundColor = Color.FromArgb(0, 176, 80);
            //        style1.Pattern = BackgroundType.Solid;
            //        style1.Font.IsBold = true;
            //        style1.Font.Color = Color.White;

            //        var styleFlag1 = new StyleFlag();
            //        styleFlag1.All = true;
            //        styleFlag1.CellShading = true;
            //        styleFlag1.FontBold = true;

            //        worksheet.Cells.Merge(0, 0, 1, registrantColumnCount);
            //        worksheet.Cells[0].Value = "REGISTRANT INFORMATION";
            //        worksheet.Cells[0, 0].GetMergedRange().ApplyStyle(style1, styleFlag1);


            //        //GET COLUMNN COUNT FROM THE REGISTRANT SECTION


            //        int wbIII = workbook.Styles.Add();
            //        var style2 = workbook.Styles[wbIII];

            //        style2.HorizontalAlignment = TextAlignmentType.CenterAcross;
            //        style2.ForegroundColor = Color.FromArgb(166, 166, 166);
            //        style2.Pattern = BackgroundType.Solid;
            //        style2.Font.IsBold = true;
            //        style2.Font.Color = Color.White;

            //        var styleFlag2 = new StyleFlag();
            //        styleFlag2.All = true;
            //        styleFlag2.CellShading = true;
            //        styleFlag2.FontBold = true;

            //        worksheet.Cells.Merge(0, registrantColumnCount, 1, formFieldColumnCount);
            //        worksheet.Cells[0, registrantColumnCount].Value = "OTHER FORM FIELDS";
            //        worksheet.Cells[0, registrantColumnCount].GetMergedRange().ApplyStyle(style2, styleFlag2);

            //        if (dependent1ColumnCount > 0)
            //        {
            //            int wbIV = workbook.Styles.Add();
            //            var style3 = workbook.Styles[wbIV];

            //            style3.HorizontalAlignment = TextAlignmentType.CenterAcross;
            //            style3.ForegroundColor = Color.FromArgb(255, 124, 128);
            //            style3.Pattern = BackgroundType.Solid;
            //            style3.Font.IsBold = true;
            //            style3.Font.Color = Color.White;

            //            var styleFlag3 = new StyleFlag();
            //            styleFlag3.All = true;
            //            styleFlag3.CellShading = true;
            //            styleFlag3.FontBold = true;

            //            if (!worksheet.Cells[0, registrantColumnCount + formFieldColumnCount].IsMerged)
            //            {
            //                worksheet.Cells.Merge(0, registrantColumnCount + formFieldColumnCount, 1, dependent1ColumnCount);
            //                worksheet.Cells[0, registrantColumnCount + formFieldColumnCount].Value = "DEPENDENT INFORMATION";
            //                worksheet.Cells[0, registrantColumnCount + formFieldColumnCount].GetMergedRange().ApplyStyle(style3, styleFlag3);
            //            }
            //        }




            //        Row row0 = worksheet.Cells.Rows[0];
            //        row0.Height = 18;


            //        Row row = worksheet.Cells.Rows[1];

            //        //Adding a new Style to the styles collection of the Excel object
            //        int wbI = workbook.Styles.Add();

            //        //Accessing the newly added Style to the Excel object
            //        var style = workbook.Styles[wbI];
            //        //Getting the Style object for the row
            //        //Aspose.Cells.Style style = row.GetStyle();

            //        //Setting Style properties
            //        //1. Setting the type of border line to Thick
            //        //style.SetBorderLine(BorderType.Right, BorderLineType.Thick);

            //        //2. Setting the color of the border to Blue
            //        //style.SetBorder(BorderType.RightBorder, Color.Blue);

            //        //3. Setting the horizontal alignment to Centered
            //        //style.HAlignment = HorizontalAlignmentType.Centred;

            //        //4. Setting the background color to Yellow
            //        style.ForegroundColor = Color.Yellow;
            //        style.Pattern = BackgroundType.Solid;
            //        style.Font.IsBold = true;

            //        //Setting the style of the row with the customized Style object
            //        var styleFlag = new StyleFlag();
            //        styleFlag.All = true;
            //        styleFlag.CellShading = true;
            //        styleFlag.FontBold = true;
            //        //Assigning the Style object to the Style property of the row
            //        row.ApplyStyle(style, styleFlag);
            //        worksheet.AutoFitColumns();


            //        #endregion

            //    }
            //try
            //{
            //    // save a copy in tmp
            //    var filePath = directoryPath;
            //    if (distEvent != null)
            //    {
            //        var filename = String.Format("{0}\\{1}_{2}.xlsx", filePath, BuildSheetName(distEvent.Title), Guid.NewGuid().ToString("D"));
            //        workbook.Save(filename);

            //        // send as excel document to browser
            //        workbook.Worksheets.RemoveAt("Sheet1");
            //        filename = String.Format("{0}_{1}", BuildSheetName(distEvent.Title), DateTime.Now.ToString("s").Replace(":", "-"));
            //        workbook.Save(System.Web.HttpContext.Current.Response, filename, ContentDisposition.Attachment, new XlsSaveOptions());
            //    }
            //}
            //catch (Exception ex)
            //{
            //    OHLogging.LogMessage(OHLogging.LogMsgLevel.Info, String.Format("Error detected while saving spreadsheet.  Error: {0}", ex.Message));
            //}
            
        }
    }
}
