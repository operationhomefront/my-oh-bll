﻿
using System;
using System.Collections;
using System.Collections.Generic;
using OH.BLL.Models.Events;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Reports;
using OH.DAL.Domain.Events;
using System.Data;
using OH.BLL.Models.Applications;
using OH.DAL.Domain.Reports;

namespace OH.BLL.Services.Reports
{
    public interface IReportsBllService
    {
        IEnumerable<PersonViewModel> GetAllPersonsByFirstName(string firstName);
        IEnumerable<PersonViewModel> GetAllPersons();
        IEnumerable<CollectionEventViewModel> GetAllActiveCollectionEvents();

        RallyPointDash BuildRallyPointDash(RallyPointDash rpdDash);

        HovDashBoard GetHovDashBoard();

        void DistributionRegistrationTable(Guid eventId);

        DataTable GetMafaReport(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce);

        IEnumerable<ReportViewModel> GetAllReports();
        IEnumerable<ReportViewModel> GetAllReportsByAppId(Guid applicationId);

        DataTable BuildReportFromStoredProcedure(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce, string includeReport);
        IEnumerable<ApplicationViewModel> GetAllApplications();
    }
}
