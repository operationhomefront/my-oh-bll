﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Households;

namespace OH.BLL.Services.Households
{
    public interface IHouseholdBLLService
    {
        void AddHouseholdMember(HouseholdMemberViewModel model);
        Guid AddUpdateHousehold(HouseholdViewModel model);
        
        HouseholdViewModel GetHousehold(Guid id);
        HouseholdViewModel GetHouseholdByMemberId(Guid id);
        void RemoveMember(Guid id);
        void UpdateHouseholdMember(HouseholdMemberViewModel model);
        bool HouseholdMemberExits(Guid memberId, Guid householdId);

        List<Guid> GetHouseholdMilitaryMembers(Guid houseHoldId);

    }
}
