﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using OH.BLL.Models.Households;
using OH.BLL.Services.ProgramProfiles.Military;
using OH.DAL.Repositories.Households;
using OH.DAL.Domain.Households;

namespace OH.BLL.Services.Households
{
    public class HouseholdBLLService : IHouseholdBLLService
    {
        private readonly IHouseholdMemberRepository _houseHoldMemberRepository;
        private readonly IHouseholdRepository _houseHoldRepository;
        private readonly IPersonMilitaryBLLService _personMilitary;

        public HouseholdBLLService(IHouseholdMemberRepository houseHoldMemberRepository,
            IHouseholdRepository houseHoldRepositoryRepository,
            IPersonMilitaryBLLService personMilitary
            )
        {
            _houseHoldMemberRepository=houseHoldMemberRepository;
            _houseHoldRepository = houseHoldRepositoryRepository;
            _personMilitary = personMilitary;
        }
        public HouseholdViewModel GetHousehold(Guid id)
        {
            var unmapped = _houseHoldRepository.GetById(id);
            return Mapper.Map<HouseholdViewModel>(unmapped);
        }

        public HouseholdViewModel GetHouseholdByMemberId(Guid id)
        {
            var unmapped = _houseHoldRepository.GetByMemberId(id);
            return Mapper.Map<HouseholdViewModel>(unmapped.FirstOrDefault());
            
        }

        public List<Guid> GetHouseholdMilitaryMembers(Guid houseHoldId)
        {
            var household = _houseHoldRepository.GetById(houseHoldId);
            List<Guid> servicemembers = new List<Guid>();
            if (household == null)
                return servicemembers;
            else
            {
                

                household.HouseholdMembers.ToList().ForEach(a =>
                {
                    //check for value in profile military table
                    var profiles = _personMilitary.GetByPersonId(a.Person.Id);
                    if ( profiles != null)
                        servicemembers.Add(a.Person.Id);

                });
            }
            return servicemembers;
        }



        public void AddHouseholdMember(HouseholdMemberViewModel model)
        {
            var mappedEntity = Mapper.Map< HouseholdMember>(model);

            _houseHoldMemberRepository.AddMember(mappedEntity);
            _houseHoldMemberRepository.Save();
        }

        public void RemoveMember(Guid id)
        {
            _houseHoldMemberRepository.RemoveMember(id);
            _houseHoldRepository.Save();
        }


        public Guid AddUpdateHousehold(HouseholdViewModel model)
        {
            /*****************************/
            /***** Mailing Address ******/
            Guid rtnVal = Guid.Empty;
            var mapped = Mapper.Map<Household>(model);

            var mappedEntity = _houseHoldRepository.GetById(mapped.Id);
            if (mappedEntity != null)
            {
                _houseHoldRepository.UpdateHousehold(mapped);
                model.HouseholdMembers.ToList().ForEach(m => UpdateHouseholdMember(m));
            }
            else
            {
                _houseHoldRepository.AddHousehold(mapped);
                model.HouseholdMembers.ToList().ForEach(m => AddHouseholdMember(m));
            }

            return rtnVal;

        }

        public void UpdateHouseholdMember(HouseholdMemberViewModel model)
        {

            var mappedEntity = Mapper.Map<HouseholdMember>(model);
            _houseHoldMemberRepository.UpdateMember(mappedEntity);
        }
        public bool HouseholdMemberExits(Guid memberId,Guid householdId)
        {

            return _houseHoldMemberRepository.HouseholdMemberExists(memberId, householdId);
        }
    }
}
