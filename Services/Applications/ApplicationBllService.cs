﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.Applications;
using OH.DAL.Domain.Applications;
using OH.DAL.Repositories.Applications;

namespace OH.BLL.Services.Applications
{
    public class ApplicationBllService : IApplicationBllService
    {
        private readonly IApplicationRepository _applicationRepository;

        public ApplicationBllService(IApplicationRepository applicationRepository)
        {
            _applicationRepository = applicationRepository;
        }

        public IEnumerable<ApplicationViewModel> GetAll()
        {
            return
                Mapper.Map<IEnumerable<Application>, IEnumerable<ApplicationViewModel>>(_applicationRepository.GetAll());
        }

        public ApplicationViewModel GetById(Guid id)
        {
            return Mapper.Map<Application, ApplicationViewModel>(_applicationRepository.GetById(id));
        }
    }
}
