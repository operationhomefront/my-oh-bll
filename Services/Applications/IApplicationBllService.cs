﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Applications;

namespace OH.BLL.Services.Applications
{
    public interface IApplicationBllService
    {
        IEnumerable<ApplicationViewModel> GetAll();

        ApplicationViewModel GetById(Guid id);
    }
}
