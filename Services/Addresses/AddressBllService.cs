﻿using System;
using OH.BLL.Helpers;
using OH.BLL.Models.Addresseses;
using OH.DAL.LegacyOHv4;
using OH.DAL.Repositories;

namespace OH.BLL.Services.Addresses
{
    public class AddressBllService : IAddressBllService
    {
        private readonly IUSZipCodeRepository _usZipCodeRepository;

        public AddressBllService(IUSZipCodeRepository usZipCodeRepository)
        {
            _usZipCodeRepository = usZipCodeRepository;
        }
        public AddressViewModel VerifyCityStateZip(string zip)
        {
            if (zip == null) throw new ArgumentNullException("zip");

            var addressInfo = new AddressViewModel();
            if (!String.IsNullOrEmpty(zip))
            {
                var subZip = zip.ToZip5();
                try
                {
                    var zipInfo = _usZipCodeRepository.GetSingleUsZipCode(subZip);
                    addressInfo.City = zipInfo.City;
                    addressInfo.State = zipInfo.State;
                }
                catch (Exception)
                {
                    addressInfo.City = "";
                    addressInfo.State = "";
                }
            }
            else
            {
                addressInfo.City = "";
                addressInfo.State = "";
            }
            return addressInfo;
        }
    }
}
