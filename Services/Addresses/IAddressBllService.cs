﻿using System;
using System.Collections.Generic;
using OH.BLL.Models;
using OH.BLL.Models.Addresseses;
using OH.DAL.Domain;

namespace OH.BLL.Services.Addresses
{
    public interface IAddressBllService
    {
        AddressViewModel VerifyCityStateZip(string zip);
    }
}
