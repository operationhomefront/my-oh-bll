﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.DAL.Domain.ProgramProfile.Caregiver;

namespace OH.BLL.Services.ProgramProfiles.Caregiver
{
    public interface IProgramProfileCaregiverBLLService
    {
        void AddUpdateProfileCaregiver(ProfileCaregiver model);
        ProfileCaregiverViewModel Initialize();
        ProfileCaregiverViewModel Initialize(Guid caregiverId,string EmailAddress);

        bool Save(ProfileCaregiverViewModel model);
    }
}
