﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.Caregiver;

namespace OH.BLL.Services.ProgramProfiles.Caregiver
{
    public interface  ICaregiverBLLService
    {
        void Save(CareGiverViewModel profileCaregiver);
        CareGiverViewModel GetCareGiver(Guid id);
    }
}
