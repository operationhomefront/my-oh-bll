﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Phones;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Services.Persons;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Repositories.Addresses;
using OH.DAL.Repositories.EmailAddresses;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.Phones;
using OH.DAL.Repositories.ProgramProfiles.CareGiver;

namespace OH.BLL.Services.ProgramProfiles.Caregiver
{
    public class CaregiverBLLService : ICaregiverBLLService
    {
        private readonly IPersonCaregiverRepository _personCaregiverRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IPersonAddressRepository _personAddresRepository;
        private readonly IPersonEmailAddressRepository _personEmailAddresRepository;
        private readonly IPersonPhoneRepository _personPhoneRepository;
        private readonly IAddressRepository _addresRepository;
        private readonly IPhoneRepository _phoneRepository;
        private readonly IEmailAddressRepository _emailAddressRepository;
        private readonly IPersonBllService _personBllService;


        public CaregiverBLLService(IPersonCaregiverRepository personCaregiverRepository, 
            IPersonPhoneRepository personPhoneRepository,
            IPersonRepository personRepository, 
            IPhoneRepository phoneRepository,
            IPersonAddressRepository personAddresRepository,
            IAddressRepository addresRepository,
             IPersonEmailAddressRepository personEmailAddresRepository,
            IEmailAddressRepository emailAddressRepository,
            IPersonBllService personBllService
            )
        {
            _personCaregiverRepository = personCaregiverRepository;
            _personAddresRepository = personAddresRepository;
            _personRepository = personRepository;
            _addresRepository = addresRepository;
            _personPhoneRepository = personPhoneRepository;
            _phoneRepository = phoneRepository;
            _emailAddressRepository = emailAddressRepository;
            _personEmailAddresRepository = personEmailAddresRepository;
            _personBllService = personBllService;

        }
        public void Save(CareGiverViewModel profileCaregiver)
        {
            Mapper.CreateMap<CareGiverViewModel, OH.DAL.Domain.Persons.PersonCaregiver>()
                .ForMember(o => o.Person, d => d.Ignore())
                .ForMember(o => o.ClusterId, d => d.Ignore());

            var personCaregiver = Mapper.Map<CareGiverViewModel, OH.DAL.Domain.Persons.PersonCaregiver>(profileCaregiver);
            var personCaregiverEntity = _personCaregiverRepository.GetMy(personCaregiver.Id);

            if (personCaregiverEntity != null)
            {
                var m = _personRepository.Update(Mapper.Map<Person>(profileCaregiver.Person));
                personCaregiverEntity = Mapper.Map(personCaregiver, personCaregiverEntity);
                _personCaregiverRepository.UpdateCaregiver(personCaregiverEntity);
            }
            else
            {
                //double check for the person record
                var personUnknown = _personBllService.AddPerson(profileCaregiver.Person);

                //personCaregiver.Person = Mapper.Map<PersonViewModel,Person>(profileCaregiver.Person);
                personCaregiver.Person = null;
                
                personCaregiver.PersonId = personUnknown;

                _personCaregiverRepository.AddOrUpdate(personCaregiver);
                _personCaregiverRepository.Save();
            }

           
            /*****************************/
            /***** Person Emails *********/

            var mappedEmailCollection = Mapper.Map<ICollection<PersonEmailAddressViewModel>, ICollection<PersonEmailAddress>>(profileCaregiver.Person.EmailAddresses);

            foreach (PersonEmailAddress p in mappedEmailCollection)
            {
                //fist check for the email add in the entity
                var unMappedEmailEntity = _emailAddressRepository.GetById(p.EmailAddress.Id);
                if (unMappedEmailEntity != null)
                {
                    _emailAddressRepository.UpdateEmailAddres(p.EmailAddress);
                }
                else
                {
                    _personEmailAddresRepository.AddOrUpdate(p);
                    _personEmailAddresRepository.Save();
                }
            }


            /*****************************/
            /***** Person Phones *********/

            var mappedPhone = Mapper.Map<ICollection<PersonPhoneViewModel>, ICollection<PersonPhone>>(profileCaregiver.Person.Phones);

            foreach (PersonPhone p in mappedPhone)
            {
                var unMappedEntity = _phoneRepository.GetById(p.Phone.Id);
                if (unMappedEntity != null)
                {
                    _phoneRepository.UpdatePhone(p.Phone);
                }
                else
                {
                    _personPhoneRepository.AddOrUpdate(p);
                    _personPhoneRepository.Save();
                }
            }

            /*****************************/
            /***** Mailing Address ******/
            var mapped = Mapper.Map<PersonAddressViewModel, PersonAddress>(profileCaregiver.Person.MailingAddress);
            
            var mappedEntity = _personAddresRepository.GetById(mapped.Id);
            if (mappedEntity != null)
            {
                var internalmapped = Mapper.Map<AddressViewModel, Address>(profileCaregiver.Person.MailingAddress.Address);
                var addressUpdate = Mapper.Map(internalmapped, mappedEntity.Address);
                _addresRepository.UpdateAddress(addressUpdate);
            }
            else
            {
                _personAddresRepository.AddOrUpdate(mapped);
                _personAddresRepository.Save();
            }

            /*****************************/
            /***** Physical Address ******/
            var mapped1 = Mapper.Map<PersonAddressViewModel, PersonAddress>(profileCaregiver.Person.PhysicalAddress);
            var mappedEntity1 = _personAddresRepository.GetById(mapped1.Id);
            if (mappedEntity != null)
            {
                var addressUpdate1 = Mapper.Map(mapped1.Address, mappedEntity1.Address);
                _addresRepository.UpdateAddress(addressUpdate1);
            }
            else
            {
                _personAddresRepository.AddOrUpdate(mapped1);
                _personAddresRepository.Save();
            }
           
        }

        public CareGiverViewModel GetCareGiver(Guid id)
        {
            var caregiverEntity = _personCaregiverRepository.GetNoTrackingByPerson(id);
            var mapped = new CareGiverViewModel(id);

            if (caregiverEntity != null)
            {

                mapped = Mapper.Map<PersonCaregiver, CareGiverViewModel>(caregiverEntity);
                
                //need adresses
                //need phone
                //email address
            }
            
            mapped.Person = _personBllService.GetFullModel(id);
            
            return mapped;
        }
    }
}
