﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.Households;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Phones;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Services.Households;
using OH.BLL.Services.ListTypes;
using OH.BLL.Services.Persons;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Households;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Repositories.Categories;
using OH.DAL.Repositories.EmailAddresses;
using OH.DAL.Repositories.Households;
using OH.DAL.Repositories.ListTypes.Person;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.ProgramProfiles.CareGiver;
using OH.DAL.Repositories.WorkFlow;

namespace OH.BLL.Services.ProgramProfiles.Caregiver
{
    public class ProgramProfileCaregiverBLLService : IProgramProfileCaregiverBLLService
    {
        private IProfileCaregiverRepository _caregiverRepository;
       
        private IPersonBllService _personBLLService;
        private ICaregiverBLLService _caregiverBllService;
        private IEmailAddressRepository _emailAddressRepository;
        private readonly IPersonAddressRepository _personAddresRepository;

        private readonly IListTypesBLLService _listTypeService;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IPersonAddressRepository _addressRepository;
        private readonly IProfileCaregiverRepository _profileCaregiverRepository;
        private readonly IHouseholdRepository _houseHoldRepository;
        private readonly IHouseholdBLLService _householdBllService;
        private readonly IWorkFlowItemRepository _workFlowItemRepository;
        private readonly IProfileCaregiverAttachmentRepository _caregiverAttachmentRepository;
        private readonly ICategoryAttachmentRepository _categoryAttachmentRepository;

        public ProgramProfileCaregiverBLLService(ICaregiverBLLService caregiverBllService,
            IGenderRepository genderRepository, 
            IPersonBllService personBLLService,
            IProfileCaregiverRepository caregiverRepository, 
            IPersonAddressRepository personAddresRepository,
            IEmailAddressRepository emailAddressRepository, IListTypesBLLService listTypeService, ICategoryRepository categoryRepository, 
            IPersonAddressRepository addressRepository,
            IProfileCaregiverRepository profileCaregiverRepository,
            IHouseholdRepository houseHoldRepository,
            IHouseholdBLLService householdBllService,
            IWorkFlowItemRepository workFlowItemRepository,
            IProfileCaregiverAttachmentRepository caregiverAttachmentRepository,
            ICategoryAttachmentRepository categoryAttachmentRepository)
        {
            _caregiverRepository = caregiverRepository;
            _caregiverBllService = caregiverBllService;
            _listTypeService = listTypeService;
            _personAddresRepository = personAddresRepository; 
            _personBLLService = personBLLService;
            _emailAddressRepository = emailAddressRepository;
            _categoryRepository = categoryRepository;
            _addressRepository = addressRepository;
            _profileCaregiverRepository = profileCaregiverRepository;
            _houseHoldRepository = houseHoldRepository;
            _householdBllService = householdBllService;
            _workFlowItemRepository = workFlowItemRepository;
            _caregiverAttachmentRepository = caregiverAttachmentRepository;
            _categoryAttachmentRepository = categoryAttachmentRepository;
        }

        public ProfileCaregiverViewModel Initialize()
        {
            //var model = new ProfileCaregiverViewModel();
            var model = Mapper.Map<ProfileCaregiver, ProfileCaregiverViewModel>(new ProfileCaregiver()
            {
                TimeStamp=new TimeStamp(),
                Profile = Mapper.Map<PersonCaregiver, PersonCaregiver>(new PersonCaregiver()),
                
            });
            model.Household = Mapper.Map<Household, HouseholdViewModel>(new Household {Id = Guid.NewGuid()});

            model.Profile.Person = Mapper.Map<Person, PersonViewModel>(new Person());
            model.Profile.Person.Addresses = new Collection<PersonAddressViewModel>();

            List<PersonEmailAddress> li2 = new List<PersonEmailAddress>();
            var f = Mapper.Map<List<PersonEmailAddress>, List<PersonEmailAddressViewModel>>(li2);
            model.Profile.Person.EmailAddresses = f;

            model.Profile.Person.Phones = new Collection<PersonPhoneViewModel>();

            model.Profile.Id = Guid.NewGuid();
            model.Household = Mapper.Map<Household,HouseholdViewModel>(new Household());
            
            return model;
        }


        public ProfileCaregiverViewModel GetProfileCaregiver(Guid id)
        {
            var unMappedModel = _profileCaregiverRepository.GetByPersonIdNoTracking(id);
            var model = Mapper.Map<ProfileCaregiverViewModel>(new ProfileCaregiver());
            if (unMappedModel != null)
               model = Mapper.Map<ProfileCaregiverViewModel>(unMappedModel);

            return model;
        }
       
        public ProfileCaregiverViewModel Initialize(Guid caregiverId, string emailAddress)
        {
            var entityCaregiver = _caregiverBllService.GetCareGiver(caregiverId);

            var model = new ProfileCaregiverViewModel();

            //load the caregiver if we have one
            if (entityCaregiver.Person.FirstName != null)
            {
                model = GetProfileCaregiver(caregiverId);
                model.Profile = Mapper.Map<CareGiverViewModel, CareGiverViewModel>(entityCaregiver);
                model.CaregiverId = entityCaregiver.Id;

                //next fill in household information if we have it
                var house = _householdBllService.GetHouseholdByMemberId(caregiverId);
                if (house != null)
                {
                    model.Household = house;    
                }
                else
                {
                    model.Household = Mapper.Map<HouseholdViewModel>(new Household(){Id=Guid.Empty});
                }

                //load attachments 
                try
                {
                    //Get by default category for this input form type
                    var files = _categoryAttachmentRepository.GetByOwnerAndCategory(caregiverId,new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B"));

                    if (files.Cast<Object>().Count() > 0)
                    {
                        var filesMapped = Mapper.Map<CareGiverAttachmentViewModel>(files.FirstOrDefault());
                        Collection<AttachmentViewModel> testing = new Collection<AttachmentViewModel>();
                        foreach (var t in files)
                        {
                            var catMapp = Mapper.Map<AttachmentViewModel>(t.Attachment);
                            testing.Add(catMapp);
                        }
                        
                        model.Profile.Attachments = filesMapped;
                        model.Profile.Attachments.Files = testing;
                        model.Profile.Attachments.OwnerId = model.Profile.Person.Id;
                        model.Profile.Attachments.CareGiverId = model.Profile.Id;
                    }
                    else
                    {
                        var careAttach = 
                            Mapper.Map<ProfileCaregiverAttachment, CareGiverAttachmentViewModel>(new ProfileCaregiverAttachment
                            {
                                Attachment = new Attachment {OwnerId = caregiverId}
                            });
                        careAttach.CareGiverId = model.Profile.Id;
                        careAttach.CategoryId = new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B");

                        model.Profile.Attachments = careAttach;
                        model.Profile.Attachments.Files = Mapper.Map<IEnumerable<AttachmentViewModel>>(new Collection<Attachment>());}
                        model.Profile.Attachments.OwnerId = model.Profile.Person.Id;
                        model.Profile.Attachments.CareGiverId = model.Profile.Id;
                }
                catch (Exception e)
                {
                    e = e;
                }
            }
            else
            {
                model = Initialize();
                //New profile setup
                var add1 = Mapper.Map<Address, AddressViewModel>(new Address());
                var add2 = Mapper.Map<Address, AddressViewModel>(new Address());

                var primaryEmail = Mapper.Map<EmailAddress, EmailAddressViewModel>(new EmailAddress(){Id = Guid.NewGuid(),Email = emailAddress});
                var primaryPhone = Mapper.Map<Phone, PhoneViewModel>(new Phone{Id=Guid.NewGuid(),PhoneType="Primary"});
                
                //load default attachment object
                try
                {

                    var careAttach = Mapper.Map<ProfileCaregiverAttachment, CareGiverAttachmentViewModel>(new ProfileCaregiverAttachment
                            {
                                Attachment = new Attachment { OwnerId = caregiverId }
                            });
                    careAttach.OwnerId = caregiverId;           
                    careAttach.CategoryId = new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B");
                    model.Profile.Attachments = careAttach;
                }
                catch(Exception e)
                {
                    //don't handle this for now
                    e = e;
                }


                model.Profile.PersonId = caregiverId;
                model.Profile.Person.Id = caregiverId;
                model.CaregiverId = caregiverId;

                model.Profile.Person.PrimaryEmail = primaryEmail;
                model.Profile.Person.PrimaryPhone = primaryPhone;
                model.Profile.Person.Addresses = new Collection<PersonAddressViewModel>();
                model.Profile.Person.EmailAddresses = new Collection<PersonEmailAddressViewModel>();

                model.Profile.Person.PhysicalAddress = new PersonAddressViewModel
                {
                    Id = Guid.NewGuid(),
                    PersonId = caregiverId,
                    AddressId = add1.Id,
                    Address = add1,
                    AddressType = new Guid("C0A9FD9A-7D1C-4C44-8B06-25D4E4503575") //Mailing addess enum
                };

                model.Profile.Person.MailingAddress = new PersonAddressViewModel
                {
                    Id = Guid.NewGuid(),
                    PersonId = caregiverId,
                    AddressId =  add2.Id,
                    Address = add2,
                    AddressType = new Guid("249EF9CC-8EC1-432D-BDDE-763F2F4DAFA2") //Physical addess enum
                };
            }

            return model;
        }


        public bool Save(ProfileCaregiverViewModel model)
        {
            try
            {
                

                model.Profile.Person.Addresses = new Collection<PersonAddressViewModel>();
                model.Profile.Person.MailingAddress.PersonId = model.Profile.Person.Id;
                model.Profile.Person.PhysicalAddress.PersonId = model.Profile.Person.Id;

                var address1 = Mapper.Map<PersonAddressViewModel, PersonAddress>(model.Profile.Person.MailingAddress);
                var address2 = Mapper.Map<PersonAddressViewModel, PersonAddress>(model.Profile.Person.PhysicalAddress);

                model.Profile.Person.MailingAddress = Mapper.Map<PersonAddress, PersonAddressViewModel>(address1);
                model.Profile.Person.PhysicalAddress = Mapper.Map<PersonAddress, PersonAddressViewModel>(address2);

                model.Profile.Person.MailingAddress.AddressId = address1.Id;
                model.Profile.Person.PhysicalAddress.Id = address2.Id;

                model.Profile.Person.Addresses.Add(model.Profile.Person.MailingAddress);
                model.Profile.Person.Addresses.Add(model.Profile.Person.PhysicalAddress);

                /*********** Phone massage from the UI layer **********************************************/
                /*********** The underlying save will ignore the any fields of prefixed with primary ******/
                var personPhoneCollection =
                    Mapper.Map<ICollection<PersonPhoneViewModel>, ICollection<PersonPhone>>(model.Profile.Person.Phones);
                var newPersonPhone =
                    Mapper.Map<PersonPhoneViewModel, PersonPhone>(new PersonPhoneViewModel
                    {
                        Id = Guid.NewGuid(),
                        PersonId = model.Profile.Person.Id,
                        PhoneId = model.Profile.Person.PrimaryPhone.Id
                    });

                newPersonPhone.Phone = Mapper.Map<Phone>(model.Profile.Person.PrimaryPhone);
                personPhoneCollection.Add(newPersonPhone);
                model.Profile.Person.Phones = Mapper.Map<ICollection<PersonPhoneViewModel>>(personPhoneCollection);
                /*********** End Phone massage from the UI layer *********************************************/

                /*********** Email massage from the UI layer **********************************************/
                /*********** The underlying save will ignore the any fields of prefixed with primary ******/
                var personEmailCollection = Mapper.Map<ICollection<PersonEmailAddress>>(model.Profile.Person.EmailAddresses);
                var newPersonemail = Mapper.Map<PersonEmailAddress>(new PersonEmailAddressViewModel
                {
                        Id = Guid.NewGuid(),
                        PersonId = model.Profile.Person.Id,
                        EmailAddressId = model.Profile.Person.PrimaryEmail.Id,
                        EmailAddress = model.Profile.Person.PrimaryEmail
                    });

                personEmailCollection.Add(newPersonemail);
                model.Profile.Person.EmailAddresses = Mapper.Map<ICollection<PersonEmailAddressViewModel>>(personEmailCollection);
                /*********** End Email massage from the UI layer *********************************************/


                /*********** Update the attachment data and connect it to the profile *********************************************/
                var attachments = _categoryAttachmentRepository.GetByOwnerAndCategory(model.Profile.Person.Id, new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B"));
                if (attachments != null)
                {
                    attachments.ToList().ForEach(
                        a =>
                        {
                            _caregiverAttachmentRepository.AddOrUpdate(new ProfileCaregiverAttachment { ProfileCaregiver = new ProfileCaregiver { Id = model.Id }, AttachmentId = a.Attachment.Id });
                        });
                    _caregiverAttachmentRepository.Save();
                }

                _caregiverBllService.Save(model.Profile);

                var mappedProfileCaregiver = Mapper.Map< ProfileCaregiver>(model);

                /********************* If household is set update *****************************************************/
                var household = Mapper.Map<HouseholdViewModel>(_householdBllService.GetHouseholdByMemberId(model.Profile.Person.Id));
                var opt = _listTypeService.Relationship();
                var obj = opt.ToList().Find(t => t.Id == model.Profile.RelationToVeteran).OptionalValue.Contains("Relative") ? new Guid("8BC93007-A6DA-46BB-AA41-B5C086277916") : new Guid("9EF48E9B-76DC-4113-AC6F-1680651415FE");
                    
                if (household == null)
                {
                    /********************* First double check that the person is not part of a household *************/
                    //******************** Build the household *******************************************************/
                    household = Mapper.Map<HouseholdViewModel>(new Household() { Name = model.Profile.Person.Lastname +" family" });
                    
                    var householdMemberMap = Mapper.Map<HouseholdMemberViewModel>(new HouseholdMember
                    {
                        Person = Mapper.Map<PersonViewModel, Person>(model.Profile.Person),
                        HouseholdId = household.Id,
                        HouseholdMemberType =obj,
                        PersonId = model.Profile.Person.Id
                    });
                    household.HouseholdMembers = new Collection<HouseholdMemberViewModel>();
                    household.HouseholdMembers.Add(householdMemberMap);
                    
                    _householdBllService.AddUpdateHousehold(household);
                }
                else
                {
                    //need to add update to members record if the relationship to the veteran changes
                    household.Name = model.Profile.Person.Lastname + " family";
                    _householdBllService.AddUpdateHousehold(household);
                }

                AddUpdateProfileCaregiver(mappedProfileCaregiver);
                
                //Next attach the upload documents to the caregiver record
                _workFlowItemRepository.saveWorkflowItem_ProfileCaregiver(mappedProfileCaregiver.Id);
                //Now that we have the caregiver object setup we can begin setting up the household obejct(s)

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        public void AddUpdateProfileCaregiver(ProfileCaregiver model)
        {
            //var mapped = Mapper.Map<ProfileCaregiverViewModel, ProfileCaregiver>(model);
            var mappedEntity = _profileCaregiverRepository.GetById(model.Id);
            if (mappedEntity != null)
            {
                _profileCaregiverRepository.UpdateProfileCaregiver(mappedEntity);
            }
            else
            {
                _profileCaregiverRepository.AddOrUpdate(model);
                _profileCaregiverRepository.Save();
            }

        }
    }
}
