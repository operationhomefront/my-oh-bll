﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OH.BLL.Models.ProgramProfiles;
using OH.DAL.Repositories.ProgramProfiles;

namespace OH.BLL.Services.ProgramProfiles
{
    public interface IInjuryInflictionBLLService
    {
        Guid AddInjuryInfliction(InjuryInflictionViewModel injuryInflicationViewModel);
    }
}
