﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using OH.BLL.Models.ProgramProfiles;
using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.ProgramProfile.Injury;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Repositories.ProgramProfiles;
using OH.DAL.Repositories.ProgramProfiles.Injury;
using OH.DAL.Repositories.ProgramProfiles.Military;

namespace OH.BLL.Services.ProgramProfiles
{
    public class InjuryInflictionBLLService : IInjuryInflictionBLLService
    {
       


        private readonly IInjuryInflictionRepository _inflictionRepository;
        private readonly IInjuryInflictionCategoryRepository _inflictionCatRepository;
        private readonly IListMilitaryCombatTheaterRepository _listMilitaryCombatTheaterRepository;

        public InjuryInflictionBLLService(IInjuryInflictionRepository inflictionRepository, IInjuryInflictionCategoryRepository inflictionCatRepository, IListMilitaryCombatTheaterRepository listMilitaryCombatTheaterRepository)
        {
            _inflictionRepository = inflictionRepository;
            _inflictionCatRepository = inflictionCatRepository;
            _listMilitaryCombatTheaterRepository = listMilitaryCombatTheaterRepository;
        }
        public Guid AddInjuryInfliction(InjuryInflictionViewModel model)
        {
            var id = Guid.Empty;
            try
            {
                //later do if exists check on these values
                //var category = _inflictionCatRepository.GetById(model.InjuryInflictionCategory.Id);
                //var combatTheater = _listMilitaryCombatTheaterRepository.GetById(model.Location.Id);

                Guid inflictionId = Guid.NewGuid();
                var injuryInfliction = new InjuryInfliction
                {
                    Id= inflictionId,
                    
                    BriefDescription = model.BriefDescription,
                    Cause = model.Cause,
                    DateOfOccurrence = model.DateOfOccurrence,
                    DetailDescription = model.DetailDescription,
                    Disposition = model.Disposition,
                    
                    TypeOfInjury = model.TypeOfInjury,
                    
                    InjuryInflictionCategoryId = (Guid)model.InjuryInflictionCategoryId,
                    LocationId = (Guid)model.LocationId,
                    
                    TimeStamp = new TimeStamp() { CreatedBy = "Created", RefId = inflictionId }
                    
                };

                id = _inflictionRepository.AddOrUpdate(injuryInfliction);
            }
            catch (Exception)
            {

                //nothing yet
            }
            

            return id;
        }
    }
}
