﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Models.ProgramProfiles.WorkFlow;
using OH.BLL.Services.ListTypes;
using OH.DAL.Domain.ListTypes.Person;

namespace OH.BLL.Services.ProgramProfiles
{
    public class ProgramProfileWorkflowBLLService : IProgramProfileWorkflowBLLService
    {
        private readonly IListTypesBLLService _listTypeService;

        public ProgramProfileWorkflowBLLService(IListTypesBLLService listTypeService)
        {
            _listTypeService = listTypeService;
        }
        public ProgramProfileWorkFlowViewModel GetCaregiverInitialInquiryWorkflow()
        {
            var model = new ProgramProfileWorkFlowViewModel(){Selectors=new ProfileCaregiverSelectors()};
            
            var list = _listTypeService.GetListByType<Relationship>();
            model.Selectors.ListGender = _listTypeService.GenderList();
            model.Selectors.ListDurationProvidingCare = _listTypeService.DurationProvidingCareList();
            model.Selectors.ListHoursProvidingCare = _listTypeService.HoursProvidingCareWeeklyList();
            model.Selectors.ListRelationship = _listTypeService.Relationship();
            model.Selectors.ListAgeRange = _listTypeService.AgeRangeList();
            return model;
        }
    }
}
