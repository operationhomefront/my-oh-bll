﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Persons;
using OH.BLL.Services.Persons;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Repositories.Addresses;
using OH.DAL.Repositories.EmailAddresses;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.Phones;

namespace OH.BLL.Services.ProgramProfiles.Military
{
    public class PersonMilitaryBLLService : IPersonMilitaryBLLService
    {
        private readonly IPersonMilitaryRepository _personMilitaryRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IPersonAddressRepository _personAddresRepository;
        private readonly IPersonEmailAddressRepository _personEmailAddresRepository;
        private readonly IPersonPhoneRepository _personPhoneRepository;
        private readonly IAddressRepository _addresRepository;
        private readonly IPhoneRepository _phoneRepository;
        private readonly IEmailAddressRepository _emailAddressRepository;
        private readonly IPersonBllService _personBllService;

        public PersonMilitaryBLLService(IPersonMilitaryRepository personMilitaryRepository, 
            IPersonPhoneRepository personPhoneRepository,
            IPersonRepository personRepository, 
            IPhoneRepository phoneRepository,
            IPersonAddressRepository personAddresRepository,
            IAddressRepository addresRepository,
             IPersonEmailAddressRepository personEmailAddresRepository,
            IEmailAddressRepository emailAddressRepository,
            IPersonBllService personBllService
            )
        {
            _personMilitaryRepository = personMilitaryRepository;
            _personAddresRepository = personAddresRepository;
            _personRepository = personRepository;
            _addresRepository = addresRepository;
            _personPhoneRepository = personPhoneRepository;
            _phoneRepository = phoneRepository;
            _emailAddressRepository = emailAddressRepository;
            _personEmailAddresRepository = personEmailAddresRepository;
            _personBllService = personBllService;

        }

        public PersonMilitaryViewModel GetByPersonId(Guid personId)
        {
            
            var person = _personMilitaryRepository.GetByPersonId(personId);
            if (person != null)
            {
                var mapped = Mapper.Map<PersonMilitaryViewModel>(person);
                mapped.MilitaryPerson = _personBllService.GetFullModel(personId);
                return mapped;
            }
            else
            {
                return null;
            }
            
        }
        

        public PersonMilitaryViewModel GetById(Guid Id)
        {
            return Mapper.Map<PersonMilitaryViewModel>(_personMilitaryRepository.GetById(Id));
        }

        public void Save(Models.Persons.PersonMilitaryViewModel model)
        {
            Mapper.CreateMap<PersonMilitaryViewModel, PersonMilitary>()
                .ForMember(o => o.MilitaryPerson, d => d.Ignore())
                .ForMember(o => o.ClusterId, d => d.Ignore());

            var personMilitary = Mapper.Map<PersonMilitaryViewModel, PersonMilitary>(model);
            var personMilitaryEntity = _personMilitaryRepository.GetByPersonId(model.MilitaryPerson.Id);

            if (personMilitaryEntity != null)
            {
                var m = _personRepository.Update(Mapper.Map<Person>(model.MilitaryPerson));
                personMilitaryEntity = Mapper.Map(personMilitary, personMilitaryEntity);
                personMilitaryEntity.PersonId = model.MilitaryPerson.Id;
                _personMilitaryRepository.UpdatePersonMilitary(personMilitaryEntity);
            }
            else
            {
                //double check for the person record
                var personUnknown = _personBllService.AddPerson(model.MilitaryPerson);

                //personCaregiver.Person = Mapper.Map<PersonViewModel,Person>(profileCaregiver.Person);
                personMilitary.MilitaryPerson = null;

                personMilitary.PersonId = personUnknown;

                _personMilitaryRepository.AddOrUpdate(personMilitary);
                _personMilitaryRepository.Save();
            }


            /*****************************/
            /***** Person Emails *********/

            var mappedEmailCollection = Mapper.Map<ICollection<PersonEmailAddressViewModel>, ICollection<PersonEmailAddress>>(model.MilitaryPerson.EmailAddresses);

            foreach (PersonEmailAddress p in mappedEmailCollection)
            {
                //fist check for the email add in the entity
                var unMappedEmailEntity = _emailAddressRepository.GetNoTracking(p.EmailAddress.Id);
                if (unMappedEmailEntity != null)
                {
                    _emailAddressRepository.UpdateEmailAddres(p.EmailAddress);
                }
                else
                {
                    _personEmailAddresRepository.AddOrUpdate(p);
                    _personEmailAddresRepository.Save();
                }
            }


            /*****************************/
            /***** Person Phones *********/

            var mappedPhone = Mapper.Map<ICollection<PersonPhoneViewModel>, ICollection<PersonPhone>>(model.MilitaryPerson.Phones);

            foreach (PersonPhone p in mappedPhone)
            {
                var unMappedEntity = _phoneRepository.GetNoTracking(p.Phone.Id);
                if (unMappedEntity != null)
                {
                    _phoneRepository.UpdatePhone(p.Phone);
                }
                else
                {
                    _personPhoneRepository.AddOrUpdate(p);
                    _personPhoneRepository.Save();
                }
            }

            /*****************************/
            /***** Mailing Address ******/
            var mapped = Mapper.Map<PersonAddressViewModel, PersonAddress>(model.MilitaryPerson.MailingAddress);

            var mappedEntity = _personAddresRepository.GetNoTracking(mapped.Id);
            if (mappedEntity != null)
            {
                var internalmapped = Mapper.Map<AddressViewModel, Address>(model.MilitaryPerson.MailingAddress.Address);
                var addressUpdate = Mapper.Map(internalmapped, mappedEntity.Address);
                _addresRepository.UpdateAddress(addressUpdate);
            }
            else
            {
                _personAddresRepository.AddOrUpdate(mapped);
                _personAddresRepository.Save();
            }

            /*****************************/
            /***** Physical Address ******/
            var mapped1 = Mapper.Map<PersonAddressViewModel, PersonAddress>(model.MilitaryPerson.PhysicalAddress);
            var mappedEntity1 = _personAddresRepository.GetNoTracking(mapped1.Id);
            if (mappedEntity != null)
            {
                var addressUpdate1 = Mapper.Map(mapped1.Address, mappedEntity1.Address);
                _addresRepository.UpdateAddress(addressUpdate1);
            }
            else
            {
                _personAddresRepository.AddOrUpdate(mapped1);
                _personAddresRepository.Save();
            }
        }


    }
}
