﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.Households;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Phones;
using OH.BLL.Models.ProgramProfiles;
using OH.BLL.Models.ProgramProfiles.Military;
using OH.BLL.Services.Households;
using OH.BLL.Services.ListTypes;
using OH.BLL.Services.Persons;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Households;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.ProgramProfile.Injury;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Repositories.Addresses;
using OH.DAL.Repositories.Categories;
using OH.DAL.Repositories.EmailAddresses;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.Phones;
using OH.DAL.Repositories.ProgramProfiles.Military;

namespace OH.BLL.Services.ProgramProfiles.Military
{
    public class ProgramProfileMilitaryBLLService : IProgramProfileMilitaryBLLService
    {
        private IProfileMilitaryRepository _serviceMemberRepository;
        private IPersonBllService _personBLLService;
        private IEmailAddressRepository _emailAddressRepository;
        private readonly IPersonAddressRepository _personAddresRepository;

        private readonly IListTypesBLLService _listTypeService;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IPersonAddressRepository _addressRepository;
        private readonly IProfileMilitaryRepository _profileMilitaryRepository;
        private readonly IHouseholdBLLService _householdBllService;
        private readonly IPersonMilitaryBLLService _personMilitaryBllService;
        

        public ProgramProfileMilitaryBLLService(IProfileMilitaryRepository serviceMemberRepository,
            IHouseholdBLLService householdBllService,
            IPersonBllService personBLLService,
            IPersonMilitaryBLLService personMilitaryBllService,
            IPersonAddressRepository personAddresRepository,
            IProfileMilitaryRepository profileMilitaryRepository,
            IEmailAddressRepository emailAddressRepository, IListTypesBLLService listTypeService,
            ICategoryRepository categoryRepository, IPersonAddressRepository addressRepository)
        {
            _serviceMemberRepository = serviceMemberRepository;
            _listTypeService = listTypeService;
            _personAddresRepository = personAddresRepository;
            _personBLLService = personBLLService;
            _emailAddressRepository = emailAddressRepository;
            _categoryRepository = categoryRepository;
            _addressRepository = addressRepository;
            _householdBllService = householdBllService;
            _personMilitaryBllService = personMilitaryBllService;
            _profileMilitaryRepository = profileMilitaryRepository;
        }

        public ProfileMilitaryViewModel Initialize()
        {
            var model = Mapper.Map<ProfileMilitaryViewModel>(new ProfileMilitary());
            model.Selectors = new ProfileMilitarySelectors();
            model.Selectors.Gender = _listTypeService.GenderList();
            model.Selectors.Rank = _listTypeService.MilitaryRank();
            model.Selectors.ServiceStatus = _listTypeService.ServiceStatus();
            model.Selectors.BranchOfService = _listTypeService.BranchOfService();
            model.Selectors.ListRelationship = _listTypeService.Relationship();
            model.Selectors.DeploymentStatus = _listTypeService.DeploymentStatus();
            model.Selectors.InjuryInflictionCategory = _listTypeService.InjuryInflictionCategory();


            model.MilitaryProfile = Mapper.Map<PersonMilitaryViewModel>(new PersonMilitary());
            model.MilitaryProfile.MilitaryPerson = Mapper.Map<PersonViewModel>(new Person());
            model.MilitaryProfile.MilitaryPerson.Addresses = new Collection<PersonAddressViewModel>();
            model.MilitaryProfile.MilitaryPerson.EmailAddresses = new Collection<PersonEmailAddressViewModel>();
            model.MilitaryProfile.MilitaryPerson.Phones = new Collection<PersonPhoneViewModel>();
            model.MilitaryProfile.MilitaryPerson = Mapper.Map<PersonViewModel>(new Person());
            model.MilitaryProfile.MilitaryPerson.PrimaryEmail = Mapper.Map<EmailAddressViewModel>(new EmailAddress() { Id = Guid.NewGuid() });
            model.MilitaryProfile.MilitaryPerson.PrimaryPhone = Mapper.Map<Phone, PhoneViewModel>(new Phone { Id = Guid.NewGuid(), PhoneType = "Primary" });
            model.MilitaryProfile.MilitaryPerson.PhysicalAddress = _personBLLService.InitializePersonAddressByType(new Guid("C0A9FD9A-7D1C-4C44-8B06-25D4E4503575"));
            model.MilitaryProfile.MilitaryPerson.MailingAddress = _personBLLService.InitializePersonAddressByType(new Guid("249EF9CC-8EC1-432D-BDDE-763F2F4DAFA2"));

           

            model.Household = Mapper.Map<HouseholdViewModel>(new Household());
            
            return model;
        }

        public ProfileMilitaryViewModel Initialize(Guid householdMemberId)
        {
            var model = new ProfileMilitaryViewModel();
            model = Initialize();
            //First attempt to load the household object because we will need it later
            var household = _householdBllService.GetHouseholdByMemberId(householdMemberId);
            //todo: make sure the rest of the ID's are getting set

            if (household != null)
            {
                model.Household = household;
                var servicemembers = _householdBllService.GetHouseholdMilitaryMembers(household.Id);
                if (servicemembers.Any())
                {
                    //if count equals one then we know which one to pick 
                    if (servicemembers.Count == 1)
                    {
                        var mem = Initialize(servicemembers[0], "");
                        model.MilitaryProfile= mem.MilitaryProfile;
                        
                        model.MilitaryProfile.MilitaryPerson.PhysicalAddress.PersonId = model.MilitaryProfile.MilitaryPerson.Id;
                        model.MilitaryProfile.MilitaryPerson.PhysicalAddress.PersonId = model.MilitaryProfile.MilitaryPerson.Id;

                        try
                        {

                            var serviceMemberAttach = Mapper.Map<ProfileMilitaryAttachment, PersonMilitaryAttachmentViewModel>(new ProfileMilitaryAttachment
                            {
                                Attachment = new Attachment { OwnerId = model.MilitaryProfile.MilitaryPerson.Id }
                            });
                            serviceMemberAttach.OwnerId = model.MilitaryProfile.MilitaryPerson.Id;
                            serviceMemberAttach.CategoryId = new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B");
                            model.MilitaryProfile.PersonMilitaryAttachments = serviceMemberAttach;
                        }
                        catch (Exception e)
                        {
                            //don't handle this for now
                            e = e;
                        }


                    }
                    else
                    {
                       //todo: will need to compensate for this
                    }
                }
            }
            else
            {
                //load default attachment object
                //try
                //{

                //    var careAttach = Mapper.Map<ProfileCaregiverAttachment, CareGiverAttachmentViewModel>(new ProfileCaregiverAttachment
                //    {
                //        Attachment = new Attachment { OwnerId = caregiverId }
                //    });
                //    careAttach.OwnerId = caregiverId;
                //    careAttach.CategoryId = new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B");
                //    model.Profile.Attachments = careAttach;
                //}
                //catch (Exception e)
                //{
                //    //don't handle this for now
                //    e = e;
                //}

                var newMil = Guid.NewGuid();
                model.MilitaryProfile.MilitaryPerson.Id = newMil;

                model.MilitaryProfile.MilitaryPerson.PhysicalAddress =
                   _personBLLService.InitializeByPersonIdAndType(newMil,
                       new Guid("C0A9FD9A-7D1C-4C44-8B06-25D4E4503575"));
                model.MilitaryProfile.MilitaryPerson.MailingAddress =
                    _personBLLService.InitializeByPersonIdAndType(newMil,
                        new Guid("249EF9CC-8EC1-432D-BDDE-763F2F4DAFA2"));
               
            }

            return model;
        }
        public ProfileMilitaryViewModel Initialize(Guid serviceMemberId, string EmailAddress)
        {
            var entityPersonMilitary = _profileMilitaryRepository.GetByPersonId(serviceMemberId);
            var model = new ProfileMilitaryViewModel();
            //model = Mapper.Map<ProfileMilitaryViewModel>(entityPersonMilitary);
            model.MilitaryProfile = new PersonMilitaryViewModel();
            var serviceMember = _personMilitaryBllService.GetByPersonId(serviceMemberId);

            model.MilitaryProfile = serviceMember;

            return model;
        }


        public bool Save(ProfileMilitaryViewModel model)
        {
            try
            {
                model.MilitaryProfile.MilitaryPerson.Addresses = new Collection<PersonAddressViewModel>();
                model.MilitaryProfile.MilitaryPerson.MailingAddress.PersonId = model.MilitaryProfile.MilitaryPerson.Id;
                model.MilitaryProfile.MilitaryPerson.PhysicalAddress.PersonId = model.MilitaryProfile.MilitaryPerson.Id;
                
                model.MilitaryProfile.MilitaryPerson.MailingAddress.AddressId = model.MilitaryProfile.MilitaryPerson.MailingAddress.Address.Id;
                model.MilitaryProfile.MilitaryPerson.PhysicalAddress.AddressId = model.MilitaryProfile.MilitaryPerson.PhysicalAddress.Address.Id;

                model.MilitaryProfile.MilitaryPerson.MailingAddress.Id = model.MilitaryProfile.MilitaryPerson.MailingAddress.Id;
                model.MilitaryProfile.MilitaryPerson.PhysicalAddress.Id = model.MilitaryProfile.MilitaryPerson.PhysicalAddress.Id;


                model.MilitaryProfile.MilitaryPerson.Addresses.Add(model.MilitaryProfile.MilitaryPerson.MailingAddress);
                model.MilitaryProfile.MilitaryPerson.Addresses.Add(model.MilitaryProfile.MilitaryPerson.PhysicalAddress);

                /*********** Phone massage from the UI layer **********************************************/
                /*********** The underlying save will ignore the any fields of prefixed with primary ******/
                var personPhoneCollection =
                    Mapper.Map<ICollection<PersonPhoneViewModel>, ICollection<PersonPhone>>(model.MilitaryProfile.MilitaryPerson.Phones);
                var newPersonPhone =
                    Mapper.Map<PersonPhoneViewModel, PersonPhone>(new PersonPhoneViewModel
                    {
                        Id = Guid.NewGuid(),
                        PersonId = model.MilitaryProfile.MilitaryPerson.Id,
                        PhoneId = model.MilitaryProfile.MilitaryPerson.PrimaryPhone.Id
                    });

                newPersonPhone.Phone = Mapper.Map<Phone>(model.MilitaryProfile.MilitaryPerson.PrimaryPhone);
                personPhoneCollection.Add(newPersonPhone);
                model.MilitaryProfile.MilitaryPerson.Phones = Mapper.Map<ICollection<PersonPhoneViewModel>>(personPhoneCollection);
                /*********** End Phone massage from the UI layer *********************************************/

                /*********** Email massage from the UI layer **********************************************/
                /*********** The underlying save will ignore the any fields of prefixed with primary ******/
                var personEmailCollection = Mapper.Map<ICollection<PersonEmailAddress>>(model.MilitaryProfile.MilitaryPerson.EmailAddresses);
                var newPersonemail = Mapper.Map<PersonEmailAddress>(new PersonEmailAddressViewModel
                {
                    Id = Guid.NewGuid(),
                    PersonId = model.MilitaryProfile.MilitaryPerson.Id,
                    EmailAddressId = model.MilitaryProfile.MilitaryPerson.PrimaryEmail.Id,
                    EmailAddress = model.MilitaryProfile.MilitaryPerson.PrimaryEmail
                });

                personEmailCollection.Add(newPersonemail);
                model.MilitaryProfile.MilitaryPerson.EmailAddresses = Mapper.Map<ICollection<PersonEmailAddressViewModel>>(personEmailCollection);

                /*********** End Email massage from the UI layer *********************************************/
                if (model.MilitaryProfile.Id == Guid.Empty)
                    model.MilitaryProfile.Id = Guid.NewGuid();


                /************ Massage data input from the UI for injury inflictions **************************/
                model.MilitaryProfile.InjuryInflictions = new Collection<InjuryInflictionViewModel>();
                //if (model.MilitaryProfile.injuryAmputee)
                //{
                //    model.MilitaryProfile.InjuryInflictions.Add(
                //        Mapper.Map<InjuryInflictionViewModel>(new InjuryInfliction()));
                //}
                //if ( model.MilitaryProfile.injuryBlindness)


                /************ End injury inflictions **************************/
                

                var mappedProfileMilitary = Mapper.Map<ProfileMilitaryViewModel>(model);
                _personMilitaryBllService.Save(model.MilitaryProfile);

                /********************* If household is set update *****************************************************/
                if (model.Household.Id.Equals(Guid.Empty))
                {
                    /********************* First double check that the person is not part of a household *************/
                    //******************** Build the household *******************************************************/
                    var household = Mapper.Map<HouseholdViewModel>(new Household() { Name = model.MilitaryProfile.MilitaryPerson.Lastname });
                    var householdMemberMap = Mapper.Map<HouseholdMemberViewModel>(new HouseholdMember
                    {
                        Person = Mapper.Map<PersonViewModel, Person>(model.MilitaryProfile.MilitaryPerson),
                        HouseholdId = household.Id,
                        PersonId = model.MilitaryProfile.MilitaryPerson.Id,
                        HouseholdMemberType = Guid.NewGuid()
                        
                    });

                    household.HouseholdMembers = new Collection<HouseholdMemberViewModel>();
                    household.HouseholdMembers.Add(householdMemberMap);

                    _householdBllService.AddUpdateHousehold(household);
                }
                else
                {
                    var householdTest = Mapper.Map<HouseholdViewModel>(_householdBllService.GetHousehold(model.Household.Id));
                    var household = Mapper.Map<HouseholdViewModel>(_householdBllService.GetHouseholdByMemberId(model.MilitaryProfile.MilitaryPerson.Id));
                    var householdMemberTest = _householdBllService.HouseholdMemberExits(model.MilitaryProfile.MilitaryPerson.Id, model.Household.Id);

                    if (!householdMemberTest && householdTest != null)
                    {
                        var householdMemberMap = Mapper.Map<HouseholdMemberViewModel>(new HouseholdMember
                        {
                            //Person = Mapper.Map<PersonViewModel, Person>(model.MilitaryProfile.MilitaryPerson),
                            HouseholdId = householdTest.Id,
                            PersonId = model.MilitaryProfile.MilitaryPerson.Id,
                            HouseholdMemberType = Guid.NewGuid()
                        });

                        _householdBllService.AddHouseholdMember(householdMemberMap);
                        //householdTest.HouseholdMembers = new Collection<HouseholdMemberViewModel>();
                        //householdTest.HouseholdMembers.Add(householdMemberMap);
                        //_householdBllService.AddUpdateHousehold(householdTest);
                    }
                    //household.Name = model.MilitaryProfile.PersonMilitary.Lastname + " family";
                }

                AddUpdateProfileMilitary(mappedProfileMilitary);

                //_workFlowItemRepository.saveWorkflowItem_ProfileCaregiver(mappedProfileCaregiver.Id);
                //Now that we have the caregiver object setup we can begin setting up the household obejct(s)
                return true;
            }
            catch (Exception)
            {
                return false;
            }
       
        }

        public void AddUpdateProfileMilitary(ProfileMilitaryViewModel mappedProfileMilitary)
        {
            var mappedEntity = _profileMilitaryRepository.GetById(mappedProfileMilitary.Id);
            if (mappedEntity != null)
            {
                _profileMilitaryRepository.UpdateProfileMilitary(mappedEntity);
            }
            else
            {
                mappedProfileMilitary.MilitaryProfileId = mappedProfileMilitary.MilitaryProfile.Id;

                var model = Mapper.Map<ProfileMilitary>(mappedProfileMilitary);
                model.MilitaryProfile = null;
                model.WorkFlowItems = null;
                model.Attachments = null;
                _profileMilitaryRepository.AddOrUpdate(model);
                _profileMilitaryRepository.Save();
            }
        }
    }
}
