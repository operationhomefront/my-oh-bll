﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.DAL.Domain.ProgramProfile.Military;

namespace OH.BLL.Services.ProgramProfiles.Military
{
    public interface IPersonMilitaryBLLService
    {
        void Save(PersonMilitaryViewModel model);
        PersonMilitaryViewModel GetById(Guid Id);
        PersonMilitaryViewModel GetByPersonId(Guid personId);
    }
}
