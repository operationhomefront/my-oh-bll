﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.Military;

namespace OH.BLL.Services.ProgramProfiles.Military
{
    public interface IProgramProfileMilitaryBLLService
    {
        ProfileMilitaryViewModel Initialize();
        ProfileMilitaryViewModel Initialize(Guid serviceMemberId, string EmailAddress);
        ProfileMilitaryViewModel Initialize(Guid householdMemberId);
        bool Save(ProfileMilitaryViewModel model);
        void AddUpdateProfileMilitary(ProfileMilitaryViewModel model);

    }
}
