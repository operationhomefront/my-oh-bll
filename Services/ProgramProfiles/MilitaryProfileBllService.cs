﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles;
using OH.BLL.Models.ProgramProfiles.Military;
using OH.BLL.Services.Attachments;
using OH.BLL.Services.ListTypes.Military;
using OH.BLL.Services.Persons;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Repositories.Attachments;
using OH.DAL.Repositories.Categories;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.ProgramProfiles;
using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Repositories.ProgramProfiles.CareGiver;
using OH.DAL.Repositories.ProgramProfiles.Military;

namespace OH.BLL.Services.ProgramProfiles
{
    public class MilitaryProfileBllService : IListMilitaryProfileBLLService 
    {
        private readonly IProfileMilitaryRepository _profileMilitaryRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryAttachmentRepository _categoryAttachmentRepository;
        private readonly IProfileCaregiverAttachmentRepository _profileCareGiverAttachmentRepository;
        
        public MilitaryProfileBllService(IProfileMilitaryRepository profileMilitaryRepository,IPersonRepository personRepository, IProfileCaregiverAttachmentRepository profileCareGiverAttachmentRepository)
        {
            _profileMilitaryRepository = profileMilitaryRepository;
            _personRepository = personRepository;
            _profileCareGiverAttachmentRepository = profileCareGiverAttachmentRepository;
        }

        public MilitaryProfileBllService(IProfileMilitaryRepository profileMilitaryRepository, IPersonRepository personRepository, IAttachmentRepository attachmentRepository, ICategoryRepository categoryRepository, ICategoryAttachmentRepository categoryAttachmentRepository, IProfileCaregiverAttachmentRepository profileCareGiverAttachmentRepository)
        {
            _profileMilitaryRepository = profileMilitaryRepository;
            _personRepository = personRepository;
            _attachmentRepository = attachmentRepository;
            _categoryRepository = categoryRepository;
            _categoryAttachmentRepository = categoryAttachmentRepository;
            _profileCareGiverAttachmentRepository = profileCareGiverAttachmentRepository;
        }

        public Guid AddMilitaryProfileWithAttachments(PersonMilitaryViewModel model)
        {
            //Guid profileId = AddMilitaryProfile(model);

            //AttachmentBllService attachmentsService = new AttachmentBllService(_attachmentRepository,_categoryRepository,_categoryAttachmentRepository, _profileCareGiverAttachmentRepository);

            //attachmentsService.UploadAttachment(model.Attachments);


            return Guid.NewGuid();
        }

        public Guid AddMilitaryProfile(PersonMilitaryViewModel model)
        {
           
           //PersonBllService personService = new PersonBllService(_personRepository);
            
           // Guid profileId = Guid.NewGuid();
           // Guid servicememberId = personService.AddPerson(model.ServiceMember);

           // var profile = _profileMilitaryRepository.AddOrUpdate(new ProfileMilitary
           // {
           //     Id = profileId,
           //     ServiceMemberId = servicememberId,
           //     BranchOfServiceId = model.BranchOfServiceId,
           //     MilitaryRankId = model.MilitaryRankId,
           //     DeploymentStatusId = model.DeploymentStatusId,
           //     VeteranStatusId = model.VeteranStatusId,
           //     ServiceStatusId = model.ServiceStatusId,
           //     TimeStamp = new TimeStamp() { CreatedBy = "Profile Created Test", RefId = profileId }
           // });
           // _profileMilitaryRepository.Save();
            
           // return profileId;
            return Guid.NewGuid();
        }
    }
}
