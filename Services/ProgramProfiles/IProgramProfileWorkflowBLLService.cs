﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ProgramProfiles.WorkFlow;

namespace OH.BLL.Services.ProgramProfiles
{
    public interface IProgramProfileWorkflowBLLService
    {
        ProgramProfileWorkFlowViewModel GetCaregiverInitialInquiryWorkflow();

    }
}
