﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models;

namespace OH.BLL.Services.Staff
{
    public class StaffMenuService
    {
        public IEnumerable<NavigationItemViewModel> GetInlineData()
        {
            return new List<NavigationItemViewModel>
            {
                new NavigationItemViewModel
                {
                    CategoryName = "Rally Point - Staff",
                    SubCategories = new List<NavigationSubItemViewModel>
                    {
                        new NavigationSubItemViewModel()
                        {
                            SubCategoryName = "Distribution",
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Create New Event",
                            Controller = "/Events/DistributionEvent",
                            Action = "New"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Active",
                            Controller = "/Events/Staff",
                            Action = "GetActiveDistributionEvents"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Closing Soon",
                            Controller = "/Events/Staff",
                            Action = "GetClosingActiveDistributionEvents"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Closed",
                            Controller = "/Events/Staff",
                            Action = "GetClosedDistributionEvents"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Upcoming",
                            Controller = "/Events/Staff",
                            Action = "GetUpcomingDistributionEvents"
                        },
                        new NavigationSubItemViewModel()
                        {
                            SubCategoryName = "Collections"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Create New Event",
                            Controller = "/Events/CollectionEvent",
                            Action = "New"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Active",
                            Controller = "/Events/Staff",
                            Action = "GetActiveCollectionEvents"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Closed",
                            Controller = "/Events/Staff",
                            Action = "GetClosedCollectionEvents"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Upcoming",
                            Controller = "/Events/Staff",
                            Action = "GetUpcomingCollectionEvents"
                        }
                    }
                },
                new NavigationItemViewModel
                {
                    CategoryName = "Military Families",
                    SubCategories = new List<NavigationSubItemViewModel>
                    {
                        new NavigationSubItemViewModel()
                        {
                            SubCategoryName = "All Military Families",
                            Controller = "/Events/Staff",
                            Action = "MilitaryFamilies"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Service Member",
                            Controller = "/Events/Staff",
                            Action = "ServiceMemberLookup"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Lookup",
                            Controller = "/Events/Staff",
                            Action = "ServiceMemberLookup"
                        }
                    }
                },
                new NavigationItemViewModel
                {
                    CategoryName = "Registration",
                    SubCategories = new List<NavigationSubItemViewModel>
                    {
                        new NavigationSubItemViewModel()
                        {
                            SubCategoryName = "Forms"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Forms List",
                            Controller = "/Events/Forms",
                            Action = "List"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Create New Form",
                            Controller = "/Events/Forms",
                            Action = "New"
                        }
                    }
                },
                new NavigationItemViewModel
                {
                    CategoryName = "Office Profiles",
                    SubCategories = new List<NavigationSubItemViewModel>
                    {
                        new NavigationSubItemViewModel()
                        {
                            SubCategoryName = "Dashboard"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "New Office",
                            Controller = "/Office",
                            Action = "New"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Edit Office",
                            Controller = "/Office",
                            Action = "Edit"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Area Management",
                            Controller = "/Office",
                            Action = "officeAreaManagement"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Important Zipcodes",
                            Controller = "/Zipcode",
                            Action = "important"
                        },
                        new NavigationSubItemViewModel
                        {
                            SubCategoryName = "Staff Directory",
                            Controller = "/Events/Staff",
                            Action = "staffdirectory"
                        }
                    }
                }
            };
        }

    }
}
