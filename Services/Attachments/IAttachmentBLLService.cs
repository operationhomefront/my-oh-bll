﻿using System;
using System.Collections.Generic;
using System.Web;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Persons;

namespace OH.BLL.Services.Attachments
{
    public interface IAttachmentBllService
    {
        UploadAttachmentViewModel InitializeUploader();
        void UploadAttachment(UploadAttachmentViewModel model);

        void UploadCareGiverAttachment(IEnumerable<HttpPostedFileBase> files, string caregiverId, string categoryId);

        void DeleteAttachment(Guid attachmentId);
        void DeleteAttachment(Guid ownerId, string fileName, Guid categoryId);
        UploadAttachmentViewModel GetAttachment(Guid ownerId, string fileName, Guid categoryId);
    }
}
