﻿using System;
using AutoMapper;
using OH.BLL.Models.Categories;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Categories;

namespace OH.BLL.Categories
{
    public class CategoryBllService : Services.Categories.ICategoryBllService
    {

        private readonly ICategoryRepository _categoryRepository;

        public CategoryBllService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public CategoryTreeViewModel GetCategoryTreeViewModelByOwnerId(Guid ownerId)
        {
            var parentCategory = _categoryRepository.GetCategoryByOwnerId(ownerId);

            var model = new CategoryTreeViewModel(parentCategory);

            return model;
        }

        public CategoryViewModel GetNewCategoryViewModel()
        {
            return Mapper.Map<Category, CategoryViewModel>(new Category());
        }

        //TODO: This will have category Id Parameter(s) that will more than like come from a case
        public CategoryTreeViewModel GetCategoryTreeViewModelById(Guid categoryId)
        {
            var parentCategory = _categoryRepository.GetCategoryByIdInclude(categoryId);

            var model = new CategoryTreeViewModel(parentCategory);

            return model;
        }

        public Guid CreateCategory(CategoryViewModel model)
        {
            var category = Mapper.Map<CategoryViewModel, Category>(model);

            var categoryId = _categoryRepository.AddOrUpdate(category);
            _categoryRepository.Save();

            return categoryId;
        }
    }
}
