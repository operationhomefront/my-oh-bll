using System;
using OH.BLL.Models.Categories;

namespace OH.BLL.Categories
{
    public interface ICategoryBllService
    {
        CategoryTreeViewModel GetCategoryTreeViewModelById(Guid categoryId);
        CategoryTreeViewModel GetCategoryTreeViewModelByOwnerId(Guid ownerId);
        CategoryViewModel GetNewCategoryViewModel();
        Guid CreateCategory(CategoryViewModel model);
    }
}