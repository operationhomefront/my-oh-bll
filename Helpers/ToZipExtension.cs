﻿using System;

namespace OH.BLL.Helpers
{
    public static class ToZipExtension {

        public static String ToZip(this String zip) {
            var zc = zip.StripNonNumeric();

            if (zc.Length <= 5)
                return zc;

            return zc.Substring(0, 5) + "-" + zc.Substring(5);
        }

        public static String ToZip5(this String zip) {
            if (zip.Length <= 5)
                return zip;
            
            return zip.ToZip().Substring(0, 5);
        }
    }
}
