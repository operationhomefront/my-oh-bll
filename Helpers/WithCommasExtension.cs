﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.BLL.Helpers
{
    public static class WithCommasExtension
    {
        public static String WithCommas(this int val)
        {
            return val.ToString("#,##0");
        }
    }
}