﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ModelMetadata = System.Web.Http.Metadata.ModelMetadata;

namespace OH.BLL.Helpers
{
    public class PlaceHolderAttribute : Attribute, System.Web.Mvc.IMetadataAware
    {
        private readonly string _placeholder;
        public PlaceHolderAttribute(string placeholder)
        {
            _placeholder = placeholder;
        }


        public void OnMetadataCreated(System.Web.Mvc.ModelMetadata metadata)
        {
            metadata.AdditionalValues["placeholder"] = _placeholder;
        }

        
    }

    public class RequiredAsAttribute : Attribute, System.Web.Mvc.IMetadataAware
    {
        private readonly string _requiredAs;

        public RequiredAsAttribute(string requiredAs)
        {
            _requiredAs = requiredAs;
        }

        public void OnMetadataCreated(System.Web.Mvc.ModelMetadata metadata)
        {
            metadata.AdditionalValues["requiredas"] = _requiredAs;
        }
    }
}
