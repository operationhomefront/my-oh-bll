﻿using System;
using System.Text;

namespace OH.BLL.Helpers
{
    public static class StripNonNumericExtension
    {
        public static String StripNonNumeric(this String s)
        {
            var retString = new StringBuilder();

            if (!String.IsNullOrEmpty(s))
            {
                foreach (var c in s)
                {
                    if (Char.IsNumber(c))
                        retString.Append(c);
                }
            }
            return retString.ToString().Trim();
        }
    }
}