﻿using System;
using System.Collections.Generic;

namespace OH.BLL.Helpers {

    public enum Title {
        None =1,
        Mr   =2,
        Mrs  =3,
        Miss =4,
        Ms   =5,
        Rev  =6,
        Dr   =7
    }

    public static class TitleTypes {

        public static String Name(this Title t)
		{
			var name = "";

			switch (t) {
                case Title.None:
                    name = "None";
                    break;
				case Title.Rev:
					name = "Rev.";
					break;

				case Title.Ms:
					name = "Ms.";
					break;

				case Title.Mrs:
					name = "Mrs.";
					break;

				case Title.Miss:
					name = "Miss";
					break;

				case Title.Dr:
					name = "Dr.";
					break;

				case Title.Mr:
					name = "Mr.";
					break;
			}
			return name;
		}

        public static Dictionary<String, Enum> ComboList()
		{
			var comboList = new Dictionary<String, Enum>
								{
                                    {"None"	    , Title.None},
									{"Mr."	    , Title.Mr},
									{"Mrs."		, Title.Mrs},
									{"Miss"		, Title.Miss},
									{"Ms."		, Title.Ms},
									{"Rev."		, Title.Rev},
									{"Dr."		, Title.Dr}
								};
			return comboList;
		}
	}
}
