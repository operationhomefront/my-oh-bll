﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Helpers
{
    public static class SpacialHelper
    {

        public static DbGeography CreatePoint(Double? latitude, Double? longitude)
        {

            if (latitude == null)
                latitude = 0;

            if (longitude == null)
                longitude = 0;

            var text = String.Format("POINT({0} {1})", longitude, latitude);
            return DbGeography.PointFromText(text, 4326);    // 4326 is most common coordinate system used by GPS/Maps
        }

        public static Double ToMiles(this Double meters)
        {

            return meters * 0.000621371192;
        }

        public static Double ToMeters(this Double miles)
        {
            return miles * 1609.344;
        }
    }
}
