﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using AutoMapper;
using Kendo.Mvc.UI;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Applications;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Categories;
using OH.BLL.Models.ChangeLogs;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.Events;
using OH.BLL.Models.Households;
using OH.BLL.Models.ListTypes;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Phones;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Models.ProgramProfiles.Military;
using OH.BLL.Models.Schedulers;
using OH.BLL.Models.TimeStamps;
using OH.BLL.Models.WorkFlow;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.Categories;
using OH.DAL.Domain.ChangeLogs;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Households;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.ListTypes.Injury;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ListTypes.Person;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Domain.Tasks;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.BLL
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<WorkflowItem, IGanttTask>();
            Mapper.CreateMap<WorkflowItem, WorkflowItemViewModel>();
            Mapper.CreateMap<WorkflowItemDependency, WorkflowItemDependencyViewModel>();

            Mapper.CreateMap<ProfileMilitary, ProfileMilitaryViewModel>();
            Mapper.CreateMap<ProfileMilitary, ProfileMilitaryViewModel>()
                .ReverseMap().ForMember(dest => dest.WorkFlowItems, opt => opt.Ignore())
                .ReverseMap().ForMember(dest => dest.Household, opt => opt.Ignore())
                .ReverseMap().ForMember(dest => dest.Attachments, opt => opt.Ignore())
                .ReverseMap().ForMember(dest => dest.Selectors, opt => opt.Ignore());

            Mapper.CreateMap<PersonMilitary, PersonMilitaryViewModel>().ForMember(s => s.TimeStamp, d => new TimeStamp());

            Mapper.CreateMap<ProfileMilitaryAttachment,PersonMilitaryAttachmentViewModel>();

            Mapper.CreateMap<CategoryAttachment, CareGiverAttachmentViewModel>();
            Mapper.CreateMap<CategoryAttachment, ProfileCaregiverAttachment>();

            Mapper.CreateMap<CategoryAttachmentViewModel, CareGiverAttachmentViewModel>().ForMember(src => src.Files, dest => dest.MapFrom(m => m.Attachment));
            Mapper.CreateMap<CategoryAttachmentViewModel, AttachmentViewModel>();


            Mapper.CreateMap<ProfileCaregiverAttachment, CareGiverAttachmentViewModel>();
            Mapper.CreateMap<ProfileCaregiverAttachment, CareGiverAttachmentViewModel>().ReverseMap().ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            Mapper.CreateMap<ProfileCaregiver, ProfileCaregiverViewModel>().ForMember(s => s.TimeStamp, d => new TimeStamp());
            Mapper.CreateMap<ProfileCaregiver, ProfileCaregiverViewModel>().ReverseMap()
                    .ForMember(s => s.ClusterId, d => d.Ignore())
                    .ForMember(s => s.WorkFlowItems, d => d.Ignore())
                    .ForMember(s => s.Profile, d => d.Ignore());

            Mapper.CreateMap<Person, CareGiverViewModel>().ForMember(s => s.TimeStamp, d => new TimeStamp());

            Mapper.CreateMap<Person, PersonCaregiver>()
               .ForMember(s => s.ClusterId, o => o.Ignore())
               .ForMember(s => s.TimeStamp, d => new TimeStamp());
            Mapper.CreateMap<Person, PersonCaregiver>().ReverseMap()
               .ForMember(s => s.ClusterId, o => o.Ignore())
               .ForMember(s => s.EmailAddresses, d => d.Ignore());


            Mapper.CreateMap<PersonCaregiver, PersonCaregiver>();
            Mapper.CreateMap<PersonCaregiver, PersonCaregiver>().ReverseMap()
                //.ForMember(s => s.Phones, o => o.Ignore())
                //.ForMember(s => s.Person.PrimaryEmail, o => o.Ignore())
                //.ForMember(s => s.Person.PrimaryPhone, o => o.Ignore())
                .ForMember(s => s.ClusterId, o => o.Ignore());

            Mapper.CreateMap<PersonCaregiver, CareGiverViewModel>().ForMember(s => s.TimeStamp, d => new TimeStamp());
            Mapper.CreateMap<PersonCaregiver, CareGiverViewModel>().ReverseMap()
                //.ForMember(s => s.Phones, o => o.Ignore())
                //.ForMember(s => s.Person.Addresses, o => o.Ignore())
                .ForMember(s => s.ClusterId, o => o.Ignore());



            Mapper.CreateMap<Person, Person>()
                //.ForMember(m=>m.PrimaryAddressId,dest=>dest.MapFrom(fr=>fr.PrimaryAddress.Id))  //todo: Roy, add this later
                .ForMember(s => s.ClusterId, o => o.Ignore())
                .ForMember(s => s.TimeStamp, d => new TimeStamp());

            //Domain Object To ViewModel
            Mapper.CreateMap<Address, AddressViewModel>();
            Mapper.CreateMap<Address, Address>();
            Mapper.CreateMap<Application, ApplicationViewModel>();
            Mapper.CreateMap<ApplicationClass, ApplicationClassViewModel>();
            Mapper.CreateMap<Attachment, AttachmentViewModel>();
            Mapper.CreateMap<CategoryAttachment, CategoryAttachmentViewModel>();
            //Mapper.CreateMap<CategoryListType, CategoryListTypeViewModel>();
            Mapper.CreateMap<CategoryItem, CategoryItem>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<FileType, FileTypeViewModel>();

            Mapper.CreateMap<Meeting, MeetingViewModel>()
                .ForMember(dest => dest.MeetingID, m => m.MapFrom(s => s.Id.ToString()))
                //    .ForMember(dest => dest.Start, m => m.MapFrom(s => DateTime.SpecifyKind(s.Start, DateTimeKind.Utc)))
                //    .ForMember(dest => dest.End, m => m.MapFrom(s => DateTime.SpecifyKind(s.End, DateTimeKind.Utc)))
                    .ForMember(dest => dest.Attendees, opt => opt.Ignore())
                .ForMember(dest => dest.Attendees, m => m.MapFrom(s => s.MeetingAttendees.Select(a => a.PersonId.ToString()).ToArray()));
            //    .ForMember(dest => dest.MeetingRoomId, m => m.MapFrom(s => s.MeetingRoomId.ToString()))
            //    .ForMember(dest => dest.MeetingLocationId, m => m.MapFrom(s => s.MeetingLocationId.ToString()));
            Mapper.CreateMap<MeetingAttendee, MeetingAttendeeViewModel>()
                .ForMember(dest => dest.Id, m => m.MapFrom(s => s.Id.ToString()));
            Mapper.CreateMap<MeetingLocation, MeetingLocationViewModel>();
            Mapper.CreateMap<MeetingRoom, MeetingRoomViewModel>();
            Mapper.CreateMap<MeetingLocation, ResourceViewModel>()
                .ForMember(dest => dest.Text, m => m.MapFrom(s => s.Name))
                .ForMember(dest => dest.Value, m => m.MapFrom(s => s.Id.ToString()));
            Mapper.CreateMap<MeetingRoom, ResourceViewModel>()
                .ForMember(dest => dest.Text, m => m.MapFrom(s => String.Format("{0} {1}", s.Name, s.Number)))
                .ForMember(dest => dest.Value, m => m.MapFrom(s => s.Id.ToString()));
            Mapper.CreateMap<Task, TaskViewModel>()
                .ForMember(dest => dest.TaskID, m => m.MapFrom(s => s.Id))
                .ForMember(dest => dest.Start, m => m.MapFrom(s => DateTime.SpecifyKind(s.Start, DateTimeKind.Utc)))
                .ForMember(dest => dest.End, m => m.MapFrom(s => DateTime.SpecifyKind(s.End, DateTimeKind.Utc)));
            Mapper.CreateMap<Person, ResourceViewModel>()
                .ForMember(dest => dest.Text, m => m.MapFrom(s => String.Format("{0} {1}", s.FirstName, s.Lastname)))
                .ForMember(dest => dest.Value, m => m.MapFrom(s => s.Id.ToString()));
            //Mapper.CreateMap<Meeting, Meeting>()
            //    .ForMember(dest => dest.Id, opt => opt.Ignore())
            //    .ForMember(dest => dest.ClusterId, opt => opt.Ignore())
            //    .ForMember(dest => dest.MeetingAttendees, opt => opt.Ignore())
            //    .ForMember(dest => dest.SubMeetings, opt => opt.Ignore())
            //    .ForMember(dest => dest.IsAllDay, m => m.MapFrom(s => s.IsAllDay))
            //    .ForMember(dest => dest.MeetingLocationId, m => m.MapFrom(s => s.MeetingLocationId))
            //    .ForMember(dest => dest.MeetingRoomId, m => m.MapFrom(s => s.MeetingRoomId))
            //    .ForMember(dest => dest.RecurrenceException, m => m.MapFrom(s => s.RecurrenceException))
            //    .ForMember(dest => dest.RecurrenceId, m => m.MapFrom(s => s.RecurrenceId))
            //    .ForMember(dest => dest.Start, m => m.MapFrom(s => s.Start))
            //    .ForMember(dest => dest.End, m => m.MapFrom(s => s.End))
            //    .ForMember(dest => dest.StartTimezone, m => m.MapFrom(s => s.StartTimezone))
            //    .ForMember(dest => dest.EndTimezone, m => m.MapFrom(s => s.EndTimezone))
            //    .ForMember(dest => dest.Description, m => m.MapFrom(s => s.Description));
            Mapper.CreateMap<Category, CategoryTreeViewModel>();
            Mapper.CreateMap<Category, CategoryViewModel>();
            Mapper.CreateMap<CategoryItem, CategoryItemViewModel>()
                .ForMember(dest => dest.id, m => m.MapFrom(s => s.Id.ToString()))
                .ForMember(dest => dest.text, m => m.MapFrom(s => s.Text))
                .ForMember(dest => dest.spriteCssClass, m => m.MapFrom(s => s.SpriteCssClass))
                .ForMember(dest => dest.imageUrl, m => m.MapFrom(s => s.ImageUrl))
                .ForMember(dest => dest.expanded, m => m.MapFrom(s => s.Expanded))
                .ForMember(dest => dest.hasChildren, m => m.MapFrom(s => s.HasChildren));
            Mapper.CreateMap<FileType, FileTypeViewModel>();
            Mapper.CreateMap<ChangeLog, ChangeLogViewModel>();
            Mapper.CreateMap<EmailAddress, EmailAddressViewModel>();


            Mapper.CreateMap<Household, HouseholdViewModel>();
            Mapper.CreateMap<Household, HouseholdViewModel>()
               .ReverseMap()
               .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<HouseholdMember, HouseholdMemberViewModel>();
            Mapper.CreateMap<HouseholdMember, HouseholdMemberViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Person, HouseholdMemberViewModel>();


            Mapper.CreateMap<ListType, ListTypeViewModel>();

            Mapper.CreateMap<Person, PersonViewModel>();
            Mapper.CreateMap<PersonAddress, PersonAddressViewModel>();
            Mapper.CreateMap<PersonPhone, PersonPhoneViewModel>();
            Mapper.CreateMap<PersonEmailAddress, PersonEmailAddressViewModel>();
            //Mapper.CreateMap<EmailAddress, PersonEmailAddressViewModel>().ForMember(o=>o.EmailAddress);
            Mapper.CreateMap<Phone, PhoneViewModel>();

            Mapper.CreateMap<TimeStamp, TimeStampViewModel>();
            Mapper.CreateMap<Event, EventViewModel>();
            //Mapper.CreateMap<OH.DAL.Domain.Events.EventType, OH.BLL.Models.Events.EventType>();
            //Mapper.CreateMap<OH.DAL.Domain.Events.ShirtSize, OH.BLL.Models.Events.ShirtSize>();
            //Mapper.CreateMap<OH.DAL.Domain.Events.PriorityLevel, OH.BLL.Models.Events.PriorityLevel>();
            //Mapper.CreateMap<OH.DAL.Domain.Events.Grade, OH.BLL.Models.Events.Grade>();
            //Mapper.CreateMap<OH.DAL.Domain.Events.DistributionMemberRegistrationStatus, OH.BLL.Models.Events.DistributionMemberRegistrationStatus>();


            Mapper.CreateMap<CollectionEvent, CollectionEvent>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());


            Mapper.CreateMap<CollectionEvent, CollectionEventViewModel>()
                .ForMember(dest => dest.MeetingID, m => m.MapFrom(s => s.Id.ToString()));
            Mapper.CreateMap<CollectionLocation, CollectionLocationViewModel>();
            Mapper.CreateMap<CollectionVolunteer, CollectionVolunteerViewModel>();
            Mapper.CreateMap<Office, OfficeViewModel>();
            Mapper.CreateMap<DistributionEvent, DistributionEventViewModel>();
            Mapper.CreateMap<DistributionEventRegistrant, DistributionEventRegistrantViewModel>();
            Mapper.CreateMap<DistributionEventAttendee, DistributionEventAttendeeViewModel>();
            Mapper.CreateMap<DistributionLocation, DistributionLocationViewModel>();
            // Mapper.CreateMap<Registrant, RegistrantViewModel>();
            Mapper.CreateMap<CollectionLocationMapping, CollectionLocationMappingViewModel>();
            Mapper.CreateMap<CollectionVolunteerMapping, CollectionVolunteerMappingViewModel>();



            //ViewModel to Domain Object 
            Mapper.CreateMap<Address, AddressViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Address, Address>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Application, ApplicationViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<ApplicationClass, ApplicationClassViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Attachment, AttachmentViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<CategoryItem, CategoryItemViewModel>()
                .ForMember(dest => dest.id, m => m.MapFrom(s => s.Id.ToString()))
                .ForMember(dest => dest.text, m => m.MapFrom(s => s.Text))
                .ForMember(dest => dest.spriteCssClass, m => m.MapFrom(s => s.SpriteCssClass))
                .ForMember(dest => dest.imageUrl, m => m.MapFrom(s => s.ImageUrl))
                .ForMember(dest => dest.expanded, m => m.MapFrom(s => s.Expanded))
                .ForMember(dest => dest.hasChildren, m => m.MapFrom(s => s.HasChildren))
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());


            Mapper.CreateMap<CategoryAttachment, CategoryAttachmentViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            //Mapper.CreateMap<CategoryListType, CategoryListTypeViewModel>()
            //    .ReverseMap()
            //    .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Meeting, MeetingViewModel>()
                .ForMember(dest => dest.MeetingID, m => m.MapFrom(s => s.Id.ToString()))
                //.ForMember(dest => dest.Start, m => m.MapFrom(s => DateTime.SpecifyKind(s.Start, DateTimeKind.Utc)))
                //.ForMember(dest => dest.End, m => m.MapFrom(s => DateTime.SpecifyKind(s.End, DateTimeKind.Utc)))            
                .ForMember(dest => dest.Attendees, opt => opt.Ignore())
                .ForMember(dest => dest.Attendees, m => m.MapFrom(s => s.MeetingAttendees.Select(a => a.PersonId.ToString()).ToArray()))
                //.ForMember(dest => dest.MeetingRoomId, m => m.MapFrom(s => s.MeetingRoomId.ToString()))
                //.ForMember(dest => dest.MeetingLocationId, m => m.MapFrom(s => s.MeetingLocationId.ToString()))
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<MeetingLocation, MeetingLocationViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<MeetingRoom, MeetingRoomViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<MeetingAttendee, MeetingAttendeeViewModel>()
                .ForMember(dest => dest.Id, m => m.MapFrom(s => s.Id.ToString()))
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<HttpPostedFileBase, Attachment>()
                .ForMember(dest => dest.FileExtension, m => m.MapFrom(s => Path.GetExtension(s.FileName)))
                .ForMember(dest => dest.FileStream,
                    m => m.MapFrom(s => Utilities.ConvertStreamToByteArray(s.InputStream)))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore())
                .ForMember(dest => dest.OriginalFileName, opt => opt.Ignore())
                .ForMember(dest => dest.ChangeLog, opt => opt.Ignore());
            Mapper.CreateMap<Task, TaskViewModel>()
                    .ForMember(dest => dest.TaskID, m => m.MapFrom(s => s.Id))
                    .ForMember(dest => dest.Start, m => m.MapFrom(s => DateTime.SpecifyKind(s.Start, DateTimeKind.Utc)))
                    .ForMember(dest => dest.End, m => m.MapFrom(s => DateTime.SpecifyKind(s.End, DateTimeKind.Utc)))
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            //Mapper.CreateMap<Category, CategoryTreeViewModel>().ReverseMap();
            Mapper.CreateMap<Category, CategoryViewModel>().ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<FileType, FileTypeViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<ChangeLog, ChangeLogViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<EmailAddress, EmailAddressViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            Mapper.CreateMap<ListType, ListTypeViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Person, PersonViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            Mapper.CreateMap<PersonPhone, PersonPhoneViewModel>().ReverseMap().ForMember(d => d.Person, s => s.Ignore()).ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<Phone, PhoneViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            Mapper.CreateMap<PersonEmailAddress, PersonEmailAddressViewModel>().ReverseMap().ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<EmailAddress, PersonEmailAddressViewModel>().ReverseMap().ForMember(d => d.ClusterId, s => s.Ignore());

            Mapper.CreateMap<PersonAddress, PersonAddressViewModel>().ReverseMap().ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            Mapper.CreateMap<TimeStamp, TimeStampViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Event, EventViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            //Mapper.CreateMap<CollectionEvent, CollectionEvent>()
            //    .ForMember(dest => dest.Id, opt => opt.Ignore())
            //    .ReverseMap();



            Mapper.CreateMap<CollectionEventViewModel, CollectionEvent>()

                .ForMember(dest => dest.Id, m => m.MapFrom(s => s.MeetingID == null ? Guid.NewGuid() : new Guid(s.MeetingID)))
                //.ForMember(dest => dest.Locations, opt => opt.Ignore())
                //.ForMember(dest => dest.Volunteers, opt => opt.Ignore())
                //.ForMember(dest => dest.Registrants, opt => opt.Ignore())
                //.ForMember(dest => dest.MeetingAttendees, opt => opt.Ignore())
                //.ForMember(dest => dest.SubMeetings, opt => opt.Ignore())
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<CollectionLocation, CollectionLocationViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<CollectionVolunteer, CollectionVolunteerViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<Office, OfficeViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<DistributionEvent, DistributionEventViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<DistributionEventRegistrant, DistributionEventRegistrantViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<DistributionEventAttendee, DistributionEventAttendeeViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());
            Mapper.CreateMap<DistributionLocation, DistributionLocationViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore());

            Mapper.CreateMap<CollectionLocationMapping, CollectionLocationMappingViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore())
                .ForMember(dest => dest.CollectionEvent, opt => opt.Ignore());
            Mapper.CreateMap<CollectionVolunteerMapping, CollectionVolunteerMappingViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ClusterId, opt => opt.Ignore())
                .ForMember(dest => dest.CollectionEvent, opt => opt.Ignore());



            Mapper.CreateMap<ListInjuryInflictionCategory, ListDisplayViewModel>().ReverseMap().ForMember(d => d.ClusterId, s => s.Ignore());

            Mapper.CreateMap<ListMilitaryBranch, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore())
                .ForMember(d => d.OptionalValue, s => s.Ignore());

            Mapper.CreateMap<ListMilitaryCombatTheater, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<ListMilitaryDeploymentStatus, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<ListMilitaryRank, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<ListMilitaryServiceStatus, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<ListMilitaryVeteranStatus, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore());
            Mapper.CreateMap<Gender, ListTypeViewModel>().ReverseMap()
                .ForMember(d => d.ClusterId, s => s.Ignore())
                .ForMember(d => d.ObjectSource, s => s.Ignore())
                .ForMember(d => d.OptionalValue, s => s.Ignore());
        }
    }
}