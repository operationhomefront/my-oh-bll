﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using OH.BLL.Models.Persons;
using OH.DAL.Domain.Persons;

namespace OH.BLL.Models.Households
{
    public class HouseholdMemberViewModel
    {
        public Guid Id { get;  set; }
        public string Grade { get; set; }
        public Guid HouseholdId { get; set; }
        public HouseholdViewModel HouseholdViewModel { get; set; }
        public Guid HouseholdMemberType { get; set; }
        public bool IsDependent { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsServiceMember { get; set; }
        public string LivesWith { get; set; }
        public Guid PersonId { get; set; }
        public PersonViewModel Person { get; set; }
    }
}