﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Households
{
    public class HouseholdViewModel
    {
        public Guid Id { get; set; }

        public String Name { get; set; }

        public ICollection<HouseholdMemberViewModel> HouseholdMembers { get; set; }
    }
}
