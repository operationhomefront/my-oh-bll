﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.DAL.Domain.Persons;

namespace OH.BLL.Models.Schedulers
{
    public class MeetingAttendeeViewModel
    {
        public string Id { get; set; }

        public string MeetingId { get; set; }

        public MeetingViewModel Meeting { get; set; }

        public string PersonId { get; set; }

        public PersonViewModel Person { get; set; }
    }
}
