﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Schedulers
{
    public class ResourceViewModel
    {
        public string Text { get; set; }

        public string Value { get; set; }

        public string Color { get; set; }
    }
}
