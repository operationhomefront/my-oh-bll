﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Kendo.Mvc.UI;

namespace OH.BLL.Models.Schedulers
{
    public class MeetingViewModel : ISchedulerEvent
    {
        public MeetingViewModel()
        {
            //MeetingID = Guid.NewGuid().ToString();
            Rooms = new List<string>();
            Attendees = new List<string>();
            EditorTemplateName = "IndividualAttendeeTemplate";
        }

        public string EditorTemplateName { get; set; }

        public string MeetingID { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime startUtc;
        [Required]
        public DateTime Start
        {
            get
            {
                return startUtc;
            }
            set
            {
                startUtc = value.ToUniversalTime();
            }
        }

        public string StartTimezone { get; set; }

        private DateTime endUtc;

        [Required]
        public DateTime End
        {
            get
            {
                return endUtc;
            }
            set
            {
                endUtc = value.ToUniversalTime();
            }
        }

        public string EndTimezone { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        [Required]
        public bool IsAllDay { get; set; }
        public string Timezone { get; set; }
        public IEnumerable<string> Rooms { get; set; }
        public IEnumerable<string> Attendees { get; set; }

        public Guid? MeetingLocationId { get; set; }

        public Guid? MeetingRoomId { get; set; }


    }
}