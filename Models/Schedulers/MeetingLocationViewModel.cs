﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Phones;

namespace OH.BLL.Models.Schedulers
{
    public class MeetingLocationViewModel
    {
        public Guid? Id { get; set; }

        public AddressViewModel Address { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<MeetingRoomViewModel> Rooms { get; set; }

        public IEnumerable<PhoneViewModel> LocationPhoneNumbers { get; set; }

        public IEnumerable<EmailAddressViewModel> LocationEmailAddresses { get; set; }

        public IEnumerable<PersonViewModel> ContactPersons { get; set; }

        public MeetingLocationViewModel()
        {
            //Id=Guid.NewGuid();
            Address = new AddressViewModel();
        }
    }
}
