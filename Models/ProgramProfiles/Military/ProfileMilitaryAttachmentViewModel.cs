﻿using System;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Persons;

namespace OH.BLL.Models.ProgramProfiles.Military
{
    public class ProfileMilitaryAttachmentViewModel
    {

        public Guid Id { get; set; }

        public PersonMilitaryViewModel MilitaryProfile { get; set; }

        public AttachmentViewModel Attachment { get; set; }


    }
}
