﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ListTypes;

namespace OH.BLL.Models.ProgramProfiles.Military
{
    public class ProfileMilitarySelectors
    {
        public ProfileMilitarySelectors()
        {
            BranchOfService = new Collection<ListTypeViewModel>();
            Rank = new Collection<ListTypeViewModel>();
            Gender = new Collection<ListTypeViewModel>();
            ListRelationship = new Collection<ListTypeViewModel>();
            ServiceStatus = new Collection<ListTypeViewModel>();
            DeploymentStatus = new Collection<ListTypeViewModel>();
            VeteranStatus = new Collection<ListTypeViewModel>();
        }
        public IEnumerable<ListTypeViewModel> BranchOfService { get; set; }
        public IEnumerable<ListTypeViewModel> Gender { get; set; }
        public IEnumerable<ListTypeViewModel> ListRelationship { get; set; }
        public IEnumerable<ListTypeViewModel> ServiceStatus { get; set; }
        public IEnumerable<ListTypeViewModel> Rank { get; set; }
        public IEnumerable<ListTypeViewModel> DeploymentStatus { get; set; }
        public IEnumerable<ListTypeViewModel> VeteranStatus { get; set; }
        public IEnumerable<ListTypeViewModel> InjuryInflictionCategory { get; set; }
        
    }
 
}
