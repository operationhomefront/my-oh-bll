﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OH.BLL.Models.Households;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.WorkFlow;

namespace OH.BLL.Models.ProgramProfiles.Military
{
    public class ProfileMilitaryViewModel
    { 
        public Guid Id { get; set; }

        public HouseholdViewModel Household { get; set; }
        
        public Guid MilitaryProfileId { get; set; }
        public PersonMilitaryViewModel MilitaryProfile { get; set; }
        public ProfileMilitarySelectors Selectors { get; set; }
        public ICollection<ProgramProfileWorkFlowViewModel> WorkFlowItems { get; set; }
        
        public ProfileMilitaryViewModel()
        {
            MilitaryProfile = new PersonMilitaryViewModel();
            Selectors = new ProfileMilitarySelectors();
            WorkFlowItems = new Collection<ProgramProfileWorkFlowViewModel>(); 
        }
        
    }
}
