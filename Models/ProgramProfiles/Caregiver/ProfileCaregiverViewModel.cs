﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Util;
using AutoMapper;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Households;
using OH.BLL.Models.ListTypes;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.Military;
using OH.BLL.Models.ProgramProfiles.WorkFlow;
using OH.BLL.Models.TimeStamps;
using OH.DAL.Domain.Households;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.TimeStamps;

namespace OH.BLL.Models.ProgramProfiles.Caregiver
{
    public class ProfileCaregiverViewModel  
    {
        public Guid Id { get; set; }
        public Guid CaregiverId { get; set; }
        public CareGiverViewModel Profile { get; set; }
        public HouseholdViewModel Household { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }
        public ICollection<WorkItem> WorkFlowItems { get; set; }
        public ProfileCaregiverViewModel()
        {
            //Based on workflow request add a specifi number of documents to the default collection
            //Profile = Mapper.Map<DAL.Domain.Persons.PersonCaregiver, CareGiverViewModel>(new DAL.Domain.Persons.PersonCaregiver());
            //Profile.Attachments = new CareGiverAttachmentViewModel();
            //Selectors = new ProfileCaregiverSelectors();
            //WorkFlowItems = new Collection<ProgramProfileWorkFlowViewModel>();
            TimeStamp = Mapper.Map<TimeStamp, TimeStampViewModel>(new TimeStamp());
            //Household = Mapper.Map<Household, HouseholdViewModel>(new Household{Id = Guid.NewGuid()});
            //generate default lists
        }
        public ProfileCaregiverViewModel(Guid Id)
        {
            //Based on workflow request add a specifi number of documents to the default collection
            Profile = Mapper.Map<PersonCaregiver, CareGiverViewModel>(new PersonCaregiver{Id = Id});
            //Profile.Attachments = new CareGiverAttachmentViewModel();
            //Selectors = new ProfileCaregiverSelectors();
            //WorkFlowItems = new Collection<ProgramProfileWorkFlowViewModel>();
            //Household = new HouseholdViewModel();
            //generate default lists
        }
    }
}
