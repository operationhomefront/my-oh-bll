﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ListTypes;


namespace OH.BLL.Models.ProgramProfiles.Caregiver
{
    public class ProfileCaregiverSelectors
    {
        public ProfileCaregiverSelectors()
        {
            ListGender = new Collection<ListTypeViewModel>();
            ListAgeRange = new Collection<ListTypeViewModel>();
            ListDurationProvidingCare = new Collection<ListTypeViewModel>();
            ListHoursProvidingCare = new Collection<ListTypeViewModel>();
            ListRelationship = new Collection<ListTypeViewModel>();
        }
        public IEnumerable<ListTypeViewModel> ListAgeRange { get; set; }
        public IEnumerable<ListTypeViewModel> ListDurationProvidingCare { get; set; }
        public IEnumerable<ListTypeViewModel> ListGender { get; set; }
        public IEnumerable<ListTypeViewModel> ListHoursProvidingCare { get; set; }
        public IEnumerable<ListTypeViewModel> ListRelationship { get; set; }
    }
}
