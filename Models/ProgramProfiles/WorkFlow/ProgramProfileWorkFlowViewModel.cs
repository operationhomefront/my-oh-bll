﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Models.WorkFlow;

namespace OH.BLL.Models.ProgramProfiles.WorkFlow
{
    public class ProgramProfileWorkFlowViewModel : WorkFlowViewModel
    {
        public ProfileCaregiverSelectors Selectors { get; set; }

    }
}
