﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.Military;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.ProgramProfiles
{
    public class InjuryInflictionViewModel
    {
        public Guid Id { get; set; }
        public string BriefDescription { get; set; }
        public string DetailDescription { get; set; }
        public Guid? TypeOfInjury { get; set; }
        public string Cause { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? InjuryInflictionCategoryId { get; set; }
        public DateTime? DateOfOccurrence { get; set; }
        public Guid? Disposition { get; set; }
        public Guid MilitaryProfileId { get; set; }
        public PersonMilitaryViewModel MilitaryProfile { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }
    }
}
