﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Domain.Forms;
using OH.DAL.Domain.TimeStamps;

namespace OH.BLL.Models.Forms
{
    public class OHFormControlViewModel
    {
        public Guid Id { get; set; }

        public OHFormControl.OHFormControlType Type { get; set; }
        public int SequencerId { get; set; }
        public OHFormDataType FormDataType { get; set; }
        public Boolean Required { get; set; }
        public Object OriginalValue { get; set; }
        public String Label { get; set; }
        public String DataName { get; set; }
        public Boolean FullWidthProperties { get; set; }
        public dynamic Properties { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public String Fields { get; set; }
        public OHFormViewModel Form { get; set; }
        public OHFormSectionViewModel Section { get; set; }
        public OHFormDescriptionPropertiesViewModel DescriptionProperties { get; set; }
        public OHFormEmbeddedHtmlPropertiesViewModel EmbeddedHtmlProperties { get; set; }
        public OHFormCheckBoxPropertiesViewModel CheckBoxProperties { get; set; }
        public OHFormRadioButtonsPropertiesViewModel RadioButtonsProperties { get; set; }
        public OHFormServiceMemberPropertiesViewModel ServiceMemberProperties { get; set; }
        public OHFormUmbracoPropertiesViewModel UmbracoProperties { get; set; }
        public OHFormDateTimePropertiesViewModel DateTimeProperties { get; set; }
        public OHFormSelectListPropertiesViewModel SelectListProperties { get; set; }
        public OHFormNumberPropertiesViewModel NumberProperties { set; get; }

        public OHFormFileUploadPropertiesViewModel FileUploadProperties { set; get; }
        public Boolean Validate { get; set; }
           
        
    }
}
