﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Domain.Forms;

namespace OH.BLL.Models.Forms
{
    public class OHFormNumberPropertiesViewModel
    {
          public Guid Id { get; private set; }

        public OHFormNumberType Type { get; set; }
        public Int32 MinValue { get; set; }
        public Int32 MaxValue { get; set; }
    }
    
}
