﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormUmbracoPropertiesViewModel
    {
        public Guid Id { get; private set; }
        public int UmbracoId { get; set; }
    }
}
