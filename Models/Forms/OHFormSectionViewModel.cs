﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormSectionViewModel
    {
        public Guid Id { get; private set; }

        public String SectionTitle { get; set; }

        public String SectionDescription { get; set; }

        public List<OHFormControlViewModel> SectionControls { get; set; }
    }
}
