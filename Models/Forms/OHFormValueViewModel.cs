﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormValueViewModel
    {
        public Guid Id { get; private set; }
        public OHFormViewModel Form { get; set; }
        public String DataValue { get; set; }
        public String DataName { get; set; }
        public OHFormControlViewModel Control { get; set; }
    }
}
