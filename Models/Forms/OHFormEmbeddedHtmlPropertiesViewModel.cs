﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormEmbeddedHtmlPropertiesViewModel
    {
        public Guid Id { get; private set; }
        public String RawHtml { get; set; }
    }
}
