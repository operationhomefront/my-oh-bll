﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormServiceMemberPropertiesViewModel
    {
        public Guid Id { get; private set; }

        public Boolean EmailAddressDisplay { get; set; }
        public Boolean EmailAddressValidate { get; set; }
        public Boolean EmailAddressPrefill { get; set; }

        public Boolean SMFirstNameDisplay { get; set; }
        public Boolean SMFirstNameValidate { get; set; }
        public Boolean SMFirstNamePrefill { get; set; }

        public Boolean SMLastNameDisplay { get; set; }
        public Boolean SMLastNameValidate { get; set; }
        public Boolean SMLastNamePrefill { get; set; }

        public Boolean SMLast4SSNDisplay { get; set; }
        public Boolean SMLast4SSNValidate { get; set; }
        public Boolean SMLast4SSNPrefill { get; set; }

        public Boolean SecQuestionDisplay { get; set; }
        public Boolean SecQuestionValidate { get; set; }
        public Boolean SecQuestionPrefill { get; set; }

        public Boolean SecAnswerDisplay { get; set; }
        public Boolean SecAnswerValidate { get; set; }
        public Boolean SecAnswerPrefill { get; set; }
        public Boolean EmailAddressConfirmedDisplay { get; set; }
        public Boolean EmailAddressConfirmedValidate { get; set; }
        public Boolean EmailAddressConfirmedPrefill { get; set; }
        public Boolean FirstNameDisplay { get; set; }
        public Boolean FirstNameValidate { get; set; }
        public Boolean FirstNamePrefill { get; set; }

        public Boolean LastNameDisplay { get; set; }
        public Boolean LastNameValidate { get; set; }
        public Boolean LastNamePrefill { get; set; }
        public Boolean MailingAddressDisplay { get; set; }
        public Boolean MailingAddressValidate { get; set; }
        public Boolean MailingAddressPrefill { get; set; }

        public Boolean PhysicalAddressDisplay { get; set; }
        public Boolean PhysicalAddressValidate { get; set; }
        public Boolean PhysicalAddressPrefill { get; set; }
        public Boolean DateOfBirthDisplay { get; set; }
        public Boolean DateOfBirthValidate { get; set; }
        public Boolean DateOfBirthPrefill { get; set; }

        public Boolean SendNewsletterDisplay { get; set; }
        public Boolean SendNewsletterValidate { get; set; }
        public Boolean SendNewsletterPrefill { get; set; }

        public Boolean SendTextMsgNotificationsDisplay { get; set; }
        public Boolean SendTextMsgNotificationsVaildate { get; set; }
        public Boolean SendTextMsgNotificationsPrefill { get; set; }

        public Boolean TimeStampDisplay { get; set; }
        public Boolean TimeStampValidate { get; set; }
        public Boolean TimeStampPrefill { get; set; }

        public Boolean GoldStarFamilyDisplay { get; set; }
        public Boolean GoldStarFamilyValidate { get; set; }
        public Boolean GoldStarFamilyPrefill { get; set; }

        public Boolean ShirtSizeDisplay { get; set; }
        public Boolean ShirtSizeVaildate { get; set; }
        public Boolean ShirtSizePrefill { get; set; }

        public Boolean PhoneDisplay { get; set; }
        public Boolean PhoneValidate { get; set; }
        public Boolean PhonePrefill { get; set; }

        public Boolean MobilePhoneDisplay { get; set; }
        public Boolean MobilePhoneValidate { get; set; }
        public Boolean MobilePhonePrefill { get; set; }

        public Boolean DocumentsDisplay { get; set; }
        public Boolean DocumentsValidate { get; set; }
        public Boolean DocumentsPrefill { get; set; }

        public Boolean DependentsDisplay { get; set; }
        public Boolean DependentsValidate { get; set; }
        public Boolean DependentsPrefill { get; set; }
        //MILITARY PROFILE PROPERTIES
        public Boolean ActiveDutyDisplay { get; set; }
        public Boolean ActiveDutyValidate { get; set; }
        public Boolean ActiveDutyPrefill { get; set; }

        public Boolean CommandDisplay { get; set; }
        public Boolean CommandValidate { get; set; }
        public Boolean CommandPrefill { get; set; }

        public Boolean ReservesDisplay { get; set; }
        public Boolean ReservesValidate { get; set; }
        public Boolean ReservesPrefill { get; set; }

        public Boolean WoundedDisplay { get; set; }
        public Boolean WoundedValidate { get; set; }
        public Boolean WoundedPrefill { get; set; }

        public Boolean YrsServiceDisplay { get; set; }
        public Boolean YrsServiceValidate { get; set; }
        public Boolean YrsServicePrefill { get; set; }

        public Boolean MilitaryBaseDisplay { get; set; }
        public Boolean MilitaryBaseValidate { get; set; }
        public Boolean MilitaryBasePrefill { get; set; }

        public Boolean BranchDisplay { get; set; }
        public Boolean BranchValidate { get; set; }
        public Boolean BranchPrefill { get; set; }

        public Boolean RankDisplay { get; set; }
        public Boolean RankValidate { get; set; }
        public Boolean RankPrefill { get; set; }

        public Boolean DeployedStatusDisplay { get; set; }
        public Boolean DeployedStatusValidate { get; set; }
        public Boolean DeployedStatusPrefill { get; set; }

        public Boolean MilitaryStatusDisplay { get; set; }
        public Boolean MilitaryStatusValidate { get; set; }
        public Boolean MilitaryStatusPrefill { get; set; }

        public Boolean VaStatusDisplay { get; set; }
        public Boolean VaStatusValidate { get; set; }
        public Boolean VaStatusPrefill { get; set; }
    }
}
