﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using OH.DAL.Domain.Forms;
using OH.DAL.Domain.TimeStamps;

namespace OH.BLL.Models.Forms
{
    public class OHFormViewModel
    {


        [Required]
        public String Name { get; set; }
        public String SubmitButtonText { get; set; }
        public String CancelButtonText { get; set; }
        public OHFormTheme Theme { get; set; }
        public TimeStamp TimeStamp { get; set; }

        //public virtual List<Section> Sections { get; set; } 
        public List<OHFormControlViewModel> Controls { get; set; }
       

    }

}
