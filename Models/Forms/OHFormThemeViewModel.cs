﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Domain.TimeStamps;

namespace OH.BLL.Models.Forms
{
    public class OHFormThemeViewModel
    {
        public Guid Id { get; private set; }


        public String Name { get; set; }
        //
        // Form
        public Color BackgroundColor { get; set; }
        public Boolean ShowBorder { get; set; }
        public Color BorderColor { get; set; }
        public String TopOfFormImage { get; set; }
        public Int32 CornerRadius { get; set; }
        //
        // Row
        public Color FormRowEven { get; set; }
        public Color FormRowOdd { get; set; }
        public Color FormRowSelected { get; set; }
        //
        // Labels
        public Color FontColor { get; set; }
        public String FontName { get; set; }
        public String FontSize { get; set; }
        //
        // Sections
        public Color SectionBgColor { get; set; }
        public Boolean ShowSectionBorder { get; set; }
        public Color SectionBorderColor { get; set; }
        public Color SectionFontColor { get; set; }
        public String SectionFontName { get; set; }
        public String SectionFontSize { get; set; }
        public TimeStamp TimeStamp { get; set; }
        //
        // Buttons
        public Color ButtonColor { get; set; }
        public Color ButtonFontColor { get; set; }
    }
}
