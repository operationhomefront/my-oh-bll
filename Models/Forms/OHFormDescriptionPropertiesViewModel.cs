﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormDescriptionPropertiesViewModel
    {
        public Guid Id { get; private set; }

        //[UIHint(@"tinymce_jquery_full"), AllowHtml]
        public string Description { get; set; }
    }
}
