﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Forms
{
    public class OHFormFileUploadPropertiesViewModel
    {
        public Guid Id { get; private set; }

        public String AllowedExtensions { get; set; }
        public String UploadButtonText { get; set; }
        public Int32 MaxKbFileSize { get; set; }
        public String UploaderHtml { get; set; }
    }
}
