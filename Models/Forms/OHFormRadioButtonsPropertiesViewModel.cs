﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Domain.Forms;

namespace OH.BLL.Models.Forms
{
    public class OHFormRadioButtonsPropertiesViewModel
    {
        public Guid Id { get; private set; }

        public ButtonOrientation Orientation { get; set; }
        //public List<RbProperty>     RbList      { get; set; }
        public String RadioName { get; set; }
        public String RadioValues { get; set; }
    }
}
