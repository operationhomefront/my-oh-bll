﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Domain.ChangeLogs;

namespace OH.BLL.Models.Attachments
{
    public class AttachmentViewModel
    {
        public Guid Id { get; set; }

        public String OriginalFileName { get; set; }

        public String FileName { get; set; }

        public String FileExtension { get; set; }
        //TODO: Add to separate table, and also add thumbnail 
        public byte[] FileStream { get; set; }

        public ChangeLog ChangeLog { get; set; }
    }
}
