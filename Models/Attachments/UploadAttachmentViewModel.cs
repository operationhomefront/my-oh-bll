﻿using System;
using System.Collections.Generic;
using System.Web;
using OH.BLL.Models.Categories;
using OH.BLL.Models.ChangeLogs;
using OH.DAL.Domain.Categories;
using OH.DAL.Domain.ChangeLogs;

namespace OH.BLL.Models.Attachments
{
    public class UploadAttachmentViewModel
    {
        public UploadAttachmentViewModel()
        {
            Id = Guid.NewGuid();
            Files = new List<HttpPostedFileBase>();
            Categories = new List<CategoryViewModel>();
            ChangeLog = new ChangeLogViewModel();
        }

        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public Guid OwnerId { get; set; }

        public IEnumerable<CategoryViewModel> Categories { get; set; }

        public IEnumerable<HttpPostedFileBase> Files { get; set; }

        public ChangeLogViewModel ChangeLog { get; set; }
    }

}