﻿using System;
using System.Collections.Generic;
using OH.DAL.Domain;

namespace OH.BLL.Models
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Category = new Category
            {
                SubCategories = new List<Category>(),
                Attachments = new List<Attachment>(),
                ChangeLog = new ChangeLog()
            };
        }

        public CategoryViewModel(Category category)
        {
            Category = category;
        }

        public Guid SelectedCategoryId { get; set; }
        public Category Category { get; set; }
    }
}