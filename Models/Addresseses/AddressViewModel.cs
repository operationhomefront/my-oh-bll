﻿using System;
using System.Data.Entity.Spatial;
using OH.BLL.Helpers;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.Addresseses
{
    public class AddressViewModel
    {
       

        public Guid Id { get; set; }

        [PlaceHolder("Address")]
        public string Address1 { get; set; }
        [PlaceHolder("Address 2")]
        public string Address2 { get; set; }
        [PlaceHolder("City")]
        public string City { get; set; }
        public string State { get; set; }
        [PlaceHolder("Zip")]
        public string Zip { get; set; }
        public DbGeography Location { get; set; }
        public string Verified { get; set; }
        public string County { get; set; }
        public string Country { get; set; }

        public TimeStampViewModel TimeStamp { get; set; }
        public bool IsActive { get; set; }

        public AddressViewModel()
        {
            //Id=Guid.NewGuid();
            TimeStamp = new TimeStampViewModel();
            IsActive = true;
        }
    }
}
