﻿using System;

namespace OH.BLL.Models.TimeStamps
{
    public class TimeStampViewModel
    {
        public Guid? Id { get;  set; }

        public String CreatedBy { get; set; }

        public DateTime UtcCreatedDate { get; set; }

        public String LastModifiedBy { get; set; }

        public DateTime UtcLastModifiedDate { get; set; }

        public TimeStampViewModel()
        {
            //Id=Guid.NewGuid();
            UtcCreatedDate = DateTime.UtcNow;
            UtcLastModifiedDate = DateTime.UtcNow;
        }
    }
}