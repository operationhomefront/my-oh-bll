﻿using System;

namespace OH.BLL.Models.EmailAddresses
{
    public class EmailAddressViewModel
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public bool DoNotSend { get; set; }
    }
}
