﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.UI;

namespace OH.BLL.Models.WorkFlow
{
    public class WorkFlowViewModel
    {
        public Guid Id { get; set; }

        public Guid? Owner { get; set; }
        public string Summary { get; set; }
        //public TimeStamp TimeStamp { get; set; }
        public string Title { get; set; }
        //public ICollection<WorkflowItem> WorkFlowItems { get; set; }
    }

    public class WorkflowItemDependencyViewModel : IGanttDependency
    {
        public Guid Id { get; set; }

        public Guid DependencyId { get; set; }
        public Guid PredecessorId { get; set; }
        public Guid SuccessorId { get; set; }
       // public TimeStamp TimeStamp { get; set; }
        public DependencyType Type { get; set; }

        DependencyType IGanttDependency.Type
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
    public class WorkflowItemViewModel : IGanttTask
    {
        public Guid Id { get; set; }

        private DateTime start;
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value.ToUniversalTime();
            }
        }

        private DateTime end;
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                end = value.ToUniversalTime();
            }
        }

        public bool Expanded { get; set; }
        public int OrderId { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? ParentWorkFlowItemId { get; set; }
        public decimal PercentComplete { get; set; }
        public bool Summary { get; set; }
       // public TimeStamp TimeStamp { get; set; }
        public string Title { get; set; }

        //[NotMapped]
        //public virtual WorkflowItem WorkflowItem1 { get; set; }
        //[NotMapped]
       // public virtual ICollection<WorkflowItem> WorkFlowItems1 { get; set; }
        public Guid WorkFlowId { get; set; }
       // public virtual Workflow Workflow { get; set; }

        //public ICollection<ProfileMilitary> ProfilesMilitary { get; set; }
       // public ICollection<ProfileCaregiver> ProfilesCaregiver { get; set; }

    }
}
