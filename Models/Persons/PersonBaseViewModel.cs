﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Helpers;

namespace OH.BLL.Models.Persons
{
    public class PersonBaseViewModel
    {
        public Guid Id { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [Required]
        [RequiredAs("true")]
        [Display(Name = "FirstName")]
        [PlaceHolder("First Name")]
        public string FirstName { get; set; }
        public string Gender { get; set; }
        [Required]
        [Display(Name = "Last name")]
        [RequiredAs("true")]
        [PlaceHolder("Last Name")]
        public string Lastname { get; set; }
        public Guid? MailingAddress { get; set; }
        public bool MailingSameAs { get; set; }
        public string MiddleName { get; set; }
        public Guid? PhysicalAddress { get; set; }
        public string PrimaryEmailAddress { get; set; }
        public string Suffix { get; set; }
        public string Title { get; set; }

    }
}
