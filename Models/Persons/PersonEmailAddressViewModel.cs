﻿using System;
using System.Collections.ObjectModel;
using OH.BLL.Models.EmailAddresses;

namespace OH.BLL.Models.Persons
{
    public class PersonEmailAddressViewModel
    {
        public Guid Id { get; set; }
        public Guid EmailAddressId { get; set; }
        public EmailAddressViewModel EmailAddress { get; set; }
        public Guid PersonId { get; set; }
    }
}