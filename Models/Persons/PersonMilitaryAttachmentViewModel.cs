﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Categories;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.Persons
{
    public class PersonMilitaryAttachmentViewModel : PersonAttachmentViewModel
    {
        
        public Guid ServiceMemberId { get; set; }
    }
}
