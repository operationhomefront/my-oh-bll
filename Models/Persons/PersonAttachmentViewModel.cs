﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Attachments;

namespace OH.BLL.Models.Persons
{
    public class PersonAttachmentViewModel
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public Guid OwnerId { get; set; }
        public IEnumerable<AttachmentViewModel> Files { get; set; }
    }
}
