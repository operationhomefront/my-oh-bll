﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.ProgramProfiles;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.Persons
{
    public class PersonMilitaryViewModel 
    {
        public PersonMilitaryViewModel()
        {

        }
        public PersonMilitaryViewModel(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }

        public PersonMilitaryAttachmentViewModel PersonMilitaryAttachments { get; set; }
        public Guid? BranchOfServiceId { get; set; }
        public string CommandPost { get; set; }
        public Guid? DeploymentStatusId { get; set; }
        public Collection<InjuryInflictionViewModel> InjuryInflictions { get; set; }
        public Guid? MilitaryRankId { get; set; }
        public bool NeedsUpdataing { get; set; }
        
        public Guid ServiceStatusId { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }
        public Guid? VeteranStatusId { get; set; } //Mili
        public bool Wounded { get; set; }
        public int YearsServed { get; set; }
        public PersonViewModel MilitaryPerson { get; set; }

        //**************   For display UIX only  *********************
        public bool injuryTBI { get; set; }
        public bool injuryPTSD { get; set; }
        public bool injuryAmputee { get; set; }
        public bool injuryMentalIlliness { get; set; }
        public bool injuryParalysis { get; set; }
        public bool injuryBlindness { get; set; }
        public string injuryTelling { get; set; }
        public string injuryOther { get; set; }


    }
}
