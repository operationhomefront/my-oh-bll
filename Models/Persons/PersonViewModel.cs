﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using OH.BLL.Helpers;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.EmailAddresses;
using OH.BLL.Models.Phones;
using OH.BLL.Models.TimeStamps;
using OH.BLL.Models.Events;

namespace OH.BLL.Models.Persons
{
    public class PersonViewModel
    {
        public PersonViewModel()
        {
            PhysicalAddress = new PersonAddressViewModel();
            MailingAddress = new PersonAddressViewModel();
            PrimaryPhone = new PhoneViewModel();
            Addresses = new Collection<PersonAddressViewModel>();
            TimeStamp = new TimeStampViewModel();
            EmailAddresses = new Collection<PersonEmailAddressViewModel>();
        }
        
        public Guid Id { get;  set; }

        [Required]
        [RequiredAs("true")]
        [Display(Name = "FirstName")]
        [PlaceHolder("First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [RequiredAs("true")]
        [PlaceHolder("Last Name")]
        public string Lastname { get; set; }

        public string MiddleName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Title { get; set; }

        public string Suffix { get; set; }

        public string Gender { get; set; }

        //TODO: Make sure this is hashed in the database
        public string DriverLicenceNumber { get; set; }

        public string DriverLicenseState { get; set; }

        public DateTime? DriverLicenseExpirationDate { get; set; }

        public string Last4Ssn { get; set; }

        //TODO: Make sure this is hashed in the database
        public string SocialSecurityNumber { get; set; }
        
        public Guid? PrimaryEmailId { get; set; }
        public  EmailAddressViewModel PrimaryEmail { get; set; }

        public Guid? PhysicalAddressId { get; set; }
        public PersonAddressViewModel PhysicalAddress { get; set; }

        public Guid? MailingAddressId { get; set; }
        public PersonAddressViewModel MailingAddress { get; set; }

        public Guid? PrimaryPhoneId { get; set; }
        public  PhoneViewModel PrimaryPhone { get; set; }

        public TimeStampViewModel TimeStamp { get; set; }
        public string  ShirtSize { get; set; }

        public  ICollection<PersonPhoneViewModel> Phones { get; set; }

        public  ICollection<PersonAddressViewModel> Addresses { get; set; }

        public  ICollection<PersonEmailAddressViewModel> EmailAddresses { get; set; }
    }
}