﻿using System;
using OH.BLL.Models.Phones;

namespace OH.BLL.Models.Persons
{
    public class PersonPhoneViewModel
    {
        public Guid Id { get; set; }

        public Guid PhoneId { get; set; }

        public PhoneViewModel Phone { get; set; }

        public Guid PersonId { get; set; }

        public PersonViewModel Person { get; set; }       
    }
}