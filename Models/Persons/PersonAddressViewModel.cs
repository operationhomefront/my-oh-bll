﻿using System;
using OH.BLL.Models.Addresseses;

namespace OH.BLL.Models.Persons
{
    public class PersonAddressViewModel
    {
        public PersonAddressViewModel()
        {
            //Id = Guid.NewGuid();
            Address=new AddressViewModel();
        }
        public Guid Id { get;  set; }

        public Guid AddressId { get; set; }

        public AddressViewModel Address { get; set; }

        public Guid? PersonId { get; set; }
        public Guid? AddressType { get; set; }

        //public PersonViewModel Person { get; set; }        
    }
}