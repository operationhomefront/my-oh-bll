﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using OH.BLL.Helpers;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.Persons
{
    public class CareGiverViewModel
    {
        public CareGiverViewModel()
        {
            Id = Guid.NewGuid();
        }

        public CareGiverViewModel(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }

        public CareGiverAttachmentViewModel Attachments
        {
            get; set;
        }
        [Required]
        public string AgeRange { get; set; }
        [Required]
        public Guid? DurationOfTimeProvidingCare { get; set; }
       
        public string GreatestChallengeAsCaregiver { get; set; }
        public bool HasChildren { get; set; }
        public string HobbiesOrInterests { get; set; }
        [Required]
        public Guid? HoursProvidingCareWeekly { get; set; }
        public int HowManyChildren { get; set; }
        public bool NeedsUpdataing { get; set; }
        public bool NotifyIfOpportunity { get; set; }
        
        [PlaceHolder("Occupation")]
        public string Occupation { get; set; }
        public bool ProfileActive { get; set; }
        public bool RegisteredCaregiverWithVa { get; set; }
        
        [Required]
        public Guid? RelationToVeteran { get; set; }
        public bool SendNewsletter { get; set; } //todo: should be moved to default workflow status 
        public string TellAboutWork { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }
        public bool WorkOutsideHome { get; set; }
        public Guid PersonId { get; set; }
        public PersonViewModel Person { get; set; }

    }
}
