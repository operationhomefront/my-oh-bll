﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace OH.BLL.Models.Navigation
{
    public class AdminNavigationItemViewModel
    {
        public string CategoryName { get; set; }
        public string Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public List<AdminNavigationSubItemViewModel> SubCategories { get; set; }
    }

    public class AdminNavigationSubItemViewModel
    {
        public string SubCategoryName { get; set; }
        public string Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }

        public List<AdminNavigationSubItemViewModel> SubCategories { get; set; }
    }
}