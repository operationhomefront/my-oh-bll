﻿using System;
using OH.BLL.Helpers;

namespace OH.BLL.Models.Phones
{
    public class PhoneViewModel
    {
        public Guid Id { get; set; }

        [PlaceHolder("Phone Number")]
        public string PhoneNumber { get; set; }
        
        [PlaceHolder("Ext")]
        public string Extension { get; set; }

        public string PhoneType { get; set; }
    }
}