﻿using OH.BLL.Models.Schedulers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.Events
{
    public class EventViewModel
    {
        public string MeetingID { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime startUtc;
        [Required]
        public DateTime Start
        {
            get
            {
                return startUtc;
            }
            set
            {
                startUtc = value.ToUniversalTime();
            }
        }

        public string StartTimezone { get; set; }

        private DateTime endUtc;

        [Required]
        public DateTime End
        {
            get
            {
                return endUtc;
            }
            set
            {
                endUtc = value.ToUniversalTime();
            }
        }

        public string EndTimezone { get; set; }

        public string Partner { get; set; }
        [Required]
        public Guid EventTypeId { get; set; }
        public bool? PublishToEventCalendar { get; set; }
        public string FormNbr { get; set; }


        public DateTime? PublishToWebUtc { get; set; }
        public DateTime? UnpublishFromWebUtc { get; set; }


        [Required]
        public DateTime RegistrationOpensUtc { get; set; }
        [Required]
        public DateTime RegistrationClosesUtc { get; set; }

        [Required]
        public bool EventClosed { get; set; }
        public bool? RemoveFromWeb { get; set; }
        public string ContactListName { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }

        [Required]
        public Guid OwnerId { get; set; }
        public OfficeViewModel Owner { get; set; }
        public string ParentEventKey { get; set; }
        public MeetingViewModel Meetting { get; set; }
        public EventViewModel() 
        {
            Title = "";
            Partner = "";
            Description = "";
            EventTypeId = new Guid("B1B81060-5F78-400C-937F-181A8F00B900");
            RegistrationOpensUtc = DateTime.UtcNow;
            RegistrationClosesUtc = DateTime.UtcNow;

            Start = RegistrationOpensUtc;
            End = RegistrationClosesUtc;


           // IsAllDay = false;
            EventClosed = false;
            FormNbr = "";
            ContactListName = "";
            TimeStamp = new TimeStampViewModel();
            ParentEventKey = "";
        }
    }
}
