﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Events
{
    public class OfficeViewModel
    {
        public Guid Id { get; set; }
        public String InternalName { get; set; }

    }
}
