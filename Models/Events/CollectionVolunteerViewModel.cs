﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using OH.BLL.Helpers;
using OH.BLL.Models.ListTypes;
using OH.BLL.Models.Persons;
using System.Web.Mvc;

namespace OH.BLL.Models.Events
{
    public class CollectionVolunteerViewModel : PersonViewModel
    {
        [Required(ErrorMessage = "Please select an event.")]
        [Display(Name = "Event")]
        public Guid? CollEventId { get; set; }
        public Int32 PickupRadiusLimit { get; set; }
        public Int32 MaxLocationsAllowed { get; set; }
        public String AffiliatedWith { get; set; }
        public String DistributingTo { get; set; }
        public String MilitaryBase { get; set; }
        public String Unit { get; set; }
        public DateTime PledgeConfirmedDate { get; set; }
        public string AddressCity { get { return this.PhysicalAddress == null ? "" : (this.PhysicalAddress.Address == null ? "" : this.PhysicalAddress.Address.City); } }
        public string AddressState { get { return this.PhysicalAddress == null ? "" : (this.PhysicalAddress.Address == null ? "" : this.PhysicalAddress.Address.State); } }

        public string Phone
        {
            get
            {
                var c = this.PrimaryPhone == null ? "" : this.PrimaryPhone.PhoneNumber.StripNonNumeric();
                switch (c.Length)
                {
                    case 7:
                        return String.Format("{0}-{1}", c.Substring(0, 3), c.Substring(3, 4));
                    case 10:
                        return String.Format("({0}) {1}-{2}", c.Substring(0, 3), c.Substring(3, 3), c.Substring(6, 4));
                    default:
                        return c;
                }
            }
        }
        public string Email { get { return this.PrimaryEmail == null ? "" : this.PrimaryEmail.Email; } }
        public IEnumerable<SelectListItem> ListTitle { get; private set; }
        public IEnumerable<SelectListItem> radiusList { get; private set; }
        public IEnumerable<SelectListItem> maxStores { get; private set; }
        public CollectionVolunteerViewModel()
            : base()
        {
            //Id = Guid.NewGuid();
            PledgeConfirmedDate = DateTime.UtcNow;
            ListTitle =TitleTypes.ComboList().Select(a =>new SelectListItem(){Text = a.Key,Value = a.Key,Selected = (a.Key == this.Title)}).ToList();
            var m = new List<SelectListItem>();
            for (int i = 1; i < 101; i++)
                m.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            maxStores = m;
            radiusList = new Dictionary<String, Int32>
            {
                {"5 Miles", 5},
                {"10 Miles", 10},
                {"15 Miles", 15},
                {"20 Miles", 20},
                {"25 Miles", 25},
                {"30 Miles", 30},
                {"35 Miles", 35},
                {"40 Miles", 40},
                {"45 Miles", 45},
                {"50 Miles", 50},
                {"75 Miles", 75},
                {"100 Miles", 100},
                {"150 Miles", 150},
                {"200 Miles", 200},
                {"300 Miles", 300},
                {"400 Miles", 400},
                {"500 Miles", 500},
                {"750 Miles", 750},
                {"1000 Miles", 1000}
            }.Select(a => new SelectListItem() { Text = a.Key, Value = a.Value.ToString() }).ToList();
        }
    }
}
