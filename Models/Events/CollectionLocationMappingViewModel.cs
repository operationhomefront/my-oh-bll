﻿using System;

namespace OH.BLL.Models.Events
{
    public class CollectionLocationMappingViewModel
    {
                public Guid Id { get;  set; }

        public Guid CollectionEventId { get; set; }
        //public CollectionEventViewModel CollectionEvent { get; set; }
        public Guid CollectionLocationId { get; set; }
        public CollectionLocationViewModel CollectionLocation { get; set; }


        
    }
}
