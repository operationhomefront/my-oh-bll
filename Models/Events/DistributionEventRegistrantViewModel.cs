﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Helpers;
using OH.BLL.Models.Persons;
using OH.BLL.Models.TimeStamps;
using OH.DAL.Domain.TimeStamps;

namespace OH.BLL.Models.Events
{
    public class DistributionEventRegistrantViewModel : RegistrantViewModel
    {        

        public new string ShirtSizeId { get; set; }
        
        //*******************************************************

        public DistributionEventRegistrantViewModel()
        {
            RegistrationEmailSent = (DateTime) SqlDateTime.MinValue;
            ConfirmationEmailSent = (DateTime) SqlDateTime.MinValue;
            ReminderEmailSent = (DateTime) SqlDateTime.MinValue;
            TimeSlot = (DateTime) SqlDateTime.MinValue;
            //RegistrationStatus = new DistributionMemberRegistrationStatus() { Id = new Guid("22B7CFE5-E3FC-43A1-A733-11D39C3DC141"), EnumValue = 0, DisplayOrder = 0, DisplayName = "None" };
            FirstName = "";
            Lastname = "";
            RegistrantEmail = "";
            TimeStamp = new TimeStampViewModel();
        }
    }
}
