﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Phones;
using OH.BLL.Models.Schedulers;
using OH.BLL.Models.TimeStamps;

namespace OH.BLL.Models.Events
{
    public class CollectionLocationViewModel :MeetingLocationViewModel
    {
        public string PointOfInterest { get; set; }
        public string PointOfContact { get; set; }
        public int StoreNumber { get; set; }
        public double HoursOfOperation { get; set; }
        public bool AddressValid { get; set; }
        public bool NotAvailable { get; set; }
        public DateTime OpeningOnUtc { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }
        public CollectionVolunteerViewModel Volunteer { get; set; }
        public string AddressCity { get { return this.Address == null ? "" : this.Address.City; } }
        public string AddressState { get { return this.Address == null ? "" : this.Address.State; } }
        public string VolunteerName { get { return this.Volunteer == null ? "" : this.Volunteer.FirstName; } }
        public string AvailOn { get { return this.OpeningOnUtc.ToShortDateString() + " " + OpeningOnUtc.ToShortTimeString(); } }
        public string Phone { get { return this.LocationPhoneNumbers == null ? "" : this.LocationPhoneNumbers.FirstOrDefault(x => x.PhoneType == "Main") == null ? "" : this.LocationPhoneNumbers.FirstOrDefault(x => x.PhoneType == "Main").PhoneNumber; } }

        public CollectionLocationViewModel():base()
        {
            AddressValid = true;
            NotAvailable = false;
            OpeningOnUtc = DateTime.UtcNow;
            TimeStamp=new TimeStampViewModel();
            //Volunteer=new CollectionVolunteerViewModel();
        }
    }
}
