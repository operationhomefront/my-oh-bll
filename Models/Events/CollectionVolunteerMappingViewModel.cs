﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ListTypes;

namespace OH.BLL.Models.Events
{
    public class CollectionVolunteerMappingViewModel
    {
        public Guid Id { get; set; }

        public Guid CollectionEventId { get; set; }
        //public CollectionEventViewModel CollectionEvent { get; set; }
        public Guid CollectionVolunteerId { get; set; }
        public CollectionVolunteerViewModel CollectionVolunteer { get; set; }


    }
}
