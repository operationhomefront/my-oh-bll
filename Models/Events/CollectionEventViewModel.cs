﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Events
{
    public class CollectionEventViewModel : EventViewModel
    {
        public Int32 RegistrationRadius { get; set; }
        public Int32 MaxStoresPerVolunteer { get; set; }

        public DateTime? CollectionBeginsUtc { get; set; }
        public DateTime? CollectionEndsUtc { get; set; }


        public IEnumerable<CollectionLocationViewModel> Locations { get; set; }
        public IEnumerable<CollectionVolunteerMappingViewModel> Volunteers { get; set; }

        //public string NumLocations { get { return (Locations == null || !Locations.Any()) ? "0" : Locations.Count().ToString("#,##0"); } }
        //public string NumLocationsFormatted { get { return (Locations == null || !Locations.Any()) ? "0" : "<span class='tTip'><a title='" + this.LocationsDetailed + "'>" + this.NumLocations + "</a></span>"; } }
        //public string LocationsDetailed { get { return (Locations == null || !Locations.Any()) ? "N/A" : Locations.Select(a=>a.CollectionLocation).Select(a => a.Address.Address1 + " " + a.Address.City + ", " + a.Address.State + " (" + this.RegistrationOpensUtc.ToShortDateString() + "-" + this.RegistrationClosesUtc.ToShortDateString() + ")").Aggregate((c, n) => c + "<br/>" + n); } }
        //public string NumVolunteers { get { return (Volunteers == null || !Volunteers.Any()) ? "0" : Volunteers.Count().ToString("#,##0"); } }
        //public string NumVolunteersFormatted { get { return (Volunteers == null || !Volunteers.Any()) ? "0" : "<span class='tTip'><a title='" + this.VolunteersDetailed + "'>" + this.NumVolunteers + "</a></span>"; } }
       // public string VolunteersDetailed { get { return (Volunteers == null || !Volunteers.Any()) ? "N/A" : Volunteers.Select(a=>a.CollectionVolunteer).Select(x => x.FirstName + " " + x.Lastname).Aggregate((c, n) => c + "<br/>" + n); } }

        public IEnumerable<OfficeViewModel> Participants { get; set; }

        public string NatLoc { get { return (this.Owner ?? new OfficeViewModel()).InternalName == "National" ? "Nat" : "Loc"; } }

        public string NumLocations { get ; set; } 
        public string NumLocationsFormatted {  get ; set; }
        public string LocationsDetailed {  get ; set; }
        public string NumVolunteers {  get ; set; }
        public string NumVolunteersFormatted {  get ; set; }
        public string VolunteersDetailed {  get ; set;}


        public CollectionEventViewModel() : base()
        {
            

        }

    }
}
