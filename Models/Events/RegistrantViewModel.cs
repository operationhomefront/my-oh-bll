﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Persons;

namespace OH.BLL.Models.Events
{
    public class RegistrantViewModel :PersonViewModel
    {
        public DateTime? RegistrationEmailSent { get; set; }
        public DateTime? ConfirmationEmailSent { get; set; }
        public DateTime? ReminderEmailSent { get; set; }
        public DateTime? TimeSlot { get; set; }
        public Guid RegistrationStatusId { get; set; }
        public new String FirstName { get; set; }
        public new String Lastname { get; set; }
        public String RegistrantEmail { get; set; }
    }
}
