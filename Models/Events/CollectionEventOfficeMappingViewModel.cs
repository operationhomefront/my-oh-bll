﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Events
{
    public class CollectionEventOfficeMappingViewModel
    {
        public Guid Id { get; set; }
        public Guid CollectionEventId { get; set; }
        public Guid CollectionOfficeId { get; set; }
    }
}
