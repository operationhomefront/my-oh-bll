﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Events
{
    public class DistributionEventViewModel: EventViewModel
    {
        public Boolean? AutoConfirm { get; set; }
        public Boolean? WaitList { get; set; }
        public Int32 WaitListMax { get; set; }
        public Boolean? CloseOnFull { get; set; }
        public String EtapCampaign { get; set; }
        public String EtapApproach { get; set; }
        public String EtapGiftOwner { get; set; }
        public String EtapFund { get; set; }
        //
        // Email And Reminders
        //                  
        public String RegAckEmailText { get; set; }
        public String RegWaitListEmailText { get; set; }
        public String RegConfirmEmailText { get; set; }
        public String RegReminderEmailText { get; set; }
        public DateTime? Reminder1Date { get; set; }
        public DateTime? Reminder2Date { get; set; }
        public DateTime? Reminder3Date { get; set; }

        public DateTime? Reminder1SentDate { get; set; }
        public DateTime? Reminder2SentDate { get; set; }
        public DateTime? Reminder3SentDate { get; set; }



        public int CountScheme { get; set; }

        //Registration Requirements
        public Boolean? ServiceMember { get; set; }
        public Boolean? Mother { get; set; }
        public Boolean? Father { get; set; }
        public Boolean? Brother { get; set; }
        public Boolean? Sister { get; set; }
        public Boolean? Son { get; set; }
        public Boolean? Daughter { get; set; }
        public Boolean? Spouse { get; set; }
        public Boolean? Friend { get; set; }
        public Boolean? StepSon { get; set; }
        public Boolean? StepDaughter { get; set; }
        public Boolean? Other { get; set; }
        public Boolean? PrivateEvent { get; set; }

        public Int32 AgeMin { get; set; }
        public Int32 AgeMax { get; set; }

        public Guid GradeMin { get; set; }
        public Guid GradeMax { get; set; }

        public IEnumerable<DistributionLocationViewModel> Locations { get; set; }
        public IEnumerable<OfficeViewModel> Participants { get; set; }

        public string NumLocations { get { return (Locations == null || !Locations.Any()) ? "0" : Locations.Count().ToString("#,##0"); } }
        public string NumLocationsFormatted { get { return (Locations == null || !Locations.Any()) ? "0" : "<span class='tTip'><a title='" + this.LocationsDetailed + "'>" + this.NumLocations + "</a></span>"; } }
        public string LocationsDetailed { get { return (Locations == null || !Locations.Any()) ? "N/A" : Locations.Select(a => a.Address.Address1 + " " + a.Address.City + ", " + a.Address.State + " (" + (a.RegistrationOpensUtc ?? this.RegistrationOpensUtc).ToShortDateString() + "-" + (a.RegistrationClosesUtc ?? this.RegistrationClosesUtc).ToShortDateString() + ")").Aggregate((c, n) => c + "<br/>" + n); } }
        public string ParticipantsDetailed { get { return (Participants == null || !Participants.Any()) ? "N/A" : Participants.Select(a => a.InternalName).Aggregate((c, n) => c + "<br/>" + n); } }
        public string NumAttendees { get { return (Locations == null || !Locations.Any()) ? "0" : Locations.Select(a => a.Attendees).Count().ToString("#,##0"); } }
        public string NumRegistrants { get { return (Locations == null || !Locations.Any()) ? "0" : Locations.Select(a => a.Registrants).Count().ToString("#,##0"); } }
        public string NumRegistrantsFormatted { get { return (Locations == null || !Locations.Any()) ? "0" : "<span class='tTip'><a title='" + this.LocationsDetailed + "'>" + this.NumLocations + "</a></span>"; } }
        public string RegistrantsDetailed { get { return (Locations == null || !Locations.Any()) ? "N/A" : Locations.SelectMany(a => a.Registrants).Select(x => x.FirstName + " " + x.Lastname).Aggregate((c, n) => c + "<br/>" + n); } }
        public DistributionEventViewModel()
        {

            AutoConfirm = true;
            WaitList = false;
            WaitListMax = 0;
            CloseOnFull = true;
            EtapCampaign = "";
            EtapApproach = "";
            EtapGiftOwner = "";
            EtapFund = "";

            RegAckEmailText = "";
            RegConfirmEmailText = "";
            RegReminderEmailText = "";
            RegWaitListEmailText = "";
            Reminder1Date = DateTime.Now;
            Reminder2Date = DateTime.Now;
            Reminder3Date = DateTime.Now;

            Reminder1SentDate = new DateTime(1753, 1, 1);
            Reminder2SentDate = new DateTime(1753, 1, 1);
            Reminder3SentDate = new DateTime(1753, 1, 1);

            EventTypeId = new Guid("B1B81060-5F78-400C-937F-181A8F00B900");

            ServiceMember = false;
            Mother = false;
            Father = false;
            Brother = false;
            Sister = false;
            Son = false;
            Daughter = false;
            Spouse = false;
            Friend = false;
            StepSon = false;
            StepDaughter = false;
            Other = false;

            PrivateEvent = false;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Locations = new List<DistributionLocationViewModel>();
            Participants = new List<OfficeViewModel>();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
            AgeMin = -1;
            AgeMax = -1;


            CountScheme = 4;    //Standard count participants

        }

    }
}
