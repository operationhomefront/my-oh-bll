﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.Schedulers;
using OH.BLL.Models.TimeStamps;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.TimeStamps;

namespace OH.BLL.Models.Events
{
    public class DistributionLocationViewModel : MeetingLocationViewModel
    {
        public String Place { get; set; }
        public String EventDetails { get; set; }
        public String LocationDetails { get; set; }
        public Int32 StartingInventory { get; set; }
        public Int32 RegistrationRadius { get; set; }

        public DateTime? StartDateTimeUtc { get; set; }
        public DateTime? EndDateTimeUtc { get; set; }

        public Boolean? RemoveFromWeb { get; set; }
        public Boolean? AutoConfirm { get; set; }
        public Boolean? WaitList { get; set; }
        public Int32 WaitListMax { get; set; }
        public Boolean? CloseOnFull { get; set; }
        public Boolean? LocationClosed { get; set; }
        public Boolean? EnableTimeSlots { get; set; }
        public Int32 TimeSlotMinutes { get; set; }
        public Guid PriorityLevelId { get; set; }
        
        public String RegAckEmailText { get; set; }
        public String RegWaitListEmailText { get; set; }
        public String RegConfirmEmailText { get; set; }
        public String RegReminderEmailText { get; set; }
        public DateTime? Reminder1Date { get; set; }
        public DateTime? Reminder2Date { get; set; }
        public DateTime? Reminder3Date { get; set; }
        public DateTime? Reminder1Sent { get; set; }
        public DateTime? Reminder2Sent { get; set; }
        public DateTime? Reminder3Sent { get; set; }
        public TimeStampViewModel TimeStamp { get; set; }

        public DateTime? RegistrationOpensUtc { get; set; }
        public DateTime? RegistrationClosesUtc { get; set; }

        public string UmbracoContentId { get; set; }

        public string LocationContact { get; set; }



        public  IEnumerable<DistributionEventRegistrantViewModel> Registrants { get; set; }
        public IEnumerable<DistributionEventAttendeeViewModel> Attendees { get; set; }

       
        //**********************************************************

    }
}
