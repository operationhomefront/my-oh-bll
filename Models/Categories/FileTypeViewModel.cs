﻿using System;

namespace OH.BLL.Models.Categories
{
    public class FileTypeViewModel
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string Extension { get; set; }

        public string CssClass { get; set; }
    }
}
