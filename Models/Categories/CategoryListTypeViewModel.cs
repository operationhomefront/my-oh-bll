﻿using System;
using OH.BLL.Models.ListTypes;

namespace OH.BLL.Models.Categories
{
    public class CategoryListTypeViewModel
    {
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public CategoryViewModel Category { get; set; }

        public Guid ListTypeId { get; set; }

        public ListTypeViewModel ListType { get; set; }        
    }
}
