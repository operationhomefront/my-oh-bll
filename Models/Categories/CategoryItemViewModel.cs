﻿using System;
using System.Collections.Generic;

namespace OH.BLL.Models.Categories
{
    public class CategoryItemViewModel
    {
        public CategoryItemViewModel()
        {
            id = Guid.NewGuid().ToString();
        }

        public string id { get; set; }
        public string text { get; set; }
        public string spriteCssClass { get; set; }
        public string imageUrl { get; set; }
        public bool expanded { get; set; }
        public bool hasChildren { get; set; }
        public IEnumerable<CategoryItemViewModel> items { get; set; }
        

        public CategoryItemViewModel Clone()
        {
            var clone = new CategoryItemViewModel
            {
                id = id,
                imageUrl = imageUrl,
                spriteCssClass = spriteCssClass,
                text = text,
                expanded = expanded,
                hasChildren = hasChildren
            };
            return clone;
        }
    }
}
