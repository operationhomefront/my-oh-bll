﻿

using System;
using OH.BLL.Models.Attachments;

namespace OH.BLL.Models.Categories
{
    public class CategoryAttachmentViewModel
    {
  
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public CategoryViewModel Category { get; set; }

        public Guid AttachmentId { get; set; }

        public UploadAttachmentViewModel Attachment { get; set; }        
    }
}
