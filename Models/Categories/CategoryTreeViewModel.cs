﻿using System.Collections.Generic;
using System.Linq;
using OH.BLL.Models.Attachments;
using OH.DAL.Domain.Categories;

namespace OH.BLL.Models.Categories
{
    public class CategoryTreeViewModel
    {
        public CategoryTreeViewModel()
        {
        }

        public CategoryTreeViewModel(Category category)
        {
            Id = category.Id.ToString();
            Category = new List<TreeViewItemViewModel> {category.TreeViewItem}.AsEnumerable();
            SelectedCategoryIds = new string[0];
        }

        public string Id { get; set; }

        public string[] SelectedCategoryIds { get; set; }

        public IEnumerable<TreeViewItemViewModel> Category { get; set; }

        public UploadAttachmentViewModel Attachment { get; set; }
    }

}