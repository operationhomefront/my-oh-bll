﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OH.BLL.Models.Categories
{
    public class CategoryViewModel
    {

        public Guid Id { get; set; }

        public  CategoryViewModel ParentCategory { get; set; }

        [Required]
        public String Name { get; set; }

    }
}
