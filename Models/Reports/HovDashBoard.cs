﻿using System.Collections.Generic;
using OH.BLL.Models.Events;
using OH.BLL.Models.Persons;

namespace OH.BLL.Models.Reports
{
    public class HovDashBoard
    {


        #region collapse

        public IList<CareGiver> CareGiverReport { get; set; }

        public IList<CollectionEventViewModel> CollectionEvent { get; set; }

        public IList<PersonViewModel> GetAllPersonses { get; set; }

        public int TotalNumberCareGivers
        {
            get
            {
                return (this.TotalAdminCareGivers + this.TotalLevel1CareGivers + this.TotalLevel2CareGivers +
                        this.TotalModeratorCareGivers);
            }
        }

        public int TotalAdminCareGivers { get; set; }

        public int TotalModeratorCareGivers { get; set; }

        public int TotalLevel1CareGivers { get; set; }
        public int TotalLevel2CareGivers { get; set; }


        public HovDashBoard()
        {
            this.CollectionEvent = new List<CollectionEventViewModel>();
            this.CareGiverReport = new List<CareGiver>()
                {
               new CareGiver(){ CareGiverCatagory = "Admin", NumberNewMonth	= 0, NumberNewWeek	= 0},
               new CareGiver(){ CareGiverCatagory = "Level1", NumberNewMonth	= 2, NumberNewWeek	= 5},
               new CareGiver(){ CareGiverCatagory = "Level2", NumberNewMonth	= 2, NumberNewWeek	= 31},
                          new CareGiver(){ CareGiverCatagory = "Moderator", NumberNewMonth	= 0, NumberNewWeek	= 0}
                };


            this.TotalAdminCareGivers = 15;
            this.TotalLevel1CareGivers = 0;
            this.TotalLevel2CareGivers = 2650;
            this.TotalModeratorCareGivers = 3;
        }

    }

    public class CareGiver
    {
        public string CareGiverCatagory { get; set; }

        public int NumberNewWeek { get; set; }

        public int NumberNewMonth { get; set; }



        

        #endregion
    }
}