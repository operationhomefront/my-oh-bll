﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.BLL.Models.Reports
{
    public class Chart
    {
        public string ChartName { get; set; }
        public string ChartTitle { get; set; }
        public bool ShowToolTips { get; set; }
        public bool ShowLegend { get; set; }
        public bool ShowLabels { get; set; }

        public string[] ColorsLight { get { return new string[] { "#CACACA", "#B1B0E2", "#CEC0AF", "#B3D0BF", "#C4EBFF", "#9BC1FF" }; } }
        public string[] ColorsMedium { get { return new string[] { "#A4A4A4", "#6666C6", "#AF977D", "#6BA281", "#A7E0FF", "#6CA2FF" }; } }
        public string[] ColorsDark { get { return new string[] { "#7F7F7F", "#4F4EBC", "#906F4A", "#3F8458", "#7ED1FF", "#4185FF" }; } }

        public string[] ColorsGray { get { return new string[] { "#7F7F7F", "#A4A4A4", "#CACACA" }; } }
        public string[] ColorsRed { get { return new string[] { "#4F4EBC", "#6666C6", "#B1B0E2" }; } }
        public string[] ColorsBlue { get { return new string[] { "#906F4A", "#AF977D", "#CEC0AF" }; } }
        public string[] ColorsGreen { get { return new string[] { "#3F8458", "#6BA281", "#B3D0BF" }; } }
        public string[] ColorsYellow { get { return new string[] { "#4185FF", "#6CA2FF", "#C4EBFF" }; } }
        public string[] ColorsOrange { get { return new string[] { "#3F8458", "#6BA281", "#9BC1FF" }; } }

        public Chart()
        {
            this.ShowToolTips = true;
            this.ShowLegend = true;
            this.ShowLabels = true;
        }
    }
}