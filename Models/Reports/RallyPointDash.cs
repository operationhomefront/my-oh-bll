﻿using System;
using System.Collections.Generic;
using System.Linq;
using OH.BLL.Models.Events;

namespace OH.BLL.Models.Reports
{
    public class RallyPointDash
    {

        public List<CollectionEventViewModel> CollectionEvents { get; set; }
        public List<DistributionEventViewModel> DistributionEvents { get; set; }

        public int ActiveDistributionEvents { 
            get
            {
                return DistributionEvents.Count(d => d.EventClosed == false);
            } 
        }
        public int ActiveCollectionEvents
        {
            get
            {
                return CollectionEvents.Count(c => c.EventClosed == false);
            }
        }

        public int AverageStoresToVolunteer
        {
            get
            {
                return (ActiveCollectionEvents / TotalActiveCollectionVolunteers) ;
            }
        }

        public int TotalActiveCollectionVolunteers
        {
            get
            {
                return CollectionEvents.Sum(c => Int32.Parse(c.NumVolunteers));
            }
        }
    }
}