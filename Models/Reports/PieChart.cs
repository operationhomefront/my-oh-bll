﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.BLL.Models.Reports
{
    public class PieChart : Chart
    {
        public ICollection<PieChartSeries> PieChartData { get; set; }

        public PieChart()
        {
            this.PieChartData = new List<PieChartSeries>();
        }
    }

    public class PieChartSeries
    {
        public string PieDataCatagory { get; set; }
        public double PieDataValue { get; set; }
        public string PieDataColorValue { get; set; }
    }
}