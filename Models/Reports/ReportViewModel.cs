﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.BLL.Models.Reports
{
   public class ReportViewModel
    {
        public int ClusterId { get; set; }

        public Guid Id { get; set; }
        public string ReportId { get; set; }
        public string ReportName { get; set; }
        public int SelectLimit { get; set; }
        public string StoredProcedureName { get; set; }
    }    
}
