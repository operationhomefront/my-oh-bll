﻿using OH.DAL.Domain;
using System;
using System.Collections.Generic;
using System.Web;
using OH.DAL.Repositories;

namespace OH.BLL.Models
{
    public class AttachmentViewModel
    {
        public AttachmentViewModel()
        {
            Files = new List<HttpPostedFileBase>();
            Categories = new List<Category>();
            ChangeLog = new ChangeLog();
        }

        public Guid Id { get; set; }

        public Guid SelectedCategoryId { get; set; }

        public IEnumerable<Category> Categories { get; set; }

        public IEnumerable<HttpPostedFileBase> Files { get; set; }

        public ChangeLog ChangeLog { get; set; }
    }

}