﻿using System;

namespace OH.BLL.Models.ListTypes
{
    public class ListTypeViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int? SortOrder { get; set; }

        public bool? ShowInCombo { get; set; }

        public int? EnumValue { get; set; }

        public Guid? CategoryListTypeId { get; set; }

        public string OptionalValue { get; set; }

    }
}