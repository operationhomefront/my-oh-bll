﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.BLL.Models.ProgramProfiles;

namespace OH.BLL.Models.ListTypes
{
    public class ListDisplayViewModel
    {
        public IEnumerable<ListTypeViewModel> ListDisplays { get; set; } 
    }
}
