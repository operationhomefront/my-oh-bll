﻿using System;


namespace OH.BLL.Models.Applications
{
    public class ApplicationClassViewModel
    {
        public Guid Id { get; set; }

        public Guid ApplicationId { get; set; }
        public virtual ApplicationViewModel Application { get; set; }

        public Guid EntityClassGuid { get; set; }
        
        public string EntityName { get; set; }
    }
}
