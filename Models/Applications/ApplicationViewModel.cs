﻿using System;

namespace OH.BLL.Models.Applications
{
    public class ApplicationViewModel
    {
  
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
