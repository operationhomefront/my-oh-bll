﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace OH.BLL.Models
{
    public class NavigationItemViewModel
    {
        public string CategoryName { get; set; }
        public RouteValueDictionary Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public List<NavigationSubItemViewModel> SubCategories { get; set; }
    }

    public class NavigationSubItemViewModel
    {
        public string SubCategoryName { get; set; }
        public RouteValueDictionary Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
    }
}