﻿using System;
using OH.BLL.Models.Attachments;

namespace OH.BLL.Attachments
{
    public interface IAttachmentBllService
    {
        UploadAttachmentViewModel InitializeUploader();
        void UploadAttachment(UploadAttachmentViewModel model);

        void DeleteAttachment(Guid attachmentId);
    }
}
