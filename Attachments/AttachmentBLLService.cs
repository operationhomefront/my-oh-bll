﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using AutoMapper;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Categories;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Attachments;
using OH.DAL.Repositories.Categories;

namespace OH.BLL.Attachments
{
    public class AttachmentBllService : Services.Attachments.IAttachmentBllService
    {
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryAttachmentRepository _categoryAttachmentRepository;

        public AttachmentBllService(IAttachmentRepository attachmentRepository, ICategoryRepository categoryRepository, ICategoryAttachmentRepository categoryAttachmentRepository)
        {
            _attachmentRepository = attachmentRepository;
            _categoryRepository = categoryRepository;
            _categoryAttachmentRepository = categoryAttachmentRepository;
        }

        public UploadAttachmentViewModel InitializeUploader()
        {
            var categories = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(_categoryRepository.GetAll());

            var model = new UploadAttachmentViewModel { Categories = categories };

            return model;
        }

        public void UploadAttachment(UploadAttachmentViewModel model)
        {
            //var attachment = Mapper.Map<AttachmentViewModel, Attachment>(model);

            var attachmentIds = model.Files.Select(file => new Attachment
            {
                FileName = file.FileName,
                FileExtension = Path.GetExtension(file.FileName),
                FileStream = ConvertStreamToByteArray(file.InputStream),
                OwnerId = model.OwnerId,
                RefId = Guid.Empty,


            }).Select(attachment => _attachmentRepository.AddOrUpdate(attachment)).ToList();

            _attachmentRepository.Save();


            foreach (var attachmentId in attachmentIds)
            {
                _categoryAttachmentRepository.AddOrUpdate(new CategoryAttachment { CategoryId = model.CategoryId, AttachmentId = attachmentId, });
            }
            _categoryAttachmentRepository.Save();




        }

        //public void UploadAttachment(UploadAttachmentViewModel model)
        //{
        //    //var attachment = Mapper.Map<AttachmentViewModel, Attachment>(model);

        //    var attachmentIds = model.Files.Select(file => new Attachment
        //    {
        //        FileName = file.FileName,
        //        FileExtension = Path.GetExtension(file.FileName),
        //        FileStream = ConvertStreamToByteArray(file.InputStream)

        //    }).Select(attachment => _attachmentRepository.AddOrUpdate(attachment)).ToList();
        //    _attachmentRepository.Save();

        //    foreach (var category in model.CategoryIds.Select(categoryId => _categoryRepository.GetById(new Guid(categoryId))))
        //    {
        //        foreach (var attachmentId in attachmentIds)
        //        {
        //            _categoryAttachmentRepository.AddOrUpdate(new CategoryAttachment{CategoryId = category.Id,AttachmentId = attachmentId});
        //        }
        //        _categoryAttachmentRepository.Save();
        //    }



        //}

        public void DeleteAttachment(Guid attachmentId)
        {
            _attachmentRepository.Delete(attachmentId);
            _attachmentRepository.Save();
        }

        public static byte[] ConvertStreamToByteArray(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        private static System.Drawing.Image ResizeImage(System.Drawing.Image sourceImage, int targetWidth, int targetHeight)
        {
            float ratioWidth = (float)sourceImage.Width / targetWidth;
            float ratioHeight = (float)sourceImage.Height / targetHeight;
            if (ratioWidth > ratioHeight)
                targetHeight = (int)(targetHeight * (ratioHeight / ratioWidth));
            else
            {
                if (ratioWidth < ratioHeight)
                    targetWidth = (int)(targetWidth * (ratioWidth / ratioHeight));
            }
            var targetImage = new Bitmap(targetWidth, targetHeight, PixelFormat.Format24bppRgb);

            using (Graphics g = Graphics.FromImage(targetImage))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                // resize image
                var rc = new System.Drawing.Rectangle(0, 0, targetImage.Width, targetImage.Height);
                g.DrawImage(sourceImage, rc);
            }
            return targetImage;
        }
        public void CropUserPhoto(String imageName, int? x1, int? y1, int? x2, int? y2, int? width, int? height, int? imgscwidth, int userid, string imageFilePath, string targetFilePath)
        {

            const int thumbnailWidth = 50;
            const int thumbnailHeight = 60;
            const int thumbnailResolution = 125;

            if (x1 != null)
            {

                //String imageFilePath = HttpContext.Server.MapPath(imageName);
                ///String targetFilePath = HttpContext.Server.MapPath(String.Format("/images/users/{0}.jpg", userid));
                FileStream fs = System.IO.File.OpenRead(imageFilePath);

                using (System.Drawing.Image imgPhoto = System.Drawing.Image.FromStream(fs))
                {

                    fs.Close();
                    System.IO.File.Delete(imageFilePath);

                    double scale = (double)imgPhoto.Width / (double)imgscwidth;
                    int targetX = (int)((double)x1 * scale);
                    int targetY = (int)((double)y1 * scale);
                    int targetWidth = (int)((double)width * scale);
                    int targetHeight = (int)((double)height * scale);

                    using (Bitmap bmPhoto = new Bitmap(thumbnailWidth, thumbnailHeight, PixelFormat.Format24bppRgb))
                    {

                        bmPhoto.SetResolution(thumbnailResolution, thumbnailResolution);

                        using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                        {
                            grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                            grPhoto.DrawImage(imgPhoto, new System.Drawing.Rectangle(0, 0, thumbnailWidth, thumbnailHeight), targetX, targetY, targetWidth, targetHeight, GraphicsUnit.Pixel);

                            bmPhoto.Save(targetFilePath, ImageFormat.Jpeg);
                        }
                    }
                }
            }
        }
        //private string ConvertToPDF(string inputFilePath, string outputFilepath, string originalExt, string newFilename)
        //{
        //    var wordsLicense = new Aspose.Words.License();
        //    wordsLicense.SetLicense("Aspose.Total.lic");

        //    var pdfLicense = new Aspose.Pdf.License();
        //    pdfLicense.SetLicense("Aspose.Total.lic");

        //    var mappedeInputFilePath = Server.MapPath(inputFilePath);
        //    var pdfFile = Server.MapPath(Path.Combine(outputFilepath, Path.ChangeExtension(newFilename, "pdf")));

        //    // Verify input file exists
        //    if (!System.IO.File.Exists(mappedeInputFilePath))
        //    {
        //        throw new Exception(string.Format("Input file path does not exist({0})", mappedeInputFilePath));
        //    }

        //    // Verify output directory exists
        //    if (!System.IO.Directory.Exists(Path.GetDirectoryName(pdfFile)))
        //    {
        //        throw new Exception(string.Format("Output directory does not exist({0})", pdfFile));
        //    }

        //    if (originalExt == ".pdf")
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        var pdf = new Pdf();

        //        switch (originalExt.ToLower())
        //        {
        //            case ".doc":
        //            case ".docx":
        //            case ".rtf":
        //                var wordDoc = new Document(mappedeInputFilePath);
        //                wordDoc.Save(pdfFile, SaveFormat.Pdf);
        //                break;
        //            case ".txt":
        //                var txtBuilder = new DocumentBuilder();
        //                using (var reader = new StreamReader(outputFilepath + newFilename, Encoding.UTF8))
        //                {
        //                    while (true)
        //                    {
        //                        string line = reader.ReadLine();
        //                        if (line != null)
        //                            txtBuilder.Writeln(line);
        //                        else
        //                            break;
        //                    }
        //                }
        //                txtBuilder.Document.Save(pdfFile, SaveFormat.Pdf);
        //                break;
        //            case ".jpg":
        //            case ".jpeg":
        //            case ".tif":
        //            case ".tiff":
        //            case ".bmp":
        //            case ".gif":
        //            case ".png":
        //                pdf = new Pdf();
        //                pdf.PageSetup.Margin.Bottom = 0;
        //                pdf.PageSetup.Margin.Top = 0;
        //                pdf.PageSetup.Margin.Left = 0;
        //                pdf.PageSetup.Margin.Right = 0;
        //                pdf.PageSetup.PageHeight = PageSize.LegalHeight;
        //                pdf.PageSetup.PageWidth = PageSize.LegalWidth;

        //                var sec = pdf.Sections.Add();
        //                //Aspose.Pdf.Section sec = pdf.Sections.Add();

        //                var image = new Aspose.Pdf.Generator.Image(sec);
        //                //var image = new Aspose.Pdf.Image(sec);
        //                if (originalExt == ".jpg")
        //                    originalExt = ".jpeg";
        //                if (originalExt == ".tif")
        //                    originalExt = ".tiff";

        //                if (originalExt == ".tiff")
        //                {
        //                    float imagewidth;
        //                    float imageheight;
        //                    using (var stream = new FileStream(mappedeInputFilePath, FileMode.Open, FileAccess.Read))
        //                    {
        //                        using (System.Drawing.Image tif = System.Drawing.Image.FromStream(stream, false, false))
        //                        {
        //                            imagewidth = tif.PhysicalDimension.Width;
        //                            imageheight = tif.PhysicalDimension.Height;
        //                        }
        //                    }

        //                    image.ImageInfo.File = mappedeInputFilePath;
        //                    image.ImageInfo.ImageFileType = (ImageFileType)Enum.Parse(typeof(ImageFileType), TitleCase(originalExt.Substring(1)));

        //                    float pdfWidth = sec.PageInfo.PageWidth - sec.PageInfo.Margin.Left - sec.PageInfo.Margin.Right;
        //                    float pdfHeight = sec.PageInfo.PageHeight - sec.PageInfo.Margin.Top - sec.PageInfo.Margin.Bottom;

        //                    if (imagewidth > pdfWidth || imageheight > pdfHeight)
        //                    {
        //                        image.ImageInfo.FixWidth = pdfWidth;
        //                        image.ImageInfo.FixHeight = pdfHeight;
        //                    }
        //                    image.ImageInfo.TiffFrame = -1;

        //                    sec.Paragraphs.Add(image);
        //                    pdf.Save(pdfFile);
        //                }
        //                else
        //                {
        //                    System.Drawing.Image dstImage = null;
        //                    System.Drawing.Image srcImage = System.Drawing.Image.FromFile(mappedeInputFilePath);

        //                    var dstWidth = (int)ConvertUtil.PointToPixel(pdf.PageSetup.PageWidth - pdf.PageSetup.Margin.Left - pdf.PageSetup.Margin.Right);
        //                    var dstHeight = (int)ConvertUtil.PointToPixel(pdf.PageSetup.PageHeight - pdf.PageSetup.Margin.Top - pdf.PageSetup.Margin.Bottom);

        //                    sec.IsLandscape = srcImage.Width > srcImage.Height;

        //                    //Resize image if necessary
        //                    if (srcImage.Width > dstWidth || srcImage.Height > dstHeight)
        //                    {
        //                        dstImage = ResizeImage(srcImage, dstWidth, dstHeight);
        //                        image.ImageInfo.SystemImage = dstImage;
        //                    }
        //                    else
        //                        image.ImageInfo.SystemImage = srcImage;

        //                    sec.Paragraphs.Add(image);
        //                    pdf.Save(pdfFile);

        //                    srcImage.Dispose();
        //                    if (dstImage != null)
        //                        dstImage.Dispose();
        //                }
        //                break;
        //        }

        //        try
        //        {
        //            // attempt to delete the file that was converted
        //            System.IO.File.Delete(mappedeInputFilePath);
        //        }
        //        catch
        //        {
        //            // Do nothing if it fails
        //        }
        //    }

        //    return pdfFile;
        //}

        private Boolean SavePDF(byte[] buffer, String filePath)
        {
            Boolean success = false;
            try
            {
                System.IO.File.WriteAllBytes(filePath, buffer);
                success = true;
            }
            catch (Exception ex)
            {
                var temp = ex.Message;
            }
            return success;
        }
    }
}